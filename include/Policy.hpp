/*
 * Header file containing the classes used for the policies
 *
 * Copyright (C) 2020, University of Trento
 * Authors: Luigi Palopoli <luigi.palopoli@unitn.it>, Marco Roveri <marco.roveri@unitn.it>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
/** \file Policy.hpp
 * Header file containing the classes used for the policies
 */

#ifndef _POLICY_HPP
#define _POLICY_HPP
#include "BasicTypes.hpp"
#include "util/CheckSet.hpp"
#include "util/json.hpp"
#include "util/Serial.hpp"
#include <string>
namespace CovidTool {
   //! \brief Empty Policy
  /*!
    This policy is empty and is used only
    in methods where no real Policy should be applied (i,e,, if during
    a reachability analysis we aim at the construction of the whole
    state
    \tparam StateType Type of the state the policy applied to
   */
  template<typename StateType>
  class EmptyPolicy {
  public:
    //! \brief Function call operator
    /*!
      The Policy is actually a function object. This operator
      is called whenever an action is needed by the Solver or
      by any other object. In this case never. Therefore an
      exception is raised.
      \param s state of the MDP for which the action is required
      \param show princt out state and action (used for debug
      purposes).
      \return The action associated with the state
    */
    typename StateType::ActionType operator() ( const StateType & s, bool show=false) {
      EXC_PRINT("Wrong Call");
      return typename StateType::ActionType();
    }
    //! \brief Conversion to string of the policy
    std::string toString() { return std::string("Empty"); };
  };

  //! \brief Constant Policy
  /*!
    This policy returns a Constant Policy independently
    from the current state.
    \tparam StateType Type of the state the policy applied to
   */
  template<typename StateType>
  class ConstantPolicy {
    typename StateType::ActionType constP; /*!< Value used for the constant policy */
  public:
    //! \brief Parameters of the policy
    /*!
      This structure contains the parameters of the policy.
    */
    struct Parameters {

      typename StateType::ActionType constP; /*!< Value of the constant policy to be applied */


    //! \brief Serialises to json
    /*!
      This function is part of the serialisation framework
      used by the project. It serialises the constant value
      used by the policy.
      \param j json structure
    */
      void to_json(nlohmann::json &j) const;

      //! \brief Deserialises from json
      /*!
	This function is part of the serialisation framework
	used by the project. It deserialises the constant value
	used by the policy from the json structure
	\param j json structure
      */
      void from_json(const nlohmann::json & j);
    };

    //! \brief Constructor of the policy
    /*!
      Construct a policy with a null action.
      Before using the object you need to call setParameters,
      which sets the appropriate value
    */
    ConstantPolicy ();

    //! \brief Returns an alphanumeric string identifying the policy
    /*!
      Returns an alphanumeric identfiier for the policy
      including the numeric constant vlaue used
      \return String identifying the constant policy
     */
    std::string getId() const;

    //! \brief Conversion to string of the policy
    std::string toString() { return std::string("Constant"); };

    //! \brief Function call operator
    /*!
      The Policy is actually a function object. This operator
      is called whenever an action is needed by the Solver or
      by any other object.
      \param s state of the MDP for which the action is required
      \param show princt out state and action (used for debug
      purposes).
      \return The action associated with the state
     */
    typename StateType::ActionType operator() ( const StateType & s, bool show=false);

    //! \brief Sets the parameter of the policy
    /*!
      Sets the parameters of the Policy. This has to be called
      prior to the execution of any service requiring the application
      of the Policy.
      \param parameters Parameter structure to be set.
    */
    void setParameters(const CovidTool::ConstantPolicy<StateType>::Parameters & parameters);
  };


  //! \brief Adaptive policy in which we set two threshold. If the number of ill subjcets (e.g., symptomatic) drops bellow a threshold we apply the most liberal policy (M), if it is above another threshold we apply the most restrictive policy. In the middle we interpolate.
  /*!
    The idea is that if the total number of symptomatic and hospitalised patient is below a threshold, we go for a totally permissive policy (maximum number of encounters allowed), if it is above another threshold, we go for a restrictive policy (i.e., we reduce to the minimum the number of allowed encounters). In between the two thresholds, we apply an interpolation between the two extremes.
    \tparam StateType Type of the state, e.g., we can specialise to  to SSV state (a.k.a., StateWrapper<SSV_>.
  */
  template<typename StateType>
  class AdaptivePolicy {
  protected:
    double minPerc; /*!< Below this percentage of symptomatic subjects we go with maximum liberality */
    double maxPerc; /*!< Above this percentage of symptomatic subjects we go for an aggressive reduction of the encounters */
    unsigned minM; /*!< Minimum number of encounters (to use when the percentage of symptomatic exceed maxPerc) */
    unsigned maxM; /*!< Maximum number of encounters (to use when the pecentae is below minPerc) */
  public:
    //! \brief Parameters to be used to define the policy
    /*!
      This data structure encapsulates all the parameters to
      be used when using the Policy (i.e., thresholds, minimum
      and maximum action).
      The minimum and maximum actions to use are those encapsulated within th SSV_::Parameters
    */
    struct Parameters {
      double minPerc; /*!< Below this percentage of symptomatic subjects we go with maximum liberality */
      double maxPerc; /*!< Above this percentage of symptomatic subjects we go for an aggressive reduction of the encounters */

      //! \brief Serialises to json
      /*!
	This function is part of the serialisation framework
	used by the project. It serialises the Parameters
	of the AdaptivePolicy into the json structure
	\param j json structure
      */
      void to_json(nlohmann::json &j) const;

      /// @brief De-serialises from json

      //! \brief Serialises from json
      /*!
	This function is part of the serialisation framework
	used by the project. It deserialises the Parameters
	of the AdaptivePolicy from a json object
	\param j json structure
      */
      void from_json(const nlohmann::json & j);
    };

    //! \brief Default constructor
    /*!
      This constructor generates an empty policy, with all the
      parameters set to 0. The Policy constructed in this way
      needs the parameters to be set through setParameters
     */
    AdaptivePolicy();

     //! \brief Constructor
    /*!
      This constructor generates a Policy associating the
      necessary thresholds to the Policy function object.
      The minimum and maximum number of encounters are take
      from the Parameteres of SSV
      \param minPerc_d Minimum percentage below which we can
      allow for maximum number of encounters
      \param maxPerc_d Maximum percentage above which we
      restrict the number of encounters to the minimum.
     */
    AdaptivePolicy ( double minPerc_d, double maxPerc_d);


    //! \brief Returns an alphanumeric string identifying the policy
    /*!
      Returns an alphanumeric identfiier for the policy
      including the thresholds used by the policy
      \return String identifying the constant policy
    */
    std::string getId() const ;

    //! \brief Conversion to string of the policy
    std::string toString() { return std::string("Adaptive"); };

    //! \brief Function call operator
    /*!
      The Policy is actually a function object. This operator
      is called whenever an action is needed by the Solver or
      by any other object.
      \param s state of the MDP for which the action is required
      \param show princt out state and action (used for debug
      purposes).
      \return The action associated with the state, which is computed adapting
      the number of possible encoutners to the percentage of symptomatic and
      hospitalised people
     */
    virtual typename StateType::ActionType operator() ( const StateType & s, bool show = false) = 0;

    //! \brief Sets the parameter of the policy
    /*!
      Sets the parameters of the Policy. This has to be called
      prior to the execution of any service requiring the
      application of the Policy.
      \param parameters Parameter structure to be set.
    */
    void setParameters( const typename CovidTool::AdaptivePolicy<StateType>::Parameters & parms );

    virtual ~AdaptivePolicy() {};
  };

  //! \brief Specialisation of AdaptivePolicy in which we take our descisions based on the number of people who are known to be sick
  /*!
    If the total number of sick people is below a threshold, we go for a totally permissive policy (maximum number of encounters allowed), if it is above another threshold, we go for a restrictive policy (i.e., we reduce to the minimum the number of allowed encounters). In between the two thresholds, we apply a a linear interpolation between the two extremes.
    \tparam StateType Type of the state, e.g., we can specialise to SSV state (a.k.a., StateWrapper<SSV_>).
  */
  template<typename StateType>
  class AdaptiveSymptPolicy : public AdaptivePolicy<StateType> {
  public:
    //! \brief Default constructor
    /*!
      This constructor generates an empty policy, with all the
      parameters set to 0. The Policy constructed in this way
      needs the parameters to be set through setParameters
    */
    AdaptiveSymptPolicy(): AdaptivePolicy<StateType>() {};

    //! \brief Constructor
    /*!
      This constructor generates a Policy associating the
      necessary thresholds to the Policy function object.
      The minimum and maximum number of encounters are take
      from the Parameteres of SSV
      \param minPerc_d Minimum percentage below which we can
      allow for maximum number of encounters
      \param maxPerc_d Maximum percentage above which we
      restrict the number of encounters to the minimum.
    */
    AdaptiveSymptPolicy( double minPerc_d, double maxPerc_d) : AdaptivePolicy<StateType>(minPerc_d, maxPerc_d) {};

    virtual typename StateType::ActionType operator() ( const StateType & s, bool show = false);

    //! \brief Conversion to string of the policy
    std::string toString() { return std::string("AdaptiveSympt"); };

  };

  //! \brief Specialisation of AdaptivePolicy in which we take our descisions based on the number of asymptomatic patients
  /*!
    If the total number of symptomatic and hospitalised and asymptomatic patients is below a threshold, we go for a totally permissive policy (maximum number of encounters allowed), if it is above another threshold, we go for a restrictive policy (i.e., we reduce to the minimum the number of allowed encounters). In between the two thresholds, we apply a a linear interpolation between the two extremes.
    \tparam StateType Type of the state, e.g., we can specialise to SSV state (a.k.a., StateWrapper<SSV_>).
  */
  template<typename StateType>
  class AdaptiveAsymptPolicy : public AdaptivePolicy<StateType> {
  public:
    //! \brief Default constructor
    /*!
      This constructor generates an empty policy, with all the
      parameters set to 0. The Policy constructed in this way
      needs the parameters to be set through setParameters
    */
    AdaptiveAsymptPolicy(): AdaptivePolicy<StateType>() {};

    //! \brief Constructor
    /*!
      This constructor generates a Policy associating the
      necessary thresholds to the Policy function object.
      The minimum and maximum number of encounters are take
      from the Parameteres of SSV
      \param minPerc_d Minimum percentage below which we can
      allow for maximum number of encounters
      \param maxPerc_d Maximum percentage above which we
      restrict the number of encounters to the minimum.
    */
    AdaptiveAsymptPolicy ( double minPerc_d, double maxPerc_d) : AdaptivePolicy<StateType>(minPerc_d, maxPerc_d) {};


    virtual typename StateType::ActionType operator() ( const StateType & s, bool show = false);

    //! \brief Conversion to string of the policy
    std::string toString() { return std::string("AdaptiveAsympt"); };

  };
}

#include "Policy_src.hpp"
#endif
