template<typename StateType>
inline void CovidTool::ConstantPolicy<StateType>::Parameters::to_json(nlohmann::json &j) const {
  j = nlohmann::json {
	  TO_JSON(constP)
  };
  return;
};

template<typename StateType>
inline void CovidTool::ConstantPolicy<StateType>::Parameters::from_json(const nlohmann::json & j) {
  FROM_JSON(j, constP);

  return;
};

template<typename StateType>
inline CovidTool::ConstantPolicy<StateType>::ConstantPolicy() :
  constP()
{};

template<typename StateType>
inline std::string CovidTool::ConstantPolicy<StateType>::getId() const {
  return std::string("Constant_Policy_")+ std::to_string(unsigned(constP));
}


template<typename StateType>
inline typename StateType::ActionType CovidTool::ConstantPolicy<StateType>::operator() ( const StateType & s, bool show) {
  return constP;
}

template<typename StateType>
inline void CovidTool::ConstantPolicy<StateType>::setParameters(const CovidTool::ConstantPolicy<StateType>::Parameters & parameters) {
      constP = parameters.constP;
};

template<typename StateType>
void CovidTool::AdaptivePolicy<StateType >::Parameters::to_json(nlohmann::json & j) const {
  j = nlohmann::json {
    TO_JSON(minPerc),
    TO_JSON(maxPerc)
  };
  return;
}

template<typename StateType>
void CovidTool::AdaptivePolicy<StateType >::Parameters::from_json(const nlohmann::json & j) {
  FROM_JSON(j, minPerc);
  FROM_JSON(j, maxPerc);

  return;
}

template<typename StateType>
inline CovidTool::AdaptivePolicy<StateType >::AdaptivePolicy() :
  minPerc(0.0),
  maxPerc(0.0),
  minM(0),
  maxM(0) {
};


template<typename StateType>
inline CovidTool::AdaptivePolicy<StateType >::AdaptivePolicy ( double minPerc_d, double maxPerc_d): minPerc(minPerc_d), maxPerc(maxPerc_d) {
  if (!StateType::parmsSet)
    EXC_PRINT("cannot create a an adaptive policy on SSV without knowing its parameters");
  minM = StateType::parms.Mmin;
  maxM = StateType::parms.M;
};

template<typename StateType>
inline std::string CovidTool::AdaptivePolicy<StateType >::getId() const {
  return std::string("_Adaptive_Sympt_Policy_")
    + std::to_string(minPerc)
    + UNDERSC
    + std::to_string(maxPerc);
}

template<typename StateType>
inline  void CovidTool::AdaptivePolicy<StateType >::setParameters( const CovidTool::AdaptivePolicy<StateType>::Parameters & parms ) {
  CHECK_RANGE(parms.minPerc, 0.0, 1.0);
  CHECK_RANGE(parms.maxPerc, 0.0, 1.0);
  if (parms.minPerc > parms.maxPerc)
    EXC_PRINT("Inconsistent choices of parameters");
  minPerc = parms.minPerc;
  maxPerc = parms.maxPerc;

  minM = StateType::parms.Mmin;
  maxM = StateType::parms.M;
}




template<typename StateType>
typename StateType::ActionType CovidTool::AdaptiveSymptPolicy<StateType >::operator() ( const StateType & s, bool show ) {
  auto minPerc = CovidTool::AdaptivePolicy<StateType>::minPerc;
  //using CovidTool::AdaptivePolicy<StateType>::minPerc;
  auto maxPerc = CovidTool::AdaptivePolicy<StateType>::maxPerc;
  auto maxM =  CovidTool::AdaptivePolicy<StateType>::maxM;
  auto minM =  CovidTool::AdaptivePolicy<StateType>::minM;
  
  double minTh = minPerc*(StateType::parms.N - s.data.d);
  double maxTh = maxPerc*(StateType::parms.N - s.data.d);
  double m;
  if (show) {
    TRACE(minTh);
    TRACE(maxTh);
    TRACE(maxM);
    TRACE(minM);
  }
  if (s.data.getSymptomatic() <= minTh) {
    return typename StateType::ActionType(maxM);
  }
  if (s.data.getSymptomatic() >= maxTh) {
    return typename StateType::ActionType(minM);
  }
  m = minM + (( double(s.data.getSymptomatic()- minTh))/double(maxTh-minTh))*(maxM-minM);
  return typename StateType::ActionType(unsigned(m));
};



template<typename StateType>
typename StateType::ActionType CovidTool::AdaptiveAsymptPolicy<StateType >::operator() ( const StateType & s, bool show ) {
  auto minPerc =  CovidTool::AdaptivePolicy<StateType>::minPerc;
  auto maxPerc =  CovidTool::AdaptivePolicy<StateType>::maxPerc;
  auto maxM =  CovidTool::AdaptivePolicy<StateType>::maxM;
  auto minM =  CovidTool::AdaptivePolicy<StateType>::minM;
  

  double minTh = minPerc*(StateType::parms.N - s.data.d);
  double maxTh = maxPerc*(StateType::parms.N - s.data.d);
  double m;
  
  if (show) {
    TRACE(minTh);
    TRACE(maxTh);
    TRACE(maxM);
    TRACE(minM);
  }

  
  if ( s.data.getAsymptomatic() <= minTh ) {
    return  typename StateType::ActionType(maxM);
  }
  if (s.data.getAsymptomatic() >= maxTh) {
    return typename StateType::ActionType(minM);
  }
  m = minM + (( double(s.data.getAsymptomatic()-minTh))/double(maxTh-minTh))*(maxM-minM);
  return typename StateType::ActionType(unsigned(m));
};
