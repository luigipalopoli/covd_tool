/*
 * Header file containing the code used for computing probabilities
 *
 * Copyright (C) 2020, University of Trento
 * Authors: Luigi Palopoli <luigi.palopoli@unitn.it>, Marco Roveri <marco.roveri@unitn.it>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License version
 * 2 as published by the Free Software Foundation.
 */
/** \file TransProb.hpp
 * Header file containing the code used for computing probabilities
 */
#ifndef _TRANS_PROB_HPP
#define _TRANS_PROB_HPP

#include "BasicTypes.hpp"

double transProbDelta(const CovidTool::SSV_ & Vk, const std::vector<unsigned> & Delta,
		      const CovidTool::SSV_::ActionType & A);

double transProbDeltaT(const CovidTool::ESV_ & Vk, const std::vector<unsigned> & Delta,
		       const CovidTool::SSV_::ActionType & A);

void print_memoizing_stats();

#endif
