template<typename StateValueType, typename Policy>
CovidTool::Solver<StateValueType, Policy>::Solver( CovidTool::StateSpace<StateValueType> & ss_d, CovidTool::ReachabilityAnalyser<StateValueType> & ra_d, Policy & policyFun) :
  _initialStepMade(false),
  _paramsSet(false),
  _ss(ss_d),
  _ra(ra_d),
  _policyFun(policyFun)
{
  _ra.setStateSpace(ss_d);
};


template<typename StateValueType, typename Policy>
CovidTool::EigMatrix CovidTool::Solver<StateValueType, Policy>::computeTransitionMatrix(  ) {
  if ( !_paramsSet )
    EXC_PRINT("Cannot compute transitions matrix before setting parameters");
#if 0
  //OUTDATE CODE......staging here but very close to removalli
  _ra.computeReachableStates( maxIter );
  // _ra.printReachableStates( maxIter);
  std::vector<CovidTool::Transition> trans = _ra.generateTransitions();
#else
  VERBOSE_PRINT("Computing the transition table", _verbose);
  std::vector<CovidTool::Transition> trans = _ra.generateTransitionsWithReachable(  true, _policyFun, _maxReachIter );
  VERBOSE_PRINT("Transition Table Computed", _verbose);
#endif
  if(trans.size() == 0) {
    WARNING_PRINT("Empyt reachable set");
    transMatrix = EigMatrix();

    return transMatrix;
  }

  // transReduced.clear();

  // VERBOSE_PRINT("Applying Policy", _verbose);
  // for (auto item : trans ) {
  //   if ((item.actionCode)== unsigned(_policyFun(_ss.getStateValue(item.sourceCode))))
  //     transReduced.push_back(item);
  // }
  // VERBOSE_PRINT("Policy applied", _verbose);

  
  unsigned size = _ss.getNumberOfStates();
#ifdef EIGEN_DENSE
  transMatrix = Eigen::MatrixXd::Zero(size, size);
#else
  transMatrix = Eigen::SparseMatrix<double>(size, size);
  std::vector<Eigen::Triplet<double> > tripl;
#endif
  VERBOSE_PRINT("Generating transition matrix",_verbose);
  int count = 0;

  for (auto el : trans ) {
    count ++;
    VERBOSE_PRINT(std::string("Coefficients generated ")+
		  std::to_string(count)+
		  std::string("/") +
		  std::to_string(trans.size()),
		  (count% 200) == 0,
		  _verbose);
#ifdef EIGEN_DENSE
    transMatrix(el.sourceCode, el.destCode) = el.prob;
#else
    tripl.push_back(Eigen::Triplet<double>(el.sourceCode, el.destCode,el.prob)); 
#endif
  }
#ifndef EIGEN_DENSE
  VERBOSE_PRINT("Constructing matrix from triplets",_verbose);
  transMatrix.setFromTriplets(tripl.begin(), tripl.end());
#endif

  
  VERBOSE_PRINT("Transition matrix computed",_verbose);
  return transMatrix;
};


template<typename StateValueType, typename Policy>
  std::map<StateValueType,double> CovidTool::Solver<StateValueType, Policy>::solve ( const int iter )
{
  
  if ( !_paramsSet )
    EXC_PRINT("Cannot solve before setting parameters");
  if (!_initialStepMade) {
    
    unsigned size = computeTransitionMatrix( ).rows();

    {
      Eigen::VectorXd c = transMatrix*Eigen::VectorXd::Ones(transMatrix.cols());      

      c = (c - Eigen::VectorXd::Constant(c.rows(), 1)).cwiseAbs();
      bool ch3 = (Eigen::ArrayXXd(c) < 1e-12).all();
    }



    if (size == 0) {
      WARNING_PRINT("No computation done");
      return std::map<StateValueType,double>();
    };
#ifdef EIGEN_DENSE
    stateVec = Eigen::VectorXd::Zero(size);
#else
    stateVec =Eigen::SparseVector<double>(size);
#endif
    //  TRACE(stateVec.size());
    VERBOSE_PRINT("Executing the first computation step",_verbose);
    for (int i = 0; i < initialStates.size(); i++) {
#ifdef EIGEN_DENSE
      stateVec[ _ss.getStateNumber(initialStates.at(i))] = initialProbability.at(i);
#else
      stateVec.insert(_ss.getStateNumber(initialStates.at(i))) = initialProbability.at(i);
#endif
    };
    VERBOSE_PRINT("First computation step executed",_verbose);


  
    _initialStepMade = true;
  }
  VERBOSE_PRINT(std::string("Executing ")+std::to_string(iter)+std::string(" iterations"), _verbose);
  auto work = solveSteps( iter);
  {
// #ifdef EIGEN_DENSE
//       auto c1 = stateVec.rowwise().sum();
// #else
//       auto c1 = stateVec.sum();
// #endif
//       TRACE(c1);
  }
  VERBOSE_PRINT("Iterations computed", _verbose);
  return work;  
}


template<typename StateValueType, typename Policy>
std::map<StateValueType,double> CovidTool::Solver<StateValueType, Policy>::solveSteps ( const int iter )
{

  assert(_initialStepMade);

  int size = transMatrix.rows();
#ifdef EIGEN_DENSE
  for (unsigned it = 0; it < iter; it++)
    stateVec = stateVec*transMatrix;
#else
  auto P = transMatrix.transpose();
  for (unsigned it = 0; it < iter; it++)
    stateVec = P*stateVec;
#endif
  std::map<StateValueType,double> work;
#ifdef EIGEN_DENSE
  for (int i = 0; i < size; i++) {
    work.insert(std::pair<StateValueType,double>(_ss.getStateValue(i), stateVec[i]));
  }
#else
  for (Eigen::SparseVector<double>::InnerIterator it(stateVec); it; ++it)
    {
      work.insert(std::pair<StateValueType,double>(_ss.getStateValue(it.index()), it.value()));
    }
#endif
  return work;

}
template<typename StateValueType, typename Policy>
void  CovidTool::Solver<StateValueType, Policy>::setParameters(const CovidTool::Solver<StateValueType, Policy>::Parameters & p_d) {
  if ( p_d.initialStates.size() != p_d.initialProbability.size() )
    EXC_PRINT("Inconsistent size of initial states and probabilities");
  initialStates = p_d.initialStates;
  initialProbability = p_d.initialProbability;
  
  double totProb = 0;
  for (auto el : initialProbability) {
    totProb += el;
  }
  
  if (std::abs(totProb - 1.0) > p_d.sp.eps_tot_prob)
    EXC_PRINT("Initial probabilities do not sum to 1.0");
      
  
  _maxReachIter = p_d.maxReachIter;
  
  StateValueType::setParameters(p_d.sp);
  _policyFun.setParameters(p_d.pp);

  for (int i = 0; i < initialStates.size(); i++) { 
    initialStates.at(i).data.setCode();
    if ( !initialStates.at(i).checkConsistency() )
      EXC_PRINT("Inconsistent Initial states");
  }
  _ra.setInitialStates(initialStates);
  
  _paramsSet = true;
  _initialStepMade = false;
  
}


template<typename StateValueType, typename Policy>
void  CovidTool::Solver<StateValueType, Policy>::setVerbose( ) {
  _ra.setVerbose();
  _verbose = true;
}




//Inline methods of the Parameters structure
template<typename StateValueType, typename Policy>
inline void CovidTool::Solver<StateValueType, Policy>::Parameters::to_json(nlohmann::json &j) const {
  nlohmann::json work;
  j["maxReachIter"] = maxReachIter;
	
  for ( auto el: initialStates )
    work.push_back(el);
  j["initialStates"] = work;
  
  nlohmann::json work1;
  for ( auto el: initialProbability )
    work1.push_back(el);
  j["initialProbability"] = work1;
  
  nlohmann::json work2;
  sp.to_json( work2);
  j["sp"] = work2;
  
  nlohmann::json work3;
  pp.to_json( work3);
  j["pp"] = work3;
  
  return;
}


template<typename StateValueType, typename Policy>
inline void CovidTool::Solver<StateValueType, Policy>::Parameters::from_json(const nlohmann::json &j) {
  nlohmann::json work0 = j.at("maxReachIter");
  nlohmann::from_json(work0,  maxReachIter );
	
  nlohmann::json work = j.at("initialStates");
  initialStates.clear();
  for (auto el : work) {
    StateValueType v;
    v.from_json(el);
    initialStates.push_back(v);
  }
  initialProbability.clear();
  nlohmann::json work1 = j.at("initialProbability");
  for (auto el : work1) {
    double v;
    nlohmann::from_json(el, v);
    initialProbability.push_back(v);
  }
  nlohmann::json work2 = j.at("sp");
  sp.from_json(work2);
  
  nlohmann::json work3 = j.at("pp");
  pp.from_json(work3);
  
  return;
}


