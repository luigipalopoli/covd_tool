/*
 * Header file for computing duration for a method call
 *
 * Copyright (C) 2017, University of Trento
 * Authors: Luigi Palopoli <luigi.palopoli@unitn.it>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

/** \file TimeCount.hpp
 * Header file for computing duration for a method call
 */
#ifndef TIME_COUNT_HPP
#define TIME_COUNT_HPP

#include <chrono>
#include <utility>
#include <chrono>
#include <utility>
#include <functional>


namespace CovidTool{
  typedef std::chrono::high_resolution_clock::time_point TimeVar;

  using Duration= std::chrono::nanoseconds;
  
#define duration(a) std::chrono::duration_cast<std::chrono::nanoseconds>(a).count()
#define timeNow() std::chrono::high_resolution_clock::now()

  template<typename R, typename ...Args>
  std::pair<R, double> functTime1(std::function<R(Args...)> fun, Args && ... args ) {
    TimeVar t1=timeNow();
    R res = fun(std::forward<Args>(args)...);
    //    R res = fun(args...);
    return std::pair<R, double>(res, duration(timeNow()-t1));
  };


  template<typename R>
  std::pair<R,double> functTime1(std::function<R( )> fun) {
     TimeVar t1=timeNow();
     R res = fun( );
     return std::pair<R, double>(res, duration(timeNow()-t1));
  };

  template<typename ...Args>
  double functTime1(std::function<void(Args...)> fun, Args && ... args ) {
    TimeVar t1=timeNow();
    fun(std::forward<Args>(args)...);
    //    R res = fun(args...);
    return double(duration(timeNow()-t1));
  };

};
#endif
