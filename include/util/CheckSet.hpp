#ifndef CHECKSET_HPP
#define CHECKSET_HPP
#include "util/json.hpp"
#include "util/exc.hpp"
#include "util/PrintUtils.hpp"



template<typename Type>
void check_range(Type v, Type vmin, Type vmax, const std::string & varName) {
  if ( (v < vmin) || (v > vmax) ){	
    std::cerr<<"Variable "<<varName<<", out of range."<<std::endl;	
      EXC_PRINT("Variable out of range");			
    };								
}

#define CHECK_RANGE(v, vmin, vmax) check_range(v, vmin, vmax, std::string(#v))

#define CHECK_PROB(v) check_range(double(v), 0.0, 1.0, std::string(#v))

template<typename T>
void JSON_TO_PAR(const nlohmann::json & j, T & p, const char * name) {
  try {
    p = j.at(name).get<T>();
  }
  catch(const std::exception & e) {
    std::cerr<<e.what()<<std::endl;
    EXC_PRINT(safe_stringprintf("Exception found in de-serialising parameter: %s\n ",name));
  };
  return;
}


#define TO_JSON(x)  {#x, x}

#define TO_JSON2(j, x)  { j[#x] =  x;}

#define FROM_JSON(j, x) \
  do {\
    JSON_TO_PAR(j, x, #x);\
  } while(0)




template<typename T>
inline std::string dump_var(const std::string & varName,  const T &  v) {
  //std::cerr<<varName<<std::endl;
  return varName+std::string(": ")+std::to_string(v)+std::string(" ");
};

template<>
inline std::string dump_var<std::string>(const std::string & varName, const std::string  & v) {
  return varName+std::string(": ")+ v +std::string(" ");
}



// template<>
// inline std::string dump_var<int>(const std::string & varName, int  v) {
//   return safe_stringprintf("%-15s, %10.2i, ", varName,  v);
// }

// template<>
// inline std::string dump_var<double>(const std::string & varName, double v) {
//   return safe_stringprintf("%-15s, %10.5f, ", varName,  v);
// }
#define DUMP_VAR(x)  dump_var( std::string(#x), x) 

#define COMMA std::string(", ")
#define UNDERSC std::string("_")
#endif
