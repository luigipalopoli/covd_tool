/*
 * Header file for serialising classes using open CV
 *
 * Copyright (C) 2017, University of Trento
 * Authors: Luigi Palopoli <luigi.palopoli@unitn.it>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#ifndef _SERIAL_HPP
#define _SERIAL_HPP
#include <string>
#include "util/json.hpp"
#include "util/exc.hpp"
namespace CovidTool {
 
    /// @brief serialises a generic sinter class to json
  /// @ingroup Utilities
  /// Given a generic class of the sinter namespace, it
  /// serialiases it to a json file. This is done by simply
  /// calling the to_json method, which is assumed defiend in the class
  /// @tparam SerialClass Class to be serialised
  /// @param j json object to serialise the object into
  /// @param p object that needs to be serialised
  template<typename SerialClass>
  inline 
  void to_json(nlohmann::json &j, const SerialClass & p)
  {
    p.to_json(j);
  }

  /// @brief Deserialises a generic sinter class from a json file
  /// @ingroup Utilities
  /// Given a json object it deserialises into an object of a generic
  /// class. This is done by simply
  /// calling the from_json method, which is assumed defiend in the class
  /// @tparam SerialClass Class to be serialised
  /// @param j json object to deserialise the object from
  /// @param p object that needs to be generated
  template<typename SerialClass>
   inline 
  void from_json(const nlohmann::json &j, SerialClass & p)
  {
    p.from_json(j);
  }
}



#endif



