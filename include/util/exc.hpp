/*
 * Header file for the exception mechanism and debugging
 *
 * Copyright (C) 2013, University of Trento
 * Authors: Luigi Palopoli <palopoli@disi.unitn.it>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
/** \file exc.hpp
 * Header file for the exception mechanism and debugging
 */
#ifndef EXC_HPP
#define EXC_HPP
#include<iostream>
#include<string>
using namespace std;
/** \defgroup Debugging Macros for debugging and error reporting 
 ** This group of macros are used for debbuging and for throwing exceptions.
 ** @{
 */
/// @brief Class used to encapsulate exceptions
///
/// This is a standard class used to encapsulate exceptions.
/// The way an excetption is thrown is simply
/// @code
/// throw Exc("exception of .....");
/// @endcode
class Exc {
  string msg;
public:
  Exc(const char *ms): msg(ms){};
  Exc(string s): msg(s){};
  virtual string what() 
  {
    cerr<<msg<<endl;
    return msg;
  };
};

/*! 
  \def EXC_PRINT(msg)
  Macro that prints out the message \a msg along with line, function, where the problem occurs and then throws an exception 
*/
#define EXC_PRINT(msg)\
 do {\
 cerr<<"file:"<<__FILE__<<", line:"<<__LINE__<<", function: "<<__PRETTY_FUNCTION__<<". "<<msg<<endl; \
 throw Exc(string(__PRETTY_FUNCTION__)+string(": ")+string(msg));\
} while(0)

/*! 
    \def EXC_PRINT_2(msg, msg1)
    Same as #EXC_PRINT, except that it prints two messages: \a msg and \a msg1 */
#define EXC_PRINT_2(msg,msg1)\
 do {\
 cerr<<"file:"<<__FILE__<<", line:"<<__LINE__<<", function: "<<__PRETTY_FUNCTION__<<". "<<msg<<" "<<msg1<<endl; \
 throw Exc(string(__PRETTY_FUNCTION__)+string(": ")+string(msg)+string(" ")+string(msg1));\
} while(0)
    

/*! 
  \def TRACE(x)
  Macro to print a variable \a x along with its name and the position where the macro is called (file an line) 
*/
#define TRACE(x) do {\
    std::cerr<<"file:"<<__FILE__<<"Line:"<<__LINE__<<" :  "<<#x << '=' << x<<std::endl;\
    std::cerr.flush();\
 } while (0)

/*!
  \def HERE
  Macro to print a position reached by the code (file + line).  
*/
#define HERE do {\
    std::cerr<<"POINT REACHED! "<<"file:"<<__FILE__<<"Line:"<<__LINE__<<std::endl; \
    std::cerr.flush();							\
 } while (0)

/** @} */ //End of Debugging macros 
#endif
