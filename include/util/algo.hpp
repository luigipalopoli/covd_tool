/*
 * Header file for some useful algorithms to be used
 *
 * Copyright (C) 2013, University of Trento
 * Authors: Luigi Palopoli <palopoli@disi.unitn.it>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
/** \file algo.hpp
 * Header file for some useful algorithms to be used
 */

#ifndef _ALGO_HPP
#define _ALGO_HPP

template< typename ContainerT, typename PredicateT >
void erase_if( ContainerT& items, const PredicateT& predicate ) {
  for( auto it = items.begin(); it != items.end(); ) {
    if( predicate(*it) ) it = items.erase(it);
    else ++it;
  };
};


#endif
