#ifndef PRINT_UTILS_HPP
#define PRINT_UTILS_HPP

#include <string>
#include <cstdarg>
#include <iostream>
#include <memory>

/// @ingroup Utilities
/// @{

/*! \brief Prints a message in verbose mode
 * Function used to print a message when in verbose mode.
 * \param x string to be printed
 * \param verbose_flag Flag that denotes if we are in verbose mode
 */
inline void VERBOSE_PRINT(const std::string &x, bool verbose_flag)
{
  if ( verbose_flag ) 
    std::cout << x << std::endl; 
  return;
}


/// @function
/// @brief Prints a message in verbose mode
///
/// Function used to print a message when in verbose mode.
/// @param msg standard null terminated character array contaning the message
/// @param verbose_flag Flag that denotes if we are in verbose mode
inline void VERBOSE_PRINT(const char * msg, bool verbose_flag)
{
  VERBOSE_PRINT(std::string(msg), verbose_flag);
  return;
}


/*! \brief Prints a message in verbose mode
 * Function used to print a message when in verbose mode when some condition is met
 * \param x string to be printed
 * \param verbose_flag Flag that denotes if we are in verbose mode
 */
inline void VERBOSE_PRINT(const std::string &x, bool cond, bool verbose_flag)
{
  if (cond)
    VERBOSE_PRINT(x, verbose_flag);
  
  return;
}

/*! \brief Prints a message in verbose mode
 * Function used to print a message when in verbose mode when some condition is met
 * \param x string to be printed
 * \param verbose_flag Flag that denotes if we are in verbose mode
 */
inline void VERBOSE_PRINT(const char * msg, bool cond, bool verbose_flag)
{
  if (cond)
    VERBOSE_PRINT(std::string(msg), verbose_flag);
  
  return;
}




/// @brief Prints a warning message
///
/// Function used to print a warning message.
/// @param x standard c++ string containing the message
inline void WARNING_PRINT(const std::string && x)  {				
  std::cerr <<"WARNING: "<< x << std::endl;				
};



/// @brief Prints a warning message
///
/// Function used to print a warning message.
/// @param msg standard null terminated character array containing the message
inline void WARNING_PRINT(const char * msg)  {				
  WARNING_PRINT(std::string(msg));
};

/// @brief Prints an error message
///
/// Function used to print an error  message.
/// @param x standard c++ string containing the message
inline void ERROR_PRINT(const std::string && x)  {				
  std::cerr <<"ERROR: "<< x << std::endl;				
};



/// @brief Prints an error message
///
/// Function used to print an error message.
/// @param msg standard null terminated character array containing the message
inline void ERROR_PRINT(const char * msg)  {				
  WARNING_PRINT(std::string(msg));
};





/// @brief Generates a bound checked c++ string from a standard printf function
///
/// This function is used to generate a standard c++ string withboudn checks from a printf.
/// @param format format string (passed as a std::string)
/// @param args variable length list of arguments
template <typename ...Args>
std::string safe_stringprintf(const std::string& format, Args && ...args)
{
    auto size = std::snprintf(nullptr, 0, format.c_str(), std::forward<Args>(args)...);
    std::string output(size + 1, '\0');
    std::sprintf(&output[0], format.c_str(), std::forward<Args>(args)...);
    return output;
}
/// @brief Generates a bound checked c++ string from a standard printf function
///
/// This function is used to generate a standard c++ string withboudn checks from a printf.
/// @param format format string (passed as a null terminated character array)
/// @param args variable length list of arguments
template <typename ...Args>
std::string safe_stringprintf(const char * format, Args && ...args)
{
  return safe_stringprintf(std::string(format), std::forward<Args>(args)...);
}


#ifndef DOXYGEN_SHOULD_SKIP_THIS
template <class Container>
int splitLine(const std::string& str, Container& cont,
              const std::string& delims = " ")
{
  std::size_t current, previous = 0; int tok=  0;
  
    current = str.find_first_of(delims);
   
    while (current != std::string::npos) {
      cont.push_back(str.substr(previous, current - previous));
      tok++;
      previous = str.find_first_not_of(delims, current);
      if (previous ==  std::string::npos)
	break;
      
      current = str.find_first_of(delims, previous );
    }
    if (previous != std::string::npos)
      cont.push_back(str.substr(previous, current - previous));
    return tok;
}
#endif


#endif



