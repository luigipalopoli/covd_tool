/*
 * Header file containing the classes used for gnerating and solving the Markov Chain with a given policy
 *
 * Copyright (C) 2020, University of Trento
 * Authors: Luigi Palopoli <luigi.palopoli@unitn.it>, Marco Roveri <marco.roveri@unitn.it>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
/** \file Solver.hpp
 * Header file containing the classes used for gnerating and solving the Markov Chain with a given policy
 */

#ifndef _SOLVER_HPP
#define _SOLVER_HPP
#include "Reachability.hpp"

#ifdef EIGEN_DENSE
#include <Eigen/Dense>
#else
#include <Eigen/Sparse>
#endif

namespace CovidTool{
#ifdef EIGEN_DENSE
  using EigMatrix = Eigen::MatrixXd;
  using EigVector = Eigen::RowVectorXd;
#else
  using EigMatrix = Eigen::SparseMatrix<double>;
  using EigVector = Eigen::SparseVector<double>;
#endif
  //! \brief Constructs and solves the Markov Chain Matrix
  /*!
    Constructs and solves the Markov Chain Matrix. The class exploits the services of a StateSpace (which maps states to numeric codes), of a reachability analyser (which identifies the reachable states and the transition) and a Policy object (which maps each state to an action)
    \tparam Policy template function object for the policy (i.e., how to associate a StateValueType with an action)
    \tparam StateValueType type of the state managed by the solver (e.g, SSV or ESV)
  */
  template<typename StateValueType, typename Policy>
  class Solver {
  public:
    //! \brief Constructor
    /*! 
      Construct a solver class. 
      \param ss_s StateSpace object to determine the mapping between states and numeric codes
      \param ra_d 
     */
    Solver( CovidTool::StateSpace<StateValueType> & ss_d, CovidTool::ReachabilityAnalyser<StateValueType> & ra_d, Policy & policyFun);
    
    //! \brief Solution function
    /*!
      It computes the solution propagating forward initial states and probabilities.
      \param iter Number of iterations
      \return Probability associated with each value of the state
    */
    std::map<StateValueType,double> solve ( const int iter );


    //! \brief Computes the transition matrix
    /*!
      It computes the transition matrix. The computation is made by first performing
      reachability analysis (in order to obtain a transition table) and then by applying the actions
      dictated by the Policy in order to obtain a state transition matrix
      \return State transition matrix (the states are numbered as in the StateSpace object)
    */
    EigMatrix computeTransitionMatrix( );

    
    //! \brief Parameters of the solver
    /*!
      This class contains the parameters used by the solver to compute a solution.
    */
    struct Parameters {
      std::vector<StateValueType>  initialStates; /*!< Initial state we start the computation from */
      std::vector<double> initialProbability; /*!< Initial probabilities associated with the state (has to have the same size as the initialStates vector) */
      int maxReachIter; /*!< Maximum number of iterations allowed to the reachability analyser */
      typename StateValueType::Parameters sp; /*!< Parameters of the state (e.g., population size, probability to transmit the virus etc. */
      typename Policy::Parameters pp; /*!< Parameters of the Policy (e.g., thresholds to decide the different actions) */

      //! \brief Serialises to json
      /*! 
	This function is part of the serialisation framework
	used by the project. It serialises the different elements of the Parameters structure.
	\param j json structure
      */ 
      void to_json(nlohmann::json &j) const;

      //! \brief De-serialises from json
      /*!
	This function is part of the serialisation framework
	used by the project. It deserialises the differrent elements of the Parameters structure.
	\param j json structure
      */ 
      void from_json(const nlohmann::json &j);
    };

    
    //! \brief Sets the parameters
    /*!
      Sets the parameters of the Solver. It recursively sets the parameters of the reachability analyser and of the states
      \param p_d desried value for the parameters
    */ 
    void setParameters(const CovidTool::Solver<StateValueType, Policy>::Parameters & p_d);

    //! \brief Sets the verbose flag
    /*!
      Sets the verbose flag to print out diagnostic messages while the RechabilityAnalyser
      executes. Useful for debug purposes. Could be commented out in production.
    */
    void setVerbose();
  
      
  private:
    
    bool _verbose = false; /*! verbose flag */
    bool _paramsSet;           /*!< Flag to decide if the parameters have been set */ 
    bool _initialStepMade;       /*!< Flag to decide if the first computation has been triggered (thereby it is possible to build on the result of the previous iterations */
    int _maxReachIter;           /*!< maximum number of iterations allowed before fixed point in reachability analysis */

    // References to the reachability analyser, StateSpace and Policy function object
    // These object provide the Solver with the necessary services to compute the
    // solution
    ReachabilityAnalyser<StateValueType> & _ra;
    CovidTool::StateSpace<StateValueType> & _ss;
    Policy & _policyFun;
    
    std::vector<StateValueType> initialStates; /*!<Vector of the initial states */
    std::vector<double> initialProbability; /*!< Vector of the probabilities associated with the initial states */
    std::vector<CovidTool::Transition> transReduced; /*!< Transition table reduced after the application of the policy */
    EigMatrix transMatrix; /*< Transition matrix found after the application of the policy */ 
    EigVector stateVec; /*!< Current value of the different probabilities. This vector is intialised at the first solution step and is updated on all subsequent calls to solve */

    //! \brief Solution for some steps after initialisation step made
    /*!
      Solution for some steps after initialisation step made. Thise function can be called only
      after the first call of the solve function
    */
    std::map<StateValueType,double> solveSteps ( const int iter );
  };
}


#include "Solver_src.hpp"


#endif
