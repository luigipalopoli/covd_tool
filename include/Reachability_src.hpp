#include <iomanip>
#include <fstream>
#include <set>
#include <map>
#include <utility>
#include <numeric>


template<typename StateValueType>
CovidTool::ReachabilityAnalyser<StateValueType>::ReachabilityAnalyser(const std::vector<StateValueType> & initialStates_d, CovidTool::StateSpace<StateValueType> & _ss_d):
  _reachedStates(),
  _ss(_ss_d),
  _initialStates(initialStates_d),
  _reachabilitytExecuted(false),
  _choices(0),
  _transComputed(false) {};


template<typename StateValueType>
CovidTool::ReachabilityAnalyser<StateValueType>::ReachabilityAnalyser( CovidTool::StateSpace<StateValueType> & _ss_d ):
  _reachedStates(),
  _ss(_ss_d),
  _initialStates(),
  _reachabilitytExecuted(false),
  _choices(0),
  _transComputed(false) {};

template<typename StateValueType>
unsigned CovidTool::ReachabilityAnalyser<StateValueType>::computeReachableStates(  const int maxIter ) {
  if ( _reachabilitytExecuted)
    return _reachedStates.size();

  if (_initialStates.size()==0)
    return 0;

  _ss.reset();
  _reachedStates.clear();
  VERBOSE_PRINT( "initialising state ", _verbose);
  // First pass insert initial states into state space.
  for ( auto el : _initialStates )
    {
      _reachedStates.insert(el);
      _ss.getStateNumber(el);
    }
  std::set<StateValueType> work;
  bool endLoop = false;

  work.clear();

  for (auto el : _reachedStates)
    work.insert(StateValueType{el});

  std::set<StateValueType> work1;

  for (int i = 0; (i < maxIter) && !endLoop; i++) {
    VERBOSE_PRINT( std::string("Executing iteration ") + std::to_string(i), _verbose);
    work1.clear();
    for (auto el : work) {

      std::map< CovidTool::NextState<StateValueType>, double > nxt = el.nextStates();

      for (auto el1 : nxt)  {
	StateValueType currSt = (el1.first).state;

	if ( _reachedStates.find(currSt) == _reachedStates.end() ) {
	  work1.insert(StateValueType(currSt));
	  _reachedStates.insert(StateValueType(currSt));
	  _ss.getStateNumber(StateValueType(currSt));
	}
      }
    }
    work.clear();
    work = work1;

    endLoop = (work1.size() == 0);
  }

  // for (unsigned i = 0; i < _ss.getNumberOfStates(); i ++) {
  //   StateValueType st = _ss.getStateValue( i );
  //   // TRACE(st.dump());
  // }

  assert(_reachedStates.size()==_ss.getNumberOfStates());
  _reachabilitytExecuted = true;

  return _reachedStates.size();
}

template<typename StateValueType>
template < typename Policy >
std::vector<CovidTool::Transition> CovidTool::ReachabilityAnalyser<StateValueType>::generateTransitionsWithReachable( bool filterTransition, Policy & pf, const int maxIter ) {

  if (_transComputed) {
    return _trans;
  }

  computeTransitionsWithReachable(filterTransition, pf, maxIter);

  return _trans;
}

template<typename StateValueType>
template < typename Policy >
void CovidTool::ReachabilityAnalyser<StateValueType>::computeTransitionsWithReachable( bool filterTransition, Policy & pf, const int maxIter ) {
  _ss.reset();
  _reachedStates.clear();
  _trans.clear();
  VERBOSE_PRINT( "initialising state ", _verbose);
  // First pass insert initial states into state space.
  for ( auto el : _initialStates )
    {
      _reachedStates.insert(el);
      _ss.getStateNumber(el);
    }

  std::set<StateValueType> work;
  bool endLoop = false;
  work.clear();
  std::set<unsigned> schoices;
  schoices.clear();
  _choices = 0;

  for (auto el : _reachedStates)
    work.insert(StateValueType{el});

  std::set<StateValueType> work1;

  for (int i = 0; (i < maxIter) && !endLoop; i++) {
    VERBOSE_PRINT( std::string("Executing iteration ") + std::to_string(i), _verbose);
    VERBOSE_PRINT( std::string("Number of states to explore ")+std::to_string(work.size()), _verbose);
    int count = 0;
    work1.clear();
    for (auto el : work) {

      std::map< CovidTool::NextState<StateValueType>, double > nxt;
      if (!filterTransition)
	nxt = el.nextStates();
      else
	nxt = el.nextStates(pf(el));

      VERBOSE_PRINT(std::string("Explored States ")+
		    std::to_string(count) +
		    std::string("/") +
		    std::to_string(work.size()),
		    (count % 50 == 0),
		    _verbose);
      count ++;
      schoices.clear();
      for (auto el1 : nxt)  {
	StateValueType currSt = (el1.first).state;

	if ( _reachedStates.find(currSt) == _reachedStates.end() ) {
	  work1.insert(StateValueType(currSt));
	  _reachedStates.insert(StateValueType(currSt));
	  _ss.getStateNumber(StateValueType(currSt));
	}
	_trans.push_back(CovidTool::Transition(_ss.getStateNumber(el),unsigned(el1.first.act), _ss.getStateNumber(StateValueType(currSt)), el1.second));
	schoices.insert(unsigned(el1.first.act));

      }
      _choices += schoices.size();
    }
    work.clear();
    work = work1;


    endLoop = (work1.size() == 0);
  }
  assert(_reachedStates.size()==_ss.getNumberOfStates());

  _reachabilitytExecuted = true;

  VERBOSE_PRINT( std::string("Transition table computed. Now sorting "), _verbose);

  std::sort(_trans.begin(),
	    _trans.end(),
	    [](const Transition & lhs, const Transition & rhs) {
	      if (lhs.sourceCode == rhs.sourceCode) {
		if (lhs.actionCode == rhs.actionCode)
		  return (lhs.destCode < rhs.destCode);
		else
		  return (lhs.actionCode < rhs.actionCode);
	      }
	      else
		return (lhs.sourceCode < rhs.sourceCode);
	    });
  VERBOSE_PRINT( std::string("Transition table sorting completed."), _verbose);

  _transComputed = true;
}

template<typename StateValueType>
std::vector<CovidTool::Transition> CovidTool::ReachabilityAnalyser<StateValueType>::generateTransitions () {

  if (!_reachabilitytExecuted)
    EXC_PRINT("Cannot compute transition table before computing reachable states");

  if (_transComputed) {
    return _trans;
  }

  _trans.clear();
  _choices = 0;

  if ( _reachedStates.size() == 0)
    return _trans;

  std::set<unsigned> schoices;
  schoices.clear();

  unsigned num = _ss.getNumberOfStates();
  for (unsigned i = 0; i < num; i ++) {
    StateValueType st = _ss.getStateValue( i );
    std::vector<Transition> work;

    work.clear();
    schoices.clear();

    auto nextS = st.nextStates();

    for ( auto el : nextS ) {
      // TRACE(_ss.getStateValue(i).dump());
      // TRACE(unsigned(el.first.act));
      // TRACE(el.first.state.dump());
      // TRACE(el.second);
      work.push_back(CovidTool::Transition(i, unsigned(el.first.act),
					   _ss.getStateNumber(el.first.state), el.second));
      schoices.insert(unsigned(el.first.act));
    }

    _choices += schoices.size();

    std::sort(work.begin(),
	      work.end(),
	      [](const Transition & lhs, const Transition & rhs) {
		if (lhs.actionCode == rhs.actionCode)
		  return (lhs.destCode < rhs.destCode);
		else
		  return (lhs.actionCode < rhs.actionCode);
	      });

    _trans.insert( _trans.end(), work.begin(), work.end());
  };

  _transComputed = true;

  return _trans;
}

template<typename StateValueType>
std::vector<CovidTool::Transition> CovidTool::ReachabilityAnalyser<StateValueType>::getTransitions () {
  if (!_reachabilitytExecuted)
    EXC_PRINT("Cannot get the transition table before computing the reachable states");

  if (_transComputed) {
    return _trans;
  }
  generateTransitions();
  assert(_transComputed);
  return _trans;
}

template<typename StateValueType>
unsigned CovidTool::ReachabilityAnalyser<StateValueType>::printReachableStates ( const int maxIter ) {
  std::cout<<"===============      STATES     =============================="<<std::endl;

  for (unsigned i = 0; i < _ss.getNumberOfStates(); i ++) {
    StateValueType st = _ss.getStateValue( i );
    std::cout<<std::setw(10)<<i<<": "<<st.data.dump()<<std::endl;
  }

  std::cout<<"===============      Actions     ============================"<<std::endl;
  for (unsigned i = SSV_::parms.getMinActionNumber(); i <= SSV_::parms.getActionNumber(); i ++)
    {
      typename StateValueType::ActionType act = typename StateValueType::ActionType(i);
      std::cout<<act.dump()<<std::endl;
    }


  std::cout<<"===============      Transitions     ========================="<<std::endl;

  std::vector<CovidTool::Transition> trans = generateTransitions ( );


  std::cout<<std::setw(10)<<"Source"<<std::setw(10)<<"Action"<<std::setw(10)<<"Dest."<<std::setw(20)<<"Probability"<<std::endl;

  for ( auto el: trans)
    std::cout<<std::setw(10)<<el.sourceCode<<std::setw(10)<<el.actionCode<<std::setw(10)<<el.destCode<<std::setw(20)<<el.prob<<std::endl;

  return _reachedStates.size();

}

template<typename StateValueType>
bool CovidTool::ReachabilityAnalyser<StateValueType>::dumpPrismExplicitModel(const std::string & name, bool isMDP, bool initT) {
  if (!_reachabilitytExecuted)
    EXC_PRINT("Cannot dump Prism Explicit Model before computing reachable states");

  if ( _reachedStates.size() == 0)
    return false;
  std::string statefile = name + std::string("_prism.sta");
  std::ofstream sfile(statefile.c_str());

  if (!sfile.is_open()) {
    EXC_PRINT_2("Unable to open file", statefile);
  }

  sfile << _ss.getStateValue(0).data.dumpPrismTupleSignature() << std::endl;
  // std::cout << "NUM first = " << _ss.getNumberOfStates() << std::endl;
  for (unsigned i = 0; i < _ss.getNumberOfStates(); i++) {
    StateValueType st = _ss.getStateValue(i);
    // sfile<<std::setw(10)<<i<<":"<<st.dumpPrismTupleValue()<<std::endl;
    sfile << i << ":" << st.data.dumpPrismTupleValue() << std::endl;
  }

  sfile.close();

  std::vector<Transition> trans = getTransitions();
  unsigned choices = getChoices();
  unsigned num = _ss.getNumberOfStates();

  std::string trafile = name + std::string("_prism.tra");
  std::ofstream tfile(trafile.c_str());

  if (!tfile.is_open()) {
    EXC_PRINT_2("Unable to open file", trafile);
  }
  // It requires as first line number of states, number of choices,
  // number of transitions
  tfile << num << " " << choices << " " << trans.size() << std::endl;

  // Then it requires "i k j x a" where i is the source state, j is
  // the destination state, k is the index of the choice it belongs
  // to, x is the probability, and a is optional and gives the
  // action label for the choice of the transition.
  for ( auto el: trans) {
    if (isMDP) {
      tfile << el.sourceCode << " " << el.actionCode << " "
	    << el.destCode << " " << el.prob;
      if (SSV_::parms.prism_dump_action_label)
	tfile << " act_M_is_"  << el.actionCode;
      tfile << std::endl;
    } else {
      // PRISM for DTMC does not want the label in the transitions, thus
      // we disregard the option and we do not print the labels
      tfile << el.sourceCode << " " << el.destCode
	    << " " << el.prob << std::endl;
    }
  }

  tfile.close();

  std::string labfile = name + std::string("_prism.lab");
  std::ofstream lfile(labfile.c_str());
  if (!lfile.is_open()) {
    EXC_PRINT_2("Unable to open file", labfile);
  }
  lfile << "0=\"init\" 1=\"deadlock\" 2=\"hospital_saturated\"" << std::endl;
  std::set<unsigned> in;
  for ( auto el : _initialStates ) {
    unsigned i = _ss.getStateNumber(el);
    in.insert(i);
  }
  for (unsigned i = 0; i < _ss.getNumberOfStates(); i++) {
    StateValueType sv = _ss.getStateValue(i);
    bool is_init = (initT || (in.find(i) != in.end()));
    bool is_hsat = sv.data.HospitalSaturated();
    if (is_init || is_hsat) {
      lfile << i << ":";
      if (is_init) lfile << " 0";
      if (is_hsat) lfile << " 2";
      lfile << std::endl;
    }
  }
  lfile.close();

  std::string staterewfile = name + std::string("_prism.srew");
  std::ofstream srfile(staterewfile.c_str());
  if (!srfile.is_open()) {
    EXC_PRINT_2("Unable to open file", staterewfile);
  }
  // It is a file with for each line: state_id reward
  srfile.close();

  std::string trarewfile = name + std::string("_prism.trew");
  std::ofstream trfile(trarewfile.c_str());
  if (!trfile.is_open()) {
    EXC_PRINT_2("Unable to open file", trarewfile);
  }
  // It is a file with for each line: source action destination reward
  trfile.close();

  return true;
}

template<typename StateValueType>
bool CovidTool::ReachabilityAnalyser<StateValueType>::dumpPrismModel(const std::string & name, bool isMDP, bool isPOMDP, bool initT) {

  if (!_reachabilitytExecuted)
    EXC_PRINT("Cannot dump Prism Symbolic Model before computing reachable states");

  if ( _reachedStates.size() == 0)
    return false;

  if (!isMDP && isPOMDP) return false;

  std::string osfile = name + std::string("_prism.nm");
  std::ofstream ofile(osfile.c_str());

  if (!ofile.is_open()) {
    EXC_PRINT_2("Unable to open file", osfile);
  }

  // It requires as first line the type of the model
  if (isMDP) {
    if (isPOMDP) {
      ofile << "pomdp" << std::endl << std::endl;
    } else {
      ofile << "mdp" << std::endl << std::endl;
    }
  } else {
    ofile << "dtmc" << std::endl << std::endl;
  }
  StateValueType st = _ss.getStateValue(0);

  if (isPOMDP) {
    ofile << "observables" << std::endl << "\t"
	  << st.data.dumpPrismObservables()
	  << std::endl << "endobservables" << std::endl;
  }
  if (isMDP) {
    ofile << "module covid_mdp" << std::endl;
  } else {
    ofile << "module covid_dtmc" << std::endl;
  }
  ofile << st.data.dumpPrismStateVariablesDeclaration() << std::endl;

  unsigned num = _ss.getNumberOfStates();
  for (unsigned i = 0; i < num; i ++) {
    std::map<unsigned, std::vector<std::pair<StateValueType,double>>> ns;
    StateValueType st = _ss.getStateValue( i );

    auto nextS = st.nextStates();
    for ( auto el : nextS ) {
      std::pair<StateValueType,double> p(el.first.state, double(el.second));
      unsigned k = unsigned(el.first.act);
      ns[k].push_back(p);
    }

    for (auto c : ns) {
      ofile << "[act_M_is_"<< c.first << "]  (" << st.data.dumpPrismSymbolicState() << ") -> ";
      bool first = true;
      for (auto n : c.second) {
	ofile << std::endl << "\t" ;
	ofile << (first ? "   " : " + ");
	ofile << n.second << " : " << n.first.data.dumpPrismSymbolicState("'");
	first = false;
      }
      ofile << ";" << std::endl << std::endl;
    }
  }

  ofile << "endmodule" << std::endl << std::endl;

  ofile << "init" << std::endl;
  if (initT) {
    // We consider all states as initial
    ofile << "(true)" << std::endl;
  } else {
    // We dump the specified initial states
    ofile << "(false " << std::endl;
    unsigned num_init = _initialStates.size();
    for ( auto el : _initialStates ) {
      ofile << " | " << el.data.dumpPrismSymbolicState() << std::endl;
    }
    ofile << ")" << std::endl;
  }
  ofile << "endinit" << std::endl << std::endl;

  if (_initialStates.size() > 0) {
    ofile << "label \"hospital_saturated\" = "
	  << _initialStates[0].data.HospitalSaturatedPrismCond()
	  << ";" << std::endl << std::endl;
  }

  ofile << "rewards" << std::endl;
  ofile << "\t [] true : 1;" << std::endl;
  ofile << "endrewards" << std::endl;

  ofile.close();
  return true;
}

template<typename StateValueType>
bool CovidTool::ReachabilityAnalyser<StateValueType>::dumpStormExplicitModel(const std::string & name, bool isMDP, bool initT) {
  if (!_reachabilitytExecuted)
    EXC_PRINT("Cannot dump Storm Explicit Model before computing reachable states");

  if ( _reachedStates.size() == 0)
    return false;

  std::vector<Transition> trans = getTransitions();
  unsigned choices = getChoices();
  unsigned num = _ss.getNumberOfStates();

  std::string trafile = name + std::string("_storm.tra");
  std::ofstream tfile(trafile.c_str());

  if (!tfile.is_open()) {
    EXC_PRINT_2("Unable to open file", trafile);
  }
  // It requires as first line the type of the model
  if (isMDP) {
    tfile << "mdp" << std::endl;
  } else {
    tfile << "dtmc" << std::endl;
  }

  // For MDPs it requires "i k j x" where i is the source state, j is
  // the destination state, k is the index of the choice it belongs
  // to, x is the probability. For DTMCs it requires "i j x" where i
  // is the source state, j is the destination state, x is the
  // probability, and no duplicates are allowed!
  if (isMDP) {
    for ( auto el: trans) {
      tfile << el.sourceCode << " " << el.actionCode << " "
	    << el.destCode << " " << el.prob << std::endl;
    }
  } else {
    for (unsigned i = 0; i < num; i ++) {
      std::map<unsigned, std::vector<double>> ns;
      StateValueType st = _ss.getStateValue( i );

      auto nextS = st.nextStates();
      for ( auto el : nextS ) {
	unsigned k = _ss.getStateNumber(el.first.state);
	ns[k].push_back(double(el.second));
      }

      for ( auto c : ns ) {
	tfile << i << " " << c.first << " ";
	double sum = std::accumulate(c.second.begin(), c.second.end(), 0.0, std::plus<double>());
	tfile << sum/c.second.size() << std::endl;
      }

#if 0
      auto cmp = [&] (const Transition & t1, const Transition & t2) {
	return ((t1.sourceCode == t2.sourceCode) &&
		(t1.destCode == t2.destCode));
      };

      trans.erase(std::unique(trans.begin(),trans.end(), cmp), trans.end());
      for ( auto el: trans) {
	tfile << el.sourceCode << " "
	      << el.destCode << " " << el.prob << std::endl;
      }
#endif
    }
  }
  tfile.close();

  std::string labfile = name + std::string("_storm.lab");
  std::ofstream lfile(labfile.c_str());
  if (!lfile.is_open()) {
    EXC_PRINT_2("Unable to open file", labfile);
  }
  lfile << "#DECLARATION" << std::endl;
  lfile << "init deadlock hospital_saturated" << std::endl;
  lfile << "#END" << std::endl;

  if (initT) {
    // We consider all states as initial
    for (unsigned i = 0; i < _ss.getNumberOfStates(); i++) {
      lfile << i << " init";
      StateValueType sv = _ss.getStateValue( i );
      if (sv.data.HospitalSaturated()) {
	lfile << " hospital_saturated";
      }
      lfile << std::endl;
    }
  } else {
    std::set<unsigned> in;
    // We dump the specified initial states
    for ( auto el : _initialStates ) {
      unsigned i = _ss.getStateNumber(el);
      in.insert(i);
      lfile << i << " init";
      if (el.data.HospitalSaturated()) {
	lfile << " hospital_saturated";
      }
      lfile << std::endl;
    }
    for (unsigned i = 0; i < _ss.getNumberOfStates(); i++) {
      if (in.find(i) == in.end()) {
	StateValueType sv = _ss.getStateValue( i );
	if (sv.data.HospitalSaturated()) {
	  lfile << i << " hospital_saturated" << std::endl;
	}
      }
    }
    in.clear();
  }
  lfile.close();

  std::string staterewfile = name + std::string("_storm.srew");
  std::ofstream srfile(staterewfile.c_str());
  if (!srfile.is_open()) {
    EXC_PRINT_2("Unable to open file", staterewfile);
  }
  // It is a file with for each line: state_id reward
  srfile.close();

  std::string trarewfile = name + std::string("_storm.trew");
  std::ofstream trfile(trarewfile.c_str());
  if (!trfile.is_open()) {
    EXC_PRINT_2("Unable to open file", trarewfile);
  }
  // It is a file with for each line: source action destination reward
  trfile.close();

  return true;
}

template<typename StateValueType>
bool CovidTool::ReachabilityAnalyser<StateValueType>::dumpStormDRNModel(const std::string & name, bool isMDP, bool isPOMDP, bool initT) {
  if (!_reachabilitytExecuted)
    EXC_PRINT("Cannot dump Storm DRN Model before computing reachable states");

  if ( _reachedStates.size() == 0)
    return false;

  std::vector<Transition> trans = getTransitions();
  unsigned choices = getChoices();
  unsigned num = _ss.getNumberOfStates();

  std::string drnfile = name + std::string("_storm.drn");
  std::ofstream dfile(drnfile.c_str());

  std::map<unsigned, unsigned> bs;
  unsigned int maxBS = 0;
  auto getBSNum = [&] (const unsigned s) {
    StateValueType st = _ss.getStateValue( s );
    unsigned base = st.data.parms.N; // i r o d
    unsigned code = st.data.i + base * st.data.r + \
                    base * base * st.data.o + base * base * st.data.d;
    auto it = bs.find(code);

    if (it != bs.end()) {
      return it->second;
    }
    unsigned res = maxBS;
    bs[code] = res;
    maxBS++;
    return res;
  };

  auto printBSNum = [&] (const unsigned s) {
    if (isPOMDP) {
      unsigned b = getBSNum(s);
      return std::string(" {") + std::to_string(b) + std::string("} ");
    } else {
      return std::string("");
    }
  };

  if (!dfile.is_open()) {
    EXC_PRINT_2("Unable to open file", drnfile);
  }

  dfile << "// Exported from covid tool" << std::endl;
  if (isMDP) {
    if (isPOMDP) {
      dfile << "@type: POMDP" << std::endl;
      dfile << "// storm-pomdp --buildchoicelab --explicit-drn "
	    << drnfile << std::endl;
    } else {
      dfile << "@type: MDP" << std::endl;
      dfile << "// storm --buildchoicelab --explicit-drn "
	    << drnfile << std::endl;
    }
  }
  else {
    dfile << "@type: DTMC" << std::endl;
  }
  dfile << "@parameters" << std::endl << std::endl
	<< "@reward_models" << std::endl << std::endl;

  std::set<unsigned> in;
  for ( auto el : _initialStates ) {
    unsigned i = _ss.getStateNumber(el);
    in.insert(i);
  }

  if (isMDP) {
    dfile << "@nr_states" << std::endl << num << std::endl
	  << "@nr_choices" << std::endl << getChoices() << std::endl;
    dfile << "@model" << std::endl;

    for (unsigned i = 0; i < num; i ++) {
      std::map<unsigned, std::vector<std::pair<unsigned,double>>> ns;
      StateValueType st = _ss.getStateValue( i );

      auto nextS = st.nextStates();
      for ( auto el : nextS ) {
	unsigned s = _ss.getStateNumber(el.first.state);
	unsigned k = unsigned(el.first.act);
	std::pair<unsigned,double> p(s, double(el.second));
	ns[k].push_back(p);
      }

      dfile << "state " << i << printBSNum(i);
      if (initT || (in.find(i) != in.end())) {
	dfile << " init";
      }
      if (st.data.HospitalSaturated()) {
	dfile << " hospital_saturated";
      }
      dfile << std::endl;
      dfile << "// [" << st.data.dumpPrismSymbolicState() << "]" << std::endl;

      for (auto c : ns) {
	dfile << "\taction " << unsigned(c.first - 1)<< std::endl;
	for (auto n : c.second) {
	  dfile << "\t\t" << n.first << " : " << n.second << std::endl;
	}
      }
    }
  }
  else {
    dfile << "@nr_states" << std::endl << num << std::endl
	  << "@nr_choices" << std::endl << num << std::endl;
    dfile << "@model" << std::endl;

    for (unsigned i = 0; i < num; i ++) {
      std::map<unsigned, std::vector<double>> ns;
      StateValueType st = _ss.getStateValue( i );

      auto nextS = st.nextStates();
      for ( auto el : nextS ) {
	unsigned k = _ss.getStateNumber(el.first.state);
	ns[k].push_back(double(el.second));
      }

      dfile << "state " << i;
      if (initT || (in.find(i) != in.end())) {
	dfile << " init";
      }
      if (st.data.HospitalSaturated()) {
	dfile << " hospital_saturated";
      }
      dfile << std::endl;
      dfile << "// [" << st.data.dumpPrismSymbolicState() << "]" << std::endl;
      dfile << "\taction 0" << std::endl;
      for ( auto c : ns ) {
	dfile << "\t\t" << c.first << " : ";
	double sum = std::accumulate(c.second.begin(), c.second.end(), 0.0, std::plus<double>());
	dfile << sum/c.second.size() << std::endl;
      }
    }
  }

  return true;
}

template<typename StateValueType>
void CovidTool::ReachabilityAnalyser<StateValueType>::setInitialStates(const std::vector<StateValueType> & initialStates_d ) {
  _initialStates = initialStates_d;
  _reachabilitytExecuted = false;
  _choices = 0;
  _transComputed = false;
}


template<typename StateValueType>
void CovidTool::ReachabilityAnalyser<StateValueType>::setStateSpace(  CovidTool::StateSpace<StateValueType> & _ss_d ) {
  _ss = _ss_d;
  _reachabilitytExecuted = false;
  _choices = 0;
  _transComputed = false;
}

template<typename StateValueType>
void CovidTool::ReachabilityAnalyser<StateValueType>::setVerbose( ) {
  _verbose = true;
}

template<typename StateValueType>
void CovidTool::ReachabilityAnalyser<StateValueType>::setParameters(const CovidTool::ReachabilityAnalyser<StateValueType>::Parameters & p_d)
{
  StateValueType::setParameters(p_d.sp);


  setInitialStates(p_d.initialStates);
  for (int i = 0; i < _initialStates.size(); i++) {
    _initialStates.at(i).data.setCode();
    if ( !_initialStates.at(i).checkConsistency() )
      EXC_PRINT("Inconsistent Initial states");
  }
}




//Inline methods of the Parameters structure
template<typename StateValueType>
inline void CovidTool::ReachabilityAnalyser<StateValueType>::Parameters::to_json(nlohmann::json &j) const {
  nlohmann::json work;
  j["maxReachIter"] = maxReachIter;

  for ( auto el: initialStates )
    work.push_back(el);
  j["initialStates"] = work;

  nlohmann::json work2;
  sp.to_json( work2);
  j["sp"] = work2;

  return;
}


template<typename StateValueType>
inline void CovidTool::ReachabilityAnalyser<StateValueType>::Parameters::from_json(const nlohmann::json &j) {
  nlohmann::json work0 = j.at("maxReachIter");
  nlohmann::from_json(work0,  maxReachIter );

  nlohmann::json work = j.at("initialStates");
  initialStates.clear();
  for (auto el : work) {
    StateValueType v;
    v.from_json(el);
    initialStates.push_back(v);
  }

  nlohmann::json work2 = j.at("sp");
  sp.from_json(work2);


  return;
}
