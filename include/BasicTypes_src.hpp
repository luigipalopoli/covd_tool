#include <fstream>

template<typename DataStruct>
std::map<CovidTool::StateWrapper<DataStruct>, std::map<CovidTool::NextState<CovidTool::StateWrapper<DataStruct>>, double> > CovidTool::StateWrapper<DataStruct>::_exploredStates;


template<typename DataStruct>
void CovidTool::StateWrapper<DataStruct>::printReachable() {
  auto m = nextStates();
  std::cerr<<"Reachable states for state "<<data.dump()<<std::endl;

  for (auto it =  m.begin(); it != m.end(); it++)
    {
      auto el = *it;
      std::string stateSignature = ((el.first).state).data.dump();
      std::cout<<"       State: "<<stateSignature<<", Action: "<<unsigned((el.first).act)<<",    Probability: "<<(el.second)<<std::endl;
    }
}

template<typename DataStruct>
void CovidTool::StateWrapper<DataStruct>::setParameters(const typename DataStruct::Parameters &ps_p) {
  ps_p.checkConsistency();


  DataStruct::parms = ps_p;
  DataStruct::parmsSet = true;
  parms = DataStruct::parms;
  _exploredStates.clear();

}

template<typename DataStruct>
std::map< CovidTool::NextState< CovidTool::StateWrapper<DataStruct> >, double>  CovidTool::StateWrapper<DataStruct>::nextStates() {
  if (! DataStruct::parmsSet )
    EXC_PRINT("Cannot compute nextState if you did not initialise the parameters");
#ifndef NO_CACHE_EXPLORED_STATES
  auto it = _exploredStates.find(*this);
  if ( it != _exploredStates.end() )
    return it->second;
#endif
  std::map < CovidTool::NextState<StateWrapper<DataStruct> >, double >  res;
  for ( unsigned code = DataStruct::parms.getMinActionNumber(); code <= DataStruct::parms.getActionNumber(); code++ ) {
    auto m = nextStates(code);
    for (auto it1 = m.begin(); it1 != m.end(); it1++)
      res.insert(*it1);

    //res.insert(m.begin(), m.end());
    //res.merge(m);
  }
#ifndef NO_CACHE_EXPLORED_STATES
  auto insOk = _exploredStates.insert(std::pair<StateWrapper<DataStruct> , std::map< CovidTool::NextState<StateWrapper<DataStruct>>, double > >  (*this, res));
  if ( insOk.second == false )
    EXC_PRINT("Could not create map entry.");
#endif
  return res;
}

template<typename DataStruct>
std::map< CovidTool::NextState<CovidTool::StateWrapper<DataStruct>>, double >  CovidTool::StateWrapper<DataStruct>::nextStates(const typename DataStruct::ActionType & action) {
  if (!DataStruct::parmsSet )
    EXC_PRINT("Cannot compute nextState if you did not initialise the parameters");

  if ( !data.checkConsistency() ) {
    std::string msg = data.dump();
    TRACE(msg);
    EXC_PRINT("Cannot compute next function for inconsistent state");
  }
  std::map< CovidTool::NextState<DataStruct>, double > work = data.nextStateDirect( action );


  double totProb = std::accumulate(work.begin(),
				   work.end(),
				   0.0,
				   [] (double sum, const std::pair<NextState<DataStruct>,double> & item) {
				     return sum + item.second;
				   });
  assert(abs(totProb-1)<DataStruct::parms.eps_tot_prob);

  erase_if(work,
  	   [] (const std::pair<CovidTool::NextState<DataStruct>,double> & item) {
  	     return item.second < DataStruct::parms.eps_prob;
  	   });
  totProb = std::accumulate(work.begin(),
			    work.end(),
			    0.0,
			    [] (double sum, const std::pair<NextState<DataStruct>,double> & item) {
			      return sum + item.second;
			    });
  std::map< CovidTool::NextState<CovidTool::StateWrapper<DataStruct>>, double > wrappedMap;
  for (auto el : work) {
    el.second /= totProb;
    wrappedMap.insert(std::pair<CovidTool::NextState<CovidTool::StateWrapper<DataStruct> >, double>(
												    CovidTool::NextState<CovidTool::StateWrapper<DataStruct> >(el.first.act, CovidTool::StateWrapper<DataStruct>(el.first.state)), el.second));
  }

// #ifndef NDEBUG
//   totProb = std::accumulate(work.begin(),
// 			    work.end(),
// 			    0.0,
// 			    [] (double sum, const std::pair<NextState<DataStruct>,double> & item) {
// 			      return sum + item.second;
// 			    });
//   assert(abs(totProb-1)<DataStruct::parms.eps_tot_prob);
// #endif

  return wrappedMap;
}

///////////////////////////////////////////////////////////
/// Inlined functions for the SSV_ class                //
/// and for its components                              //
//////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////

// Inline functions of SSV_::Parameters

inline void CovidTool::SSV_::Parameters::to_json(nlohmann::json &j) const {
  j = nlohmann::json {
    TO_JSON(N),
    TO_JSON(C),
    TO_JSON(omega),
    TO_JSON(beta),
    TO_JSON(delta),
    TO_JSON(mu),
    TO_JSON(psi),
    TO_JSON(alpha),
    TO_JSON(sigma),
    TO_JSON(xi),
    TO_JSON(M),
    TO_JSON(Mmin),
    TO_JSON(eps_tot_prob),
    TO_JSON(eps_prob),
    TO_JSON(prism_dump_action_label)
  };
  return;
}

inline void CovidTool::SSV_::Parameters::from_json(const nlohmann::json &j) {
  FROM_JSON(j, N);
  FROM_JSON(j, C);
  FROM_JSON(j, omega);
  FROM_JSON(j, beta);
  FROM_JSON(j, delta);
  FROM_JSON(j, mu);
  FROM_JSON(j, psi);
  FROM_JSON(j, alpha);
  FROM_JSON(j, sigma);
  FROM_JSON(j, xi);
  FROM_JSON(j, M);
  FROM_JSON(j, Mmin);
  FROM_JSON(j, eps_tot_prob);
  FROM_JSON(j, eps_prob);
  FROM_JSON(j, prism_dump_action_label);
  return;
}

//Inlined functions of ActionType

inline CovidTool::SSV_::AA::AA(): value(0) {};

inline CovidTool::SSV_::AA::AA(unsigned v): value(v)
{
  if (!CovidTool::SSV_::parmsSet)
    EXC_PRINT("cannot instiantiate an action before knowing the admissible range");
  if (v > CovidTool::SSV_::parms.M )
    EXC_PRINT("Wrong type");
  if (v < CovidTool::SSV_::parms.Mmin )
    EXC_PRINT("Wrong type");
};

inline CovidTool::SSV_::AA::operator unsigned() const {
  return value;
}

inline void CovidTool::SSV_::AA::to_json(nlohmann::json &j) const {
  j = nlohmann::json {
    TO_JSON(value)
  };
}

inline void CovidTool::SSV_::AA::from_json(const nlohmann::json &j)
{
  FROM_JSON(j, value);
}

inline std::string CovidTool::SSV_::AA::dump() const {
  std::string msg = DUMP_VAR(value);
  msg += std::string(": ( M =");
  msg += DUMP_VAR(value);
  msg += std::string(" )");
  return msg;
}

inline std::string CovidTool::SSV_::AA::dumpPrismName() const {
  std::string msg = std::string("act_M_is_");
  msg += std::to_string(value);

  return msg;
}

inline std::string CovidTool::SSV_::AA::dumpPrismId() const {
  return std::to_string(value);
}


//Inlined functions of SSV_

inline CovidTool::SSV_::SSV_(unsigned s_p, unsigned a_p, unsigned i_p, unsigned r_p, unsigned o_p, unsigned d_p ):
  s(s_p),
  a(a_p),
  i(i_p),
  r(r_p),
  o(o_p),
  d(d_p) {
  if (parmsSet)
    setCode();
  };

inline CovidTool::SSV_::SSV_( ):
  s(0),
  a(0),
  i(0),
  r(0),
  o(0),
  d(0) {
  };

inline void CovidTool::SSV_::to_json(nlohmann::json &j) const {
  j = nlohmann::json {
    TO_JSON(s),
    TO_JSON(a),
    TO_JSON(i),
    TO_JSON(r),
    TO_JSON(o),
    TO_JSON(d)
  };
  return;
}
inline void CovidTool::SSV_::from_json(const nlohmann::json & j) {
  FROM_JSON(j, s);
  FROM_JSON(j, a);
  FROM_JSON(j, i);
  FROM_JSON(j, r);
  FROM_JSON(j, o);
  FROM_JSON(j, d);

  return;
}

inline unsigned CovidTool::SSV_::getSymptomatic() const {
  return o + i;
}

inline unsigned CovidTool::SSV_::getAsymptomatic() const {
  return a;
}

inline std::string CovidTool::SSV_::dump() const {
  const std::string C = ",";
  std::string msg = std::string("(");
  msg += DUMP_VAR(s) + C;
  msg += DUMP_VAR(a) + C;
  msg += DUMP_VAR(i) + C;
  msg += DUMP_VAR(r) + C;
  msg += DUMP_VAR(o) + C;
  msg += DUMP_VAR(d);
  msg += std::string(")");

  return msg;
}

inline std::string CovidTool::SSV_::dumpPrismTupleSignature() const {
  return std::string("(nS, nA, nI, nR, nO, nD)");
}

inline std::string CovidTool::SSV_::dumpPrismTupleValue() const {
  const std::string C = ",";
  std::string msg = std::string("(");
  msg += std::to_string(s) + C;
  msg += std::to_string(a) + C;
  msg += std::to_string(i) + C;
  msg += std::to_string(r) + C;
  msg += std::to_string(o) + C;
  msg += std::to_string(d);
  msg += std::string(")");
  return msg;
}

inline std::string CovidTool::SSV_::dumpPrismStateVariablesDeclaration() const {
  std::string msg = std::string("");
  msg += std::string("nS : [0..") + std::to_string(parms.N) + std::string("];\n");
  msg += std::string("nA : [0..") + std::to_string(parms.N) + std::string("];\n");
  msg += std::string("nI : [0..") + std::to_string(parms.N) + std::string("];\n");
  msg += std::string("nR : [0..") + std::to_string(parms.N) + std::string("];\n");
  msg += std::string("nO : [0..") + std::to_string(parms.N) + std::string("];\n");
  msg += std::string("nD : [0..") + std::to_string(parms.N) + std::string("];\n");
  return msg;
}

inline std::string CovidTool::SSV_::dumpPrismObservables() const {
  std::string msg = std::string("\tnI, nR, nO, nD");
  return msg;
}

inline std::string CovidTool::SSV_::dumpPrismSymbolicState(const char * ss ) const {
  std::string msg = std::string("");
  msg += std::string("(nS") + std::string(ss) + std::string(" = ") + std::to_string(s) + std::string(")");
  msg += std::string(" & ");
  msg += std::string("(nA") + std::string(ss) + std::string(" = ") + std::to_string(a) + std::string(")");
  msg += std::string(" & ");
  msg += std::string("(nI") + std::string(ss) + std::string(" = ") + std::to_string(i) + std::string(")");
  msg += std::string(" & ");
  msg += std::string("(nR") + std::string(ss) + std::string(" = ") + std::to_string(r) + std::string(")");
  msg += std::string(" & ");
  msg += std::string("(nO") + std::string(ss) + std::string(" = ") + std::to_string(o) + std::string(")");
  msg += std::string(" & ");
  msg += std::string("(nD") + std::string(ss) + std::string(" = ") + std::to_string(d) + std::string(")");
  msg += std::string("");
  return msg;
}

inline bool CovidTool::SSV_::HospitalSaturated() const {
  return (o == parms.C);
}

inline std::string CovidTool::SSV_::HospitalSaturatedPrismCond() const {
  std::string msg = std::string("(");
  msg += "nO = " + std::to_string(parms.C);
  msg += std::string(")");
  return msg;
}

inline unsigned CovidTool::SSV_::getCode() const {
  return code;
};


inline unsigned CovidTool::SSV_::setCode() {
  unsigned base = parms.N;
  code = s + a * base + i * base* base + r * base * base * base + o * base * base * base *base + d * base *base *base *base*base;

  return code;
};

inline void CovidTool::pmf2cdf(const std::vector<double> & pmf, std::vector<double> & cdf) {
  double count = 0;
  cdf.clear();
  for ( auto it = pmf.begin(); it != pmf.end(); it++ ) {
    count += *it;
    cdf.push_back(count);
  }
  return;
}

template<typename Policy>
std::pair< std::vector< std::vector<double> >, std::vector<double> > CovidTool::marginalProbabilities(const std::map<CovidTool::SSV,double> & jointProb, Policy & pf, bool save, const char * filePrefix ) {
  std::vector< std::vector<double> >  workPmf{
    std::vector<double>(CovidTool::SSV_::parms.N + 1, 0),
      std::vector<double>(CovidTool::SSV_::parms.N + 1, 0),
      std::vector<double>(CovidTool::SSV_::parms.N + 1, 0),
      std::vector<double>(CovidTool::SSV_::parms.N + 1, 0),
      std::vector<double>(CovidTool::SSV_::parms.N + 1, 0),
      std::vector<double>(CovidTool::SSV_::parms.N + 1, 0)};
  std::vector<double> workAPmf(CovidTool::SSV_::parms.M + 1, 0);

  std::vector< std::vector<double> >  workCdf(6);
  std::vector<double> workACdf;


  for ( auto el : jointProb ) {
    (workPmf.at(static_cast<int>(CovidTool::SSV_::StateNames::Susceptible))).at(el.first.data.s) += el.second;
    (workPmf.at(static_cast<int>(CovidTool::SSV_::StateNames::Asymptomatic))).at(el.first.data.a) += el.second;
    (workPmf.at(static_cast<int>(CovidTool::SSV_::StateNames::Symptomatic))).at(el.first.data.i) += el.second;
    (workPmf.at(static_cast<int>(CovidTool::SSV_::StateNames::Recovered))).at(el.first.data.r) += el.second;
    (workPmf.at(static_cast<int>(CovidTool::SSV_::StateNames::Hospitalised))).at(el.first.data.o) += el.second;
    (workPmf.at(static_cast<int>(CovidTool::SSV_::StateNames::Deceased))).at(el.first.data.d) += el.second;

    workAPmf.at(unsigned(pf(el.first))) += el.second;
  }
  for (int i = static_cast<int>(CovidTool::SSV_::StateNames::Susceptible); i!= static_cast<int>(CovidTool::SSV_::StateNames::End); i++)
    CovidTool::pmf2cdf(workPmf.at(i), workCdf.at(i ));

  // HERE;
  // CovidTool::pmf2cdf(workPmf.at(static_cast<int>(CovidTool::SSV_::StateNames::Susceptible)), workCdf.at(static_cast<int>(CovidTool::SSV_::StateNames::Susceptible)));
  // HERE;
  // CovidTool::pmf2cdf(workPmf.at(static_cast<int>(CovidTool::SSV_::StateNames::Asymptomatic)), workCdf.at(static_cast<int>(CovidTool::SSV_::StateNames::Asymptomatic)));
  // CovidTool::pmf2cdf(workPmf.at(static_cast<int>(CovidTool::SSV_::StateNames::Symptomatic)), workCdf.at(static_cast<int>(CovidTool::SSV_::StateNames::Symptomatic)));
  // CovidTool::pmf2cdf(workPmf.at(static_cast<int>(CovidTool::SSV_::StateNames::Recovered)), workCdf.at(static_cast<int>(CovidTool::SSV_::StateNames::Recovered)));
  // CovidTool::pmf2cdf(workPmf.at(static_cast<int>(CovidTool::SSV_::StateNames::Hospitalised)), workCdf.at(static_cast<int>(CovidTool::SSV_::StateNames::Hospitalised)));
  // CovidTool::pmf2cdf(workPmf.at(static_cast<int>(CovidTool::SSV_::StateNames::Deceased)), workCdf.at(static_cast<int>(CovidTool::SSV_::StateNames::Deceased)));

  CovidTool::pmf2cdf(workAPmf, workACdf );

  if (save) {
    std::string policyId = pf.getId();
    CovidTool::saveProbs(workCdf, workACdf, std::string(filePrefix), policyId);
    CovidTool::saveProbs(workPmf, workAPmf, std::string("PMF_")+std::string(filePrefix), policyId);
  }

  return std::pair< std::vector< std::vector<double> >, std::vector<double> >(workCdf, workACdf);
};

///////////////////////////////////
/// Methods of ESV_
//////////////////////////////////

//Inline methods for ESV_::Parameters
inline void CovidTool::ESV_::Parameters::to_json(nlohmann::json &j) const {
  j = nlohmann::json {
    TO_JSON( baseParms ),
    TO_JSON( gamma ),
    TO_JSON ( iota),
    TO_JSON ( upsilon),
    TO_JSON ( T),
    TO_JSON ( Tmin)
  };
  return;
}

inline void CovidTool::ESV_::Parameters::from_json(const nlohmann::json &j) {
  FROM_JSON(j, baseParms);
  FROM_JSON(j, gamma);
  FROM_JSON(j, iota);
  FROM_JSON(j, upsilon);
  FROM_JSON(j, T);
  FROM_JSON(j, Tmin);
  return;
}


//Inline methods for ESV_
//Inlined functions of SSV_

inline CovidTool::ESV_::ESV_(unsigned s_p, unsigned a_p, unsigned i_p, unsigned r_p, unsigned o_p, unsigned d_p, unsigned q_p, unsigned ra_p ):
  s(s_p),
  a(a_p),
  i(i_p),
  r(r_p),
  o(o_p),
  d(d_p),
  q(q_p),
  ra(ra_p)
{
  unsigned base = parms.baseParms.N;
  code = s + a * base + i * base* base + r * base * base * base + o * base * base * base *base + d * base *base *base *base*base + q * base *base *base *base*base*base+ ra* base *base *base *base * base* base* base ;
};

inline CovidTool::ESV_::ESV_( ):
  s(0),
  a(0),
  i(0),
  r(0),
  o(0),
  d(0),
  q(0),
  ra(0)
{
};

inline void CovidTool::ESV_::to_json(nlohmann::json &j) const {
  j = nlohmann::json {
    TO_JSON(s),
    TO_JSON(a),
    TO_JSON(i),
    TO_JSON(r),
    TO_JSON(o),
    TO_JSON(d),
    TO_JSON(q),
    TO_JSON(ra)
  };
  return;
}
inline void CovidTool::ESV_::from_json(const nlohmann::json & j) {
  FROM_JSON(j, s);
  FROM_JSON(j, a);
  FROM_JSON(j, i);
  FROM_JSON(j, r);
  FROM_JSON(j, o);
  FROM_JSON(j, d);
  FROM_JSON(j, q);
  FROM_JSON(j, ra);

  return;
}

inline std::string CovidTool::ESV_::dump() const {
  const std::string C = ",";
  std::string msg = std::string("(");
  msg += DUMP_VAR(s) + C;
  msg += DUMP_VAR(a) + C;
  msg += DUMP_VAR(i) + C;
  msg += DUMP_VAR(r) + C;
  msg += DUMP_VAR(o) + C;
  msg += DUMP_VAR(d) + C;
  msg += DUMP_VAR(q) + C;
  msg += DUMP_VAR(ra) ;
  msg += std::string(")");

  return msg;
}

inline std::string CovidTool::ESV_::dumpPrismTupleSignature() const {
  return std::string("(nS, nA, nI, nR, nO, nD, nQ, nRa)");
}

inline std::string CovidTool::ESV_::dumpPrismTupleValue() const {
  const std::string C = ",";
  std::string msg = std::string("(");
  msg += std::to_string(s) + C;
  msg += std::to_string(a) + C;
  msg += std::to_string(i) + C;
  msg += std::to_string(r) + C;
  msg += std::to_string(o) + C;
  msg += std::to_string(d) + C;
  msg += std::to_string(q) + C;
  msg += std::to_string(ra);

  msg += std::string(")");
  return msg;
}

inline std::string CovidTool::ESV_::dumpPrismStateVariablesDeclaration() const {
  std::string msg = std::string("");
  msg += std::string("nS : [0..") + std::to_string(parms.baseParms.N) + std::string("];\n");
  msg += std::string("nA : [0..") + std::to_string(parms.baseParms.N) + std::string("];\n");
  msg += std::string("nI : [0..") + std::to_string(parms.baseParms.N) + std::string("];\n");
  msg += std::string("nR : [0..") + std::to_string(parms.baseParms.N) + std::string("];\n");
  msg += std::string("nO : [0..") + std::to_string(parms.baseParms.N) + std::string("];\n");
  msg += std::string("nD : [0..") + std::to_string(parms.baseParms.N) + std::string("];\n");
  msg += std::string("nQ : [0..") + std::to_string(parms.baseParms.N) + std::string("];\n");
  msg += std::string("nRa : [0..") + std::to_string(parms.baseParms.N) + std::string("];\n");
  return msg;
}

inline std::string CovidTool::ESV_::dumpPrismObservables() const {
  std::string msg = std::string("\tnI, nR, nO, nD, nQ");
  return msg;
}

inline std::string CovidTool::ESV_::dumpPrismSymbolicState(const char * ss ) const {
  std::string msg = std::string("");
  msg += std::string("(nS") + std::string(ss) + std::string(" = ") + std::to_string(s) + std::string(")");
  msg += std::string(" & ");
  msg += std::string("(nA") + std::string(ss) + std::string(" = ") + std::to_string(a) + std::string(")");
  msg += std::string(" & ");
  msg += std::string("(nI") + std::string(ss) + std::string(" = ") + std::to_string(i) + std::string(")");
  msg += std::string(" & ");
  msg += std::string("(nR") + std::string(ss) + std::string(" = ") + std::to_string(r) + std::string(")");
  msg += std::string(" & ");
  msg += std::string("(nO") + std::string(ss) + std::string(" = ") + std::to_string(o) + std::string(")");
  msg += std::string(" & ");
  msg += std::string("(nD") + std::string(ss) + std::string(" = ") + std::to_string(d) + std::string(")");
  msg += std::string("(nQ") + std::string(ss) + std::string(" = ") + std::to_string(q) + std::string(")");
  msg += std::string("(nRa") + std::string(ss) + std::string(" = ") + std::to_string(ra) + std::string(")");

  msg += std::string("");
  return msg;
}

inline bool CovidTool::ESV_::HospitalSaturated() const {
  return (o == parms.baseParms.C);
}

inline std::string CovidTool::ESV_::HospitalSaturatedPrismCond() const {
  std::string msg = std::string("(");
  msg += "nO = " + std::to_string(parms.baseParms.C);
  msg += std::string(")");
  return msg;
}

inline unsigned CovidTool::ESV_::getCode() const {
  return code;
};

inline CovidTool::ESV_::AA::AA(): meetings(0), tests(0) {};

inline CovidTool::ESV_::AA::AA(unsigned meetings_p,  unsigned tests_p ): meetings(meetings_p), tests(tests_p)
{
  if (!CovidTool::SSV_::parmsSet)
    EXC_PRINT("cannot instiantiate an action before knowing the admissible range");
  if (meetings > CovidTool::ESV_::parms.baseParms.M )
    EXC_PRINT("Wrong type");
  if (meetings < CovidTool::ESV_::parms.baseParms.Mmin )
    EXC_PRINT("Wrong type");
  if (tests > CovidTool::ESV_::parms.T )
    EXC_PRINT("Wrong type");
  if (tests < CovidTool::ESV_::parms.Tmin )
    EXC_PRINT("Wrong type");
};

inline CovidTool::ESV_::AA::operator unsigned() const {
  return meetings + std::max(CovidTool::ESV_::parms.baseParms.M, CovidTool::ESV_::parms.T)*tests;
}

inline void CovidTool::ESV_::AA::to_json(nlohmann::json &j) const  {
  j = nlohmann::json {
    TO_JSON(meetings),
    TO_JSON(tests)
  };
};


inline void CovidTool::ESV_::AA::from_json(const nlohmann::json &j) {
  FROM_JSON(j, meetings);
  FROM_JSON(j, tests);
};


inline std::string CovidTool::ESV_::AA::dump() const {
  std::string C = std::string(",");
  unsigned code = unsigned(*this);
  std::string msg = DUMP_VAR(code);
  msg += std::string(": ( M =");
  msg += DUMP_VAR(meetings);
  msg += C;
  msg += std::string(" T =");
  msg += DUMP_VAR(tests);
  msg += std::string(" )");
  return msg;
}

inline std::string CovidTool::ESV_::AA::dumpPrismName() const {
  std::string msg = std::string("act_M_is_");
  msg += std::to_string(meetings);
  msg+= std::string("_T_is_");
  msg += std::to_string(tests);
  return msg;
}

inline std::string CovidTool::ESV_::AA::dumpPrismId() const {
  return std::to_string(unsigned(*this));
}



inline unsigned CovidTool::ESV_::getSymptomatic() const {
  return o + i+ q;
}

inline unsigned CovidTool::ESV_::getAsymptomatic() const {
  return a;
}

///////////////////////////////
// Method of StateSpace
//////////////////////////////
template<typename StateValueType, unsigned MaxId>
CovidTool::StateSpace<StateValueType, MaxId>::StateSpace() : _maxValue(0), _valueToNum(), _numToValue() {
};

template<typename StateValueType, unsigned MaxId>
unsigned CovidTool::StateSpace<StateValueType, MaxId>::getStateNumber( const StateValueType & sv ) {
  auto it = _valueToNum.find(sv);

  if ( it != _valueToNum.end() ) {
    return it->second;
  }
  auto insOk = _valueToNum.insert(std::pair<StateValueType, unsigned>(sv, _maxValue));
  if ( insOk.second == false )
    EXC_PRINT("Could not create map entry.");

  _numToValue.push_back(sv);

  if (_maxValue > MaxId)
    EXC_PRINT("reached the limit of the data structure");

  return _maxValue++;
}

template<typename StateValueType, unsigned MaxId>
StateValueType CovidTool::StateSpace<StateValueType, MaxId>::getStateValue( const unsigned  si ) const {
  if ( si >= _maxValue )
    EXC_PRINT("Critical error! Looked up for a state idnetifie that does not exit");

  return _numToValue.at( si );
}

template<typename StateValueType, unsigned MaxId>
void CovidTool::StateSpace<StateValueType, MaxId>::reset() {
  _valueToNum.clear();
  _numToValue.clear();
  _maxValue = 0;
};

template<typename StateValueType, unsigned MaxId>
unsigned CovidTool::StateSpace<StateValueType, MaxId>::getNumberOfStates() {
  return _maxValue;
};


// Transition methods
inline CovidTool::Transition::Transition(unsigned sourceCode_d, unsigned actionCode_d, unsigned destCode_d, double prob_d):
  sourceCode(sourceCode_d),
  actionCode(actionCode_d),
  destCode(destCode_d),
  prob(prob_d)
{
  CHECK_PROB(prob_d);
};
