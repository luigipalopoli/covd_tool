/*
 * Header file containing the basic types used for analysis and synthesis
 *
 * Copyright (C) 2020, University of Trento
 * Authors: Luigi Palopoli <luigi.palopoli@unitn.it>, Marco Roveri <marco.roveri@unitn.it>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
/** \file BasicTypes.hpp
 * Header file containing the basic types used for analysis and synthesis
 */
#ifndef _BASIC_TYPES_HPP
#define _BASIC_TYPES_HPP

#include <vector>
#include <map>
#include <string>
#include "util/exc.hpp"
#include "util/Serial.hpp"
#include "util/CheckSet.hpp"
#include "util/algo.hpp"
namespace CovidTool {
  /*! \brief General Data Structure for encapsulating the next state
   *
   * This is a template data structure that contains one of the next state reachable
   * from the current state
   * \tparam StateType Type of the state the structure encapsulates
   */
  template<typename StateType>
  struct NextState {
    typename StateType::ActionType act; /*!< Action associated to the state transition */
    StateType state;                    /*!< Nest state reached by the transition */

    //! Constructor of NextState
    /*!
      \param act_f action associated with the transition
      \param state_f next state associated with the transition
    */
    NextState( const typename StateType::ActionType & act_f, const StateType &state_f): act(act_f), state(state_f) {};
  };


  //! Template function to compare two NextState structure
  /*! The function is used to orgniase the NextState as a map (for which comparisong is needed).
    The ordering between the structure is by first looking at the actions (which in turn
    are assumed totally ordered and then at the state component.
    \tparam StateType Type of the state enncapsulated by NextState
    \param lhs left hand side member of the comparison
    \param rhs right hand side of the comparison
    \return true is lhs < rhs, false otherwise
  */
  template<typename StateType>
  inline bool operator< (const CovidTool::NextState<StateType> & lhs, const CovidTool::NextState<StateType> & rhs)
  {
    if (unsigned(lhs.act) == unsigned(rhs.act))
      return (lhs.state < rhs.state);
    return (unsigned(lhs.act) < unsigned(rhs.act));
  };


  /*! \brief This class encapsulates the data structure implementing some common services
   *
   * This is a template data structure that encapsulates a data structure encoding the
   * state in order to implement some basic services related to the search of the next states.
   * \tparam DataStruct Type of the state the structure encapsulates
   */
  template<typename DataStruct>
  struct StateWrapper {

    //! \brief Constructor of the StateWrapper
    /*!
      Constructs the StateWrapper out of an instance of the
      encapsulated DataStruct
      \param d instance of the DataStruct that is encapsulated.
    */
    StateWrapper(const DataStruct & d): data(d) {
    };

    //! \brief Default Constructor of the StateWrapper
    /*!
      The Default Constructor of the StateWrapper generates an instance
      Containing an empty encapsulated data structure.
    */
    StateWrapper( ): data( ) {};



    //! \brief returns the set of possible next states associated with an action (identified by the code)
    /*!
      Returns the set of possible next states associated with an action (identified by the code)
      \param code numeric code of the action
      \return a map associating each transition with its probability
    */
    typename std::map< CovidTool::NextState< StateWrapper<DataStruct> >, double > nextStates( unsigned code ) {
      return nextStates( typename DataStruct::ActionType(code) );
    }

    //! \brief Returns the set of possible next states associated with an action
    /*!
      Returns the set of possible next states associated with an action (identified through its type)
      \param action action described by its type
      \return a map associating each transition with its probability
    */
    typename std::map< CovidTool::NextState< StateWrapper<DataStruct> >, double >  nextStates(const typename DataStruct::ActionType & action);

    //! \brief returns the set of possible next states associated with all possible actions
    /*!
      It scans all possible actions and for each it computes the next states
      \return a map associating each transition with its probability
    */
    typename std::map< CovidTool::NextState< StateWrapper<DataStruct> >, double> nextStates();

    //! \brief Sets the parameters used for all the states belogning to DataStruct
    /*!
      Each type of state is associated with a parameter description. This is a static structure
      (i.e., it applies with the same value to all the elements of the class).
      This static function sets the parameters to its desired value. This function has
      to be called prior to the execution of any operation on the state.
    */
    static void setParameters( const typename DataStruct::Parameters & ps_p);

    DataStruct data; /*!< Element of the DataStruct that gets wrapped within the Wrapper */

    //! \brief Prints a dump of all the reachable states
    /*!
      This function prints a dump of all the reachable states. It is used for debug puprposes.
    */
    void printReachable();

    //! \brief Checks the consistency
    /*!
      This function is used to verify that the assignment for the data is
      consistent with the parameters (i.e., the sum of all the elements has
      to be equal to param.N).
      \return true if the data are consistent.
    */
    bool checkConsistency() const {
      return data.checkConsistency();
    }

    //! \brief Serialises to json
    /*!
      This function is part of the serialisation framework
      used by the project. It recursively calls the serialiser
      on the wrapped object.
      \param j json structure
    */
    void to_json(nlohmann::json &j) const {
      j = nlohmann::json {
	TO_JSON(data)
      };
      return;
    }
    //! \brief De-serialises from json
    /*!
      This function is part of the serialisation framework
      used by the project. It recursively calls the de-serialiser
      on the wrapped object.
      \param j json structure
    */
    void from_json(const nlohmann::json & j) {
      FROM_JSON(j, data);
      return;
    };

    using Parameters = typename DataStruct::Parameters;
    using ActionType = typename DataStruct::ActionType;
    using StateNames = typename DataStruct::StateNames;
    static Parameters & parms;

  private:
    //! \brief Map used to memorise the states for which the next states have been computed already
    /*!
      This map memorises for each wrapped state, the states that can be reached.
      This implements a memorising scheme to avoid that a state next states are computed more
      than once.
      The map is static (i.e., there is a single copy shared between all the instances of the
      wrapped object).
    */
    static std::map< StateWrapper<DataStruct>, std::map< CovidTool::NextState< StateWrapper<DataStruct> >, double > > _exploredStates;
  };

  //! \brief Comparison operator for wrapped state
  /*!
    Comparison operator for wrapped state. The operator calls recursively the operator
    on the data type.
    \tparam DataStruct Type of the state wrapped by StateWrapper
    \param lhs left hand side member of the comparison
    \param rhs right hand side of the comparison
    \return true is lhs < rhs, false otherwise
  */
  template<typename DataStruct>
  bool operator<(const StateWrapper<DataStruct> & lhs, const StateWrapper<DataStruct> & rhs) {
    return lhs.data < rhs.data;
  };

  template<typename DataStruct>
  bool operator==(const StateWrapper<DataStruct> & lhs, const StateWrapper<DataStruct> & rhs) {
    return lhs.data == rhs.data;
  };


  //! Data structure implementing the simplified model (without quarantines and tests
  /*!
    Data structure implementing the simplified model (without quarantines and tests). The data structure
    contains data structures and methods needed for the correct operations of the model.
    This class is used after embbeding it into a StateWrapper
   */
  struct SSV_  {
    //! \brief Parameters modeling the characteristics of the simplified model
    /*!
      Parameters modeling the characteristics of the simplified model.
      It contains such parameters as population size, number
      of beds in the Intensive Care Facilities, and probability
      of transitions between the different states.
    */
    using Parameters = struct Parameters {

      unsigned N; /*!< Total size of the population */

      unsigned C; /*!< Capacity of Intensive Care Facilities (i.e., maximum number of people that can be hospitalised */

      double omega; /*!< Probability to contract infection when meeting an infectious subject */

      double beta; /*!< Probability that an infectious asymptomatic recovers */

      double delta; /*!< Probability that an infectious asymptomatic develops symptoms */

      double mu; /*!< Probability that a symptomatic subject recovers */

      double psi; /*!< Probability that a symptomatic subject is found eligible for hospitalisation */

      double alpha; /*!< Probability that a symptomatic subject dies */

      double sigma; /*!< Probability that an hospitalised subject dies */

      double xi; /*!< Probability that and hospitalised subject recovers */

      unsigned M; /*!< Maximum number of people that can be met */

      unsigned Mmin=1; /*!< Minimum number of people that can be met */

      double eps_tot_prob = 1e-15; /*!< Maximum deviation tolerated for the total probability (the sum of the probability can be 1.0 except for a deviation eps_top_prob) */

      double eps_prob = 1e-20; /*!< Minimum allowed value of a probability (below this value the probability is set to 0 */

      bool prism_dump_action_label = true; /*!< If this boolean is, we dump the action label when creating the prism model */

      //! \brief Serialises to json
      /*!
	This function is part of the serialisation framework
	used by the project. It serialises the data structure
	containing the parameters.
	\param j json structure
       */
      void to_json(nlohmann::json &j) const;

      //! \brief Deserialises from json
      /*!
	This function is part of the serialisation framework
	used by the project. It deserialises the data structure
	containing the parameters.
	\param j json structure
       */
      void from_json(const nlohmann::json & j);

      //! \brief Returns the maximum code associated with an action.
      /*!
	Actions are in one to one correspondence with usnigned numbers.
	This function returns the maximum action code.
      */
      unsigned getActionNumber() const {return M; };

      //! \brief Returns the minimum code associated with an action.
      /*!
	Actions are in one to one correspondence with usnigned numbers.
	This function returns the minimum action code.
      */
      unsigned getMinActionNumber() const {return Mmin; };

      //! \brief Returns the minimum code associated with an action.
      /*!
	It check consistency of a parameter set (i.e., verifying that the
	probability stay in the appropriate range.
	\return true if the parameter set is consistent, false otherwise.
      */
      bool checkConsistency() const;
    };

    //! \brief Data structure encoding the possible actions for the simplified model
    /*!
      Data structure encoding the possible actions for the simplified model. In this case and action amounts to deciding the
      number of people that each non-symptomatic subject is
      allowed to meat. This number is a natural in the range
      [Parameters::Mmin, Parameters::M]
    */
    using ActionType=struct AA {
      //! \brief Default constructor
      /*!
	Default constructor. It constructs a null action
	associated with the value (0), which is a stuttering
	input (it could also be inconsistent with respect
	to the choice of parameters).
      */
      AA();

      //! \brief Constructor of an action
      /*!
	It defines an action with a specified value,
	corresponding to the number of people that
	can be met (required to be in the range
	[params::Mmin, params::M]
	\param v value of the required action
      */
      AA(unsigned v);

      //! \brief Cast operator to unsigned
      /*!
	Cast operator to usigned. In this case the conversion
	is simply the value itself of the action.
      */
      explicit operator unsigned () const;

      //! \brief Serialises to json
      /*!
	This function is part of the serialisation framework
	used by the project. It serialises the value of the action
	\param j json structure
       */
      void to_json(nlohmann::json &j) const;

      //! \brief Deerialises from json
      /*!
	This function is part of the serialisation framework
	used by the project. It deserialises the value of the action
	\param j json structure
       */
      void from_json(const nlohmann::json & j);

      //! \brief Dumps the content of the action to a string
      /*!
	Produces a string with a formatted print of the action
	\return string containing the formatted dump.
      */
      std::string dump() const;

      //! \brief Dumps the name of the action for use in Prism
      /*!
	Produces a string with a formatted print of the action name and of its value in a way that is usable in a Prism file
	\return string containing the formatted dump.
      */
      std::string dumpPrismName() const;

      //! \brief Dumps the value of the action for use in Prism
      /*!
	Produces a string with a formatted print of the value of the action in a way that is usable in a Prism file
	\return string containing the formatted dump.
      */
      std::string dumpPrismId() const;

    private:
      unsigned value; /*!< Value of the action */
    };

    //! Constructor for the class
    /*!
      Constructor for the SSV_ class. It sets the internal data structure and computes the
      has code used for ordering. The number of all subjects are non negative and have to sum
      to Parameters::N
      \param s_p Number of susceptible subjects
      \param a_p Number of asymptomatic subjects
      \param i_p Number of symptomatic subjects
      \param r_p Number of recovered subjects
      \param o_p Number of hospitalised subjects
      \param d_p Number of deceased subjects
     */
    SSV_(unsigned s_p, unsigned a_p, unsigned i_p, unsigned r_p, unsigned o_p, unsigned d_p );

    //! Default constructor for the class
    /*!
      Default Constructor for the SSV_ class.
      It creates an empty object which cannot be used directly
      (i.e., it is used to create empty containers).
     */
    SSV_( );


    //! Direct computation of the next state.
    /*!
      This function is called by the StateWrapper when the next states cannot be returned by
      looking up the map of the _exploredStates.
      \param action Action for which the next states computation is required
      \return map containing the different transitions and associating a probability with it
    */
    std::map< CovidTool::NextState<SSV_>, double >  nextStateDirect(const SSV_::ActionType & action) const;

    //! Checks the consistency of a state assignment
    /*!
      Checks the consistency of a state assignment. It is called by the
      function with the same name in StateWrapper.
      \return true if the state is consistently assigned
    */
    bool checkConsistency() const;

    //! \brief Serialises to json
    /*!
      This function is part of the serialisation framework
      used by the project. It serialises diffeent values of
      the state variables.
      \param j json structure
    */
    void to_json(nlohmann::json &j) const;

    //! \brief Deserialises from json
    /*!
      This function is part of the serialisation framework
      used by the project. It deserialises different values of
      from the json structure into the state variables.
      \param j json structure
    */
    void from_json(const nlohmann::json & j);

    //! \brief Dumps the content of the state into a string
    /*!
      This function dumps the content of the state into
      a formatted string.
      \return a formatted string containing the different elements of the state as fields.
    */
    std::string dump() const;

    //! \brief Dumps the signature of the Tuple for Prism
    /*!
      This function dumps the tuple signature to be used in
      Prism.
      \return a formatted string containing the signature of the state.
    */
    std::string dumpPrismTupleSignature() const;

    //! \brief Dumps the value of the state tuple
    /*!
      This function dumps the value of the state tuple for use in
      Prism.
      \return a Prism-formatted string containing the tuple.
    */
    std::string dumpPrismTupleValue() const;


    //! \brief Dumps the declaration of the state variable and their range
    /*!
      This function dumps the decalration of the state variables and of their range  for use in
      Prism.
      \return a Prism-formatted string containing the declaration of the state variables and their range
    */
    std::string dumpPrismStateVariablesDeclaration() const;

    //! \brief Dumps the declaration of the state observable state variables
    /*!
      This function dumps the list of the observable state variables.
      \return a Prism-formatted string containing the list of the observable state variables
    */
    std::string dumpPrismObservables() const;

    //! \brief Dumps the declaration of the state variables and their value in symbolic form
    /*!
      This function dumps the decalration of the state variables and their value in symbolic form for their use in Prism.
      \param ss a postfix string used in the dump
      \return a Prism-formatted string containing the symbolic dump of the state variables.
    */
    std::string dumpPrismSymbolicState(const char * ss = "") const;

    /*!
      Checks whether the hospital is saturated
      \return true if the hospital is saturated
    */
    bool HospitalSaturated() const;
    /*!
      String representing condition hospital is saturated
      \return String representing condition hospital is saturated
    */
    std::string HospitalSaturatedPrismCond() const;

    friend bool operator< (const SSV_ & lhs, const SSV_ & rhs);
    friend bool operator== (const SSV_ & lhs, const SSV_ & rhs);

    //! \brief Enumerated class containing the names of the different states
    /*!
      Enumerated class containing the names of the different states. This is used for instance when computing the marginal probabilities
    */
    enum class StateNames {
      Susceptible = 0,
      Asymptomatic,
      Symptomatic,
      Recovered,
      Hospitalised,
      Deceased,
      End
    };

    //! \brief Returns the hash code of the state
    /*!
      Returns the hash code of the state. It is used
      for defining a total order on the states.
    */
    unsigned getCode() const;

    //! \brief Sets the hash code of the state
    /*!
      Sets the hash code of the state. It is used
      for defining a total order on the states.
    */
    unsigned setCode();

    //! \brief Returns the number of people who are known to be sick
    /*!
      Returns the number  of people who are known to be sick.
      \return i + o (i = symptomatic, o = hospitalised)
    */
    unsigned getSymptomatic() const;

    //! \brief Returns the number of people who are asymptomatic
    /*!
      Returns the number of asymptomatic subjects
      \return a (a = asymptomatic)
    */
    unsigned getAsymptomatic() const;



    unsigned s; /*!< Number of Susceptible subjects */

    unsigned a; /*!< Number of Asymptomatic subjects */

    unsigned i; /*!< Number of Symptomatic subjects */

    unsigned r; /*!< Number of Recovered subjects */

    unsigned o; /*!< Number of Hospitalised subjects */

    unsigned d; /*!< Number of Deceased subjects */


    static Parameters parms;    /*!< Static copy of the class parameters */
    static bool parmsSet;      /*!< Static flag to decide if the parameters have been set */

  private:
    unsigned code; /*!< Unique code used for hashing */

  };

  //! \brief Comparison operator for two objects of type SSV_
  /*!
    Compares two objects of type SSV_ to set a total order.
    \param lhs left hand side of the comparison
    \param rhs right hand side of the comparison
    \retun true if lhs < rhs, flase otherwise.
*/
  inline bool operator< (const SSV_ & lhs, const SSV_ & rhs) {
    return lhs.code < rhs.code;
  };

  inline bool operator== (const SSV_ & lhs, const SSV_ & rhs) {
    return (lhs.code == rhs.code);
  };

  using SSV=StateWrapper<SSV_>;

  //! \brief Computes the marginal probabilities for an object of type SSV
  /*!
    Given the joint probabiities prodiced by the solver, this function marginalises the
    probabilities for the different states.
    \tparam Policy is a class encapsulating the function that implements the policy (i.e., a function that associates each state with an action)
    \param jointProb joint probability (resulting from the execution of the solver
    \param pf policy function object
    \param save flag to decide if the results have to be saved to a file
    \param fileNamePrefix prefix used for the file names in case the save flag is set
    \return a pair in which the first element is a vector of vectors (each one describing the pmf of one of the state variables
    and the second elmenet is a vector describing the distribution of the actions.
  */
  template <typename Policy>
  std::pair< std::vector< std::vector<double> >, std::vector<double> > marginalProbabilities(const std::map<CovidTool::SSV,double> & jointProb, Policy &pf, bool save = false, const char * fileNamePrefix="" );


  //! Data structure implementing the extended model (without quarantines and tests)
  /*!
    Data structure implementing the extended model (with quarantines and tests). The data structure
    contains data structures and methods needed for the correct operations of the model.
    This class is used after embbeding it into a StateWrapper
   */
  struct ESV_  {
     //! \brief Parameters modeling the characteristics of the extended model
    /*!
      Parameters modeling the characteristics of the simplified
      model.  It contains the same parameters as the simplified model
      plus additional probabilities.
    */
    struct Parameters {
      SSV_::Parameters baseParms; /*!< copy of the parameters shared with the simplified (base) model. */

      double gamma; /*!< Probability that an asymptomatic subject become quarantined */

      double iota;/*!< Probability that a quarantined subject develops symptoms */

      double upsilon; /*!< Probability that a quarantiend subect recovers */

      unsigned T; /*!< Maximum number of tests that can be performed */

      unsigned Tmin=1; /*!< Minimum number of tests that can be performed */
      //! \brief Serialises to json
      /*!
	This function is part of the serialisation framework
	used by the project. It serialises the different
	fields of the Parameters.
	\param j json structure
      */
      virtual void to_json(nlohmann::json &j) const;

      //! \brief Deserialises from json
      /*!
	This function is part of the serialisation framework
	used by the project. It deserialises the different
	fields of the Parameters from the json structure.
	\param j json structure
      */
      virtual void from_json(const nlohmann::json & j);
    };


    /// @brief the possible actions are in this case integers (the number of people that can be met, smaller than M)
    using ActionType=struct AA {
      //! \brief Default constructor
      /*!
	Default constructor. It constructs a null action
	associated with the value for the meetings (0) and 0 for the tests, which is a stuttering
	input
      */
      AA();

      //! \brief Constructor of an action
      /*!
	It defines an action with a specified value,
	corresponding to the number of people that
	can be met (required to be in the range
	[params::Mmin, params::M]) and to the number
	of tests that can be administered
	\param meetings_p value of the number of meetings
	\param tests_p value of the number of tests
      */
      AA(unsigned meetings_p,  unsigned tests_p );

      //! \brief Cast operator to unsigned
      /*!
	Cast operator to usigned. In this case the conversion
	is an hash code composing meetings and tests.
      */
      explicit operator unsigned () const;

      //! \brief Serialises to json
      /*!
	This function is part of the serialisation framework
	used by the project. It serialises the value of the action
	\param j json structure
       */
      void to_json(nlohmann::json &j) const;

      //! \brief Deserialises from json
      /*!
	This function is part of the serialisation framework
	used by the project. It deserialises the value of the action
	\param j json structure
       */
      void from_json(const nlohmann::json & j);


    //! \brief Returns the number of people who are known to be sick
    /*!
      Returns the number  of people who are known to be sick.
      \return i + o + q (i = symptomatic, o = hospitalised)
    */
      unsigned getSymptomatic() const;

    //! \brief Returns the number of people who are asymptomatic
    /*!
      Returns the number of asymptomatic subjects
      \return a (a = asymptomatic)
    */
      unsigned getAsymptomatic() const;

      //! \brief Dumps the content of the action to a string
      /*!
	Produces a string with a formatted print of the action
	\return string containing the formatted dump.
      */
      std::string dump() const;

      //! \brief Dumps the name of the action for use in Prism
      /*!
	Produces a string with a formatted print of the action name and of its value in a way that is usable in a Prism file
	\return string containing the formatted dump.
      */
      std::string dumpPrismName() const;

      //! \brief Dumps the value of the action for use in Prism
      /*!
	Produces a string with a formatted print of the value of the action in a way that is usable in a Prism file
	\return string containing the formatted dump.
      */
      std::string dumpPrismId() const;

    private:
      unsigned meetings; /*!< Number of meetings allowed */
      unsigned tests; /*!< Number of tests administered */
    };

    //! \brief Enumerated class containing the names of the different states
    /*!
      Enumerated class containing the names of the different states. This is used for instance when computing the marginal probabilities
    */
    enum class StateNames {
      Susceptible = 0,
      Asymptomatic,
      Symptomatic,
      Recovered,
      Hospitalised,
      Deceased,
      Quarantined,
      RecoveredAsymptomatic
    };


    //! Constructor for the class
    /*!
      Constructor for the ESV_ class. It sets the internal data structure and computes the
      hash code used for ordering. The number of all subjects are non negative and have to sum
      to Parameters::N
      \param s_p Number of susceptible subjects
      \param a_p Number of asymptomatic subjects
      \param i_p Number of symptomatic subjects
      \param r_p Number of recovered subjects
      \param o_p Number of hospitalised subjects
      \param d_p Number of deceased subjects
      \param q_p Number of quarantined subjects
      \param ra_b Number of recovered asymptomatic subjects
    */
    ESV_(unsigned s_p, unsigned a_p, unsigned i_p, unsigned r_p, unsigned o_p, unsigned d_p, unsigned q_p, unsigned ra_p );

    //! Default constructor for the class
    /*!
      Default Constructor for the SSV_ class.
      It creates an empty object which cannot be used directly
      (i.e., it is used to create empty containers).
     */
    ESV_( );

    //! Conversion to SSV_ class
    /*!
      Conversion from ESV_ to SSV_ class.
      It generates a new object stripping away
      the q and ra components
     */
    explicit operator SSV_ () const {
      return SSV_(s, a, i, r, o, d);
    };

    //! \brief Serialises to json
    /*!
      This function is part of the serialisation framework
      used by the project. It serialises different values of
      the state variables.
      \param j json structure
    */
    inline void to_json(nlohmann::json &j) const;


    //! \brief Deserialises from json
    /*!
      This function is part of the serialisation framework
      used by the project. It deserialises different values of
      from the json structure into the state variables.
      \param j json structure
    */
    void from_json(const nlohmann::json & j);


    //! \brief Dumps the content of the state into a string
    /*!
      This function dumps the content of the state into
      a formatted string.
      \return a formatted string containing the different elements of the state as fields.
    */
    std::string dump() const;


    //! \brief Dumps the signature of the Tuple for Prism
    /*!
      This function dumps the tuple signature to be used in
      Prism.
      \return a formatted string containing the signature of the state.
    */
    std::string dumpPrismTupleSignature() const;

    //! \brief Dumps the value of the state tuple
    /*!
      This function dumps the value of the state tuple for use in
      Prism.
      \return a Prism-formatted string containing the tuple.
    */
    std::string dumpPrismTupleValue() const;

    //! \brief Dumps the declaration of the state variable and their range
    /*!
      This function dumps the declaration of the state variables and of their range  for use in
      Prism.
      \return a Prism-formatted string containing the declaration of the state variables and their range
    */
    std::string dumpPrismStateVariablesDeclaration() const;

        //! \brief Dumps the declaration of the state observable state variables
    /*!
      This function dumps the list of the observable state variables.
      \return a Prism-formatted string containing the list of the observable state variables
    */
    std::string dumpPrismObservables() const;

    //! \brief Dumps the declaration of the state variables and their value in symbolic form
    /*!
      This function dumps the decalration of the state variables and their value in symbolic form for their use in Prism.
      \param ss a postfix string used in the dump
      \return a Prism-formatted string containing the symbolic dump of the state variables.
    */
    std::string dumpPrismSymbolicState(const char * ss = "") const;

    /*!
      Checks whether the hospital is saturated
      \return true if the hospital is saturated
    */
    bool HospitalSaturated() const;

    /*!
      String representing condition hospital is saturated
      \return String representing condition hospital is saturated
    */
    std::string HospitalSaturatedPrismCond() const;

    //! \brief Returns the hash code of the state
    /*!
      Returns the hash code of the state. It is used
      for defining a total order on the states.
    */
    unsigned getCode() const;

 /*!
      Returns the number  of people who are known to be sick.
      \return i + o (i = symptomatic, o = hospitalised)
    */
    unsigned getSymptomatic() const;

    //! \brief Returns the number of people who are asymptomatic
    /*!
      Returns the number of asymptomatic subjects
      \return a (a = asymptomatic)
    */
    unsigned getAsymptomatic() const;

    friend bool operator< (const ESV_ & lhs, const ESV_ & rhs);


    unsigned s; /*!< Susceptible subjects */

    unsigned a; /*!< Asymptomatic subjects */

    unsigned i; /*!< Symptomatic subjects */

    unsigned r; /*!< Recovered subjects */

    unsigned o; /*!< Hospitalised subjects */

    unsigned d; /*!< Deceased subjects */

    unsigned q; /*!< Quarantined subjects */

    unsigned ra; /*!< Recovered asymptomatic subjects */




    static Parameters parms; /*!< Static copy of the class parameters */
    static bool parmsSet; /*!< Static flag to decide if the parameters have been set */

  private:
    unsigned code;  /*!< Unique code used for hashing */
  };


  using SimplifiedStateValue = SSV;
  //  using ExtendedStateValue = struct ESV;


  //! \brief This data structure contains a collection of statet
  /*!
    This data structure contains a collection of states. Each state
    is associated with a unique numeric identifier. The typical use of this class is to produce a numbering of the states to produce a transition matrix.
    This class is not thread safe as yet (only one thread can use it at a time).
    \tparam StateValueType : externally provided value for the state
    \tparam MaxId : maximum value taken by the identifier
  */
  template<typename StateValueType, unsigned MaxId=1000000>
  struct StateSpace {
    // PUBLIC TYPES
    //! \brief Default constructor
    /*! Initialises the data sturcture used to implement
         the mapping between a StateValueType and a unique
	 code.
    */
    StateSpace();

    //! \brief Returns the number uniquely associated with a value
    /*!
      Returns the number uniquely associated with a value.
      If the state has already been created, the fucntion returns its identifier, otherwise it
      creates one
      \param sv Value of the state to look for
    */
    unsigned getStateNumber( const StateValueType & sv );

    //! \brief Returns the state value associated with a numeric identifier.
    /*!
      Returns the value uniquely associated with a numeric identifier.
      If the indentifier does not exist, it raises an exception.
      \param state identifier
    */
    StateValueType getStateValue( const unsigned  si ) const;

    //! \brief Resets the value of the data structure
    /*!
      Resets the content of the different data structures.
     */
    void reset();

    //! \brief Returns the number of states
    /*!
      Returns the number of states that have been recorded
      so far.
     */
    unsigned getNumberOfStates();

  private:
    unsigned _maxValue;  /*!< Maximum value of the states recorded in the StateSpace so far */
    std::map<StateValueType, unsigned> _valueToNum; /*!< Map that associates a state with its unique numeric code */
    std::vector<StateValueType> _numToValue; /*!< Vector used to associate a number with a state (in fact it is the inverse of the _valueToNum map */
  };




  //! \brief Basic data structure to generate a transition table
  /*!
    A transition table is a set of these data structure. Each of them specifies sourceCode, destCode, actionCode and probabiity.
   */
  struct Transition {
    unsigned sourceCode; /*!< Code of the source state (as stored in the StateSpace) */
    unsigned actionCode; /*!< Code of the action code (as resulting from a cast to unsigned) */
    unsigned destCode;   /*!< Code of the destination state (as stored in the StateSpace) */
    double prob;         /*!< Probability associated with the transition */

    //! \brief Constructor of the Transition
    /*!
      Constructor of the Transition
      \param sourceCode_d Code of the source state of the transition
      \param actionCode_d Code of the action associated with the transition
      \param destCode_d Code of the destination state of the transition
      \param prob_d Probability associated with the transition
    */
    Transition(unsigned sourceCode_d, unsigned actionCode_d, unsigned destCode_d, double prob_d);
  };


  // Service function to save the probabiity to files
  void saveProbs(const std::vector< std::vector<double> >  & pmfs, std::vector<double> & actPmf, const std::string & filePrefix, const std::string & policyId);
  void  pmf2cdf(const std::vector<double> & pmf, std::vector<double> & cdf);
};

#include "BasicTypes_src.hpp"

#endif
