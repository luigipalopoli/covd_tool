
/*
 * Header file containing the classes used for reachability analysis
 *
 * Copyright (C) 2020, University of Trento
 * Authors: Luigi Palopoli <luigi.palopoli@unitn.it>, Marco Roveri <marco.roveri@unitn.it>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
/** \file Reachability.hpp
 * Header file containing the classes used for reachability analysis
 */

#ifndef _REACHABILITY_HPP_
#define _REACHABILITY_HPP_
#include <vector>
#include <set>
#include "BasicTypes.hpp"
namespace CovidTool {

  //! \brief Constructs the reachable state space.
  /*!
    This class is used to construct the reachable StateSpace startig from an initial state.
    The class exploits the service of the StateValueType it is parametrised by. In addition,
    it uses a StateSpace<StateValueType> to generate consecutive code for the different states
    as soon as they are found in the exploration.
    After developing the set of reachable states, the class also generates a transition table.
  */
  template<typename StateValueType>
  class ReachabilityAnalyser {
  public:
    //! Constructor
    /*!
      This constructor allows us to specify the set of initial states.
      \param initialStates_d Set of possible initial states the reachability analysis starts from
      \param _ss_d StateSpace used as dictionary to associate states with unique numeric identifiers
    */
    ReachabilityAnalyser(const std::vector<StateValueType> & initialStates_d, CovidTool::StateSpace<StateValueType> & _ss_d);

    //! Constructor
    /*!
      This constructor does not specify the set of initial states (which have to be provided in a different way)
      \param _ss_d StateSpace used as dictionary to associate states with unique numeric identifiers
    */
    ReachabilityAnalyser( CovidTool::StateSpace<StateValueType> & _ss_d );

    //! \brief Computes the reachable states up until a maximum number of iterations or fixed point reached
    /*!
      Computes the space of reachable states starting from the initial states
      \param maxIter maximum number of iteration allowed befroe reaching the fixed point
    */
    unsigned computeReachableStates(const int maxIter = 100);

    //! \brief Computes the reachable states and prints out the table
    unsigned printReachableStates(const int maxIter = 100);

    //! \brief Generates the transition table
    std::vector<CovidTool::Transition> generateTransitions();

    //! \brief Generates the transition table and the reachable states
    /*!
      Computes the reachable state space and the same time generates the transition
      table (the use of this function is recommended instead of doing the computation
      in two steps)
      \param maxIter Maximum number of iterations allowed before reaching the fixed point
      \param filterTransition Filters out the transition that do not conform to the policy (set to false to get the entire transition system)
      \param pf Policy used to do the filtering in case filterTransition is on
      \tparam Policy fuction object defining the policy
      \return returns a transition table (i.e., a vecotr of the differernt transitions associated
              with the reachable space).
    */
    template < typename Policy >
    std::vector<CovidTool::Transition> generateTransitionsWithReachable(  bool filterTransition, Policy & pf, const int maxIter = 100 );

    //! \brief Computes the transition table and the reachable states at once.
    /*!
      Computes the transition table and the reachable space at the same time. Called by generateTransitionsWithReachable
      \param maxIter Maximum number of iterations allowed before reaching the fixed point
      \param filterTransition Filters out the transition that do not conform to the policy (set to false to get the entire transition system)
      \param pf Policy used to do the filtering in case filterTransition is on
      \tparam Policy fuction object defining the policy
     */
    template < typename Policy >
    void computeTransitionsWithReachable( bool filterTransition, Policy & pf, const int maxIter = 100 );



    //! \brief Returns the number of Choices of the MDP
    unsigned getChoices() const {return _choices;};

    //! \brief Returns the number of Choices of the MDP
    std::vector<CovidTool::Transition> getTransitions();

    //! \brief Dumps a series of files suitable to run the Prism Model Checker
    bool dumpPrismExplicitModel(const std::string & name, bool isMDP, bool initT = false);

    //! \brief Dumps a series of files suitable to run the Prism Model Checker
    bool dumpPrismModel(const std::string & name, bool isMDP, bool isPOMDP = false, bool initT = false);

    //! \brief Dumps a series of files suitable to run the Storm Model Checker
    bool dumpStormExplicitModel(const std::string & name, bool isMDP, bool initT = false);

    //! \brief Dumps a DRN Storm file suitable to run the Storm Model Checker
    bool dumpStormDRNModel(const std::string & name, bool isMDP, bool isPOMDP = false, bool initT = false);

    //! \brief Sets the state space to a different object than the one linked on creation
    /*!
      Sets the state space to a different object than the one linked on creation and resets all
      data structures
      \param initialStates_d Value of the intial states to start the search from
    */
    void setInitialStates(  const std::vector<StateValueType> & initialStates_d );

    //! \brief Sets the state space to a different object than the one linked on creation
  /*!
     Sets the state space to a different object than the one linked on creation- Resets all the data structures.
     \param _ss_d StateSpace object to link
  */
    void setStateSpace(  CovidTool::StateSpace<StateValueType> & _ss_d );

    //! \brief Sets the verbose flag
    /*!
      Sets the verbose flag to print out diagnostic messages while the RechabilityAnalyser
      executes. Useful for debug purposes. Could be commented out in production.
    */
    void setVerbose();



    //! \brief Parameters of the solver
    /*!
      This class contains the parameters used by the solver to compute a solution.
    */
    struct Parameters {
      std::vector<StateValueType>  initialStates; /*!< Initial state we start the computation from */
      int maxReachIter; /*!< Maximum number of iterations allowed to the reachability analyser */
      typename StateValueType::Parameters sp; /*!< Parameters of the state (e.g., population size, probability to transmit the virus etc. */

      //! \brief Serialises to json
      /*!
	This function is part of the serialisation framework
	used by the project. It serialises the different elements of the Parameters structure.
	\param j json structure
      */
      void to_json(nlohmann::json &j) const;

      //! \brief De-serialises from json
      /*!
	This function is part of the serialisation framework
	used by the project. It deserialises the differrent elements of the Parameters structure.
	\param j json structure
      */
      void from_json(const nlohmann::json &j);
    };

    //! \brief Sets the parameters
    /*!
      Sets the parameters of the RaechabilityAnalyser. It recursively sets theparameters of the ReachabilityAnlyser and of the states
      \param p_d desried value for the parameters
    */
    void setParameters(const CovidTool::ReachabilityAnalyser<StateValueType>::Parameters & p_d);


  private:
    std::set<StateValueType> _reachedStates; /*!< Number of reached states (coincides with the maximimum code of a reached state containes in the StateSpace _ss */
    CovidTool::StateSpace<StateValueType> & _ss; /*!< StateSpace used to map the states to numeric codes */
    std::vector<StateValueType> _initialStates; /*!< vector of initial states to start the reachability analysis from (can be initialised throught the constructor or through the appropriated method */
    bool _reachabilitytExecuted; /*!< This flag is set when the reachability analysis has been executed */
    std::vector<CovidTool::Transition> _trans; /*!< Transition table computed by the reachability analyser */
    unsigned _choices = 0;
    bool _transComputed = false; /*!< Flag set when the trnasition table has been computed */
    bool _verbose = false; /*!< Verbose flag. When set we print out diagnostic messages */
  };

}


#include "Reachability_src.hpp"




#endif
