COVID-TOOL - A tool to Study Spread of Deseases in Population
============================================

This tool currently considers a Markov Decision Process (MDP)
representing the spread of a disease like the SARS-COVID-19 flu. In
this model each subject evolve according to the following automata.

![Picture of the Automata](docs/Picture1.png?raw=true "Automata")

The overall population consists of N subjects. The states of the MDP
are tuples (S,A,I,R,Ra,O,D,Q) such that S is the susceptible subjects,
A the number of asymptomatic subjects, I the number of symptomatic
subjects, R the number of recovered subjects, Ra the number of
asymptomatic recovered subjects, O <= C number of hospitalised
subjects, D the number of deceased subjects, and Q the number of
quarantined subjects. C is a parameter that represent the number of
beds in hospitals. We also consider that in each transition at most t
tests can be administered, and that each person can meet at most M
subjects. Each transiton is associated to a given transition
probability.

Given this model, the COVID-TOOL can:

* reachability analysis: starting from an initialset of states and
  from a complete specification of the system’s parameters, compute
  all the states that are reachable and constructs a transition table
  that associates each pair (currentState, action) with a pair
  (nextState, probability), where the action is associated with the
  number M of people that are allowed to be met for that state;

* apply of a Policy: apply a control law associating each state with
  one of the actions that are possible for that state generating a
  DTMC;

* solve the DTMC (i.e., given set of initial states amd a known
  initial probability compute the evolutions for different steps
  and/or at the steady state);

* dump the MDP/DTMC model in the PRISM/STORM model checker formats to
  verify some Probabilistic Linear Temporal Logic (PLTL) properties,
  and in the MDP case to also enable for the synthesis of policies
  compliant with the PLTL property.

Authors
-----------------------------
COVID-TOOL has been developed at the University of Trento.

###### Principal developers
* Luigi Palopoli
* Marco Roveri

Building instructions
-----------------------------

Enter the src/apps directory, edit the Makefile to point to proper
needed libraries, and the run make. This will generate an executable
named covidsolver.

###### Dependencies
* eigen3: http://eigen.tuxfamily.org/index.php?title=Main_Page
* gsl: https://www.gnu.org/software/gsl
* PRISM: http://www.prismmodelchecker.org
* Storm: https://www.stormchecker.org

Execution instructions
-----------------------------

Run covidsolver -help for a list of options. The tool accepts all the
parameters through a json file, where you can specify the initial
states with associated probability as well as the other parameters
(e.g, population size, capacity of hospitals, transition
probabilities). An example of json file is the following:

~~~text
{
  "initialProbability": [
    0.5,
    0.5
  ],
  "initialStates": [
    {
      "data": {
        "a": 3,
        "d": 0,
        "i": 0,
        "o": 0,
        "r": 0,
        "s": 17
      }
    },
    {
      "data": {
        "a": 1,
        "d": 0,
        "i": 0,
        "o": 0,
        "r": 0,
        "s": 19
      }
    }
  ],
  "maxReachIter": 100,
  "pp": {
      "maxPerc": 0.1,
      "minPerc": 0.05
  },
  "sp": {
   "C": 2,
    "M": 5,
    "Mmin": 1,
    "N": 20,
    "alpha": 0.25,
    "beta": 0.45,
    "delta": 0.25,
    "eps_prob": 1e-15,
    "eps_tot_prob": 1e-12,
    "mu": 0.3,
    "omega": 0.5,
    "prism_dump_action_label": true,
    "psi": 0.35,
    "sigma": 0.05,
    "xi": 0.65
  }
}

~~~

Acknowledgements
-----------------------------

This work is partially funded with project 40900028, Bando interno
2020 Università di Trento "Covid 19".