#include <iostream>
#include "BasicTypes.hpp"
#include "Reachability.hpp"


bool testUnitialised() {
  CovidTool::SSV ssv;
  bool ret = false;
  ssv.data.s = 2;
  ssv.data.a = 3;
  ssv.data.i = 1;
  ssv.data.r = 4;
  ssv.data.o = 1;
  ssv.data.d = 1;

  try {
    auto v = ssv.nextStates();
  } catch(Exc & e)
    {
      ret = true;
    }
  return ret;
}


bool testWrongParameters() {
  CovidTool::SSV ssv;
  bool ret = false;

  try {
    CovidTool::SSV_::Parameters p{12, 3, 0.1, 0.4, 0.8, 0.1, 0.23, 0.1, 0.3, 0.5, 5};

    CovidTool::SSV::setParameters(p);
  } catch(Exc & e)
    {
      ret = true;
    }
  return ret;
}

bool testWrongCorrectState() {
  bool ret = false;
  CovidTool::SSV::Parameters p{12, 3, 0.1, 0.4, 0.6, 0.1, 0.23, 0.1, 0.3, 0.5, 5};

  CovidTool::SSV::setParameters(p);

  try {
    CovidTool::SSV ssv(CovidTool::SSV_{2,3,1,4,1,2});
  } catch(Exc & e)
    {
      ret = true;
    }

  try {
    CovidTool::SSV ssv(CovidTool::SSV_{2,3,1,4,1,1});
  } catch(Exc & e)
    {
      ret = false;
    }

  return ret;


  // CovidTool::SSV ssv;
  // bool ret = true;
  // ssv.s = 2;
  // ssv.a = 3;
  // ssv.i = 1;
  // ssv.r = 4;
  // ssv.o = 1;
  // ssv.d = 1;

  // try {
  //   auto v = ssv.nextStates();
  // } catch(Exc & e)
  //   {
  //     ret = false;
  //   }
  // return ret;
}



bool testCMP() {
  bool ret = false;
  CovidTool::SSV::Parameters p{3, 1, 0.1, 0.4, 0.6, 0.1, 0.23, 0.1, 0.3, 0.5, 1};

  CovidTool::SSV::setParameters(p);

  try {
    CovidTool::NextState<CovidTool::SSV> s1(CovidTool::SSV::ActionType(1), CovidTool::SSV(CovidTool::SSV_{3, 0, 0, 0, 0, 0}));
    CovidTool::NextState<CovidTool::SSV> s2(CovidTool::SSV::ActionType(0), CovidTool::SSV(CovidTool::SSV_{3, 0, 0, 0, 0, 0}));
    ret = (s2 < s1);

  } catch(Exc & e)
    {
      ret = false;
    }

  return ret;

}







bool testReach1() {
  bool ret = true;
  CovidTool::SSV::Parameters p{3, 1, 0.1, 0.4, 0.6, 0.1, 0.23, 0.1, 0.3, 0.5, 1};

  CovidTool::SSV::setParameters(p);

  try {
    CovidTool::SSV ss(CovidTool::SSV_{3,0,0,0,0,0});
    auto m = ss.nextStates();

    ss.printReachable();

    ret = (m.size() == 2);
  } catch(Exc & e)
    {
      ret = false;
    }

  return ret;

}
bool testReach2() {
  bool ret = true;
  CovidTool::SSV::Parameters p{3, 1, 0.3, 0.35, 0.5, 0.15, 0.23, 0.21, 0.35, 0.5, 2};

  CovidTool::SSV::setParameters(p);

  try {
    CovidTool::SSV ss(CovidTool::SSV_{2,1,0,0,0,0});
    auto m = ss.nextStates();

    ss.printReachable();

    ret = (m.size() == 21);
  } catch(Exc & e)
    {
      ret = false;
    }

  return ret;

}



bool testReachabilityAnalyser() {
  bool ret = true;
  CovidTool::SSV::Parameters p{3, 1, 0.3, 0.35, 0.5, 0.15, 0.23, 0.21, 0.35, 0.5, 2};

  CovidTool::SSV::setParameters(p);

  try {
    CovidTool::SSV ss(CovidTool::SSV_{2,1,0,0,0,0});
    std::vector<CovidTool::SSV> s0 = {ss};
    CovidTool::StateSpace<CovidTool::SSV> stateSp;
    CovidTool::ReachabilityAnalyser<CovidTool::SSV> ra(s0, stateSp);


    unsigned n1 = ra.computeReachableStates(10000);

    unsigned n2 = ra.computeReachableStates(10000);

    ra.printReachableStates();

    TRACE(n1);
    TRACE(n2);

    //unsigned n = ra.printReachableStates(s0);

    ret = (n1 == n2);
  } catch(Exc & e)
    {
      ret = false;
    }

  return ret;

}

int main() {
  bool res=true;

  bool resPartial;
  if ( resPartial = testUnitialised() )
    {
      std::cerr<<"First Test on correct initialisation succeeded"<<std::endl;
    }
  else
    {
      std::cerr<<"First Test on correct initialisation failed"<<std::endl;

    }
  res = res && resPartial;

  if ( resPartial =  testWrongParameters() )
    {
      std::cerr<<"Test on wrong initialisation succeeded"<<std::endl;
    }
  else
    {
      std::cerr<<"Test on wrong initialisation failed"<<std::endl;
    }
  res = res&& resPartial;

  if ( resPartial = testWrongCorrectState() )
    {
      std::cerr<<"Test on state creation succeeded"<<std::endl;
    }
  else
    {
      std::cerr<<"Test on state creation failed"<<std::endl;
    }
  res = res && resPartial;

  if ( resPartial = testCMP() )
    {
      std::cerr<<"Test on comparison succeeded"<<std::endl;
    }
  else
    {
      std::cerr<<"Test on comparison failed"<<std::endl;
    }
   res = res && resPartial;


  if ( resPartial = testReach1() )
    {
      std::cerr<<"First Test on reachability succeeded"<<std::endl;
    }
  else
    {
      std::cerr<<"First Test on reachability failed"<<std::endl;
    }
  res = res && resPartial;


  if ( resPartial =  testReach2() )
    {
      std::cerr<<"Second Test on reachability succeeded"<<std::endl;
    }
  else
    {
      std::cerr<<"Second Test on reachability failed"<<std::endl;
    }
  res = res && resPartial;

  if ( resPartial =  testReachabilityAnalyser() )
    {
      std::cerr<<"Test on reachability analyser succeeded"<<std::endl;
    }
  else
    {
      std::cerr<<"Test on reachability analyser failed"<<std::endl;
    }
  res = res && resPartial;

  if (res)
    std::cout << "All the tests passe" << std::endl;
  else
    std::cout << "There are some failing tests" << std::endl;
  return res;

  return res;
}
