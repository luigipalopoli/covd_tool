#include <iostream>
#include <iomanip>
#include <chrono>
#include "BasicTypes.hpp"
#include "Reachability.hpp"

typedef std::chrono::high_resolution_clock Clock;
typedef std::chrono::milliseconds milliseconds;

bool testReachabilityAnalyserPrism() {
  bool ret = true;
  const int N = 10;
  CovidTool::SSV::Parameters p{N, 1, 0.3, 0.35, 0.5, 0.15, 0.23, 0.21, 0.35, 0.5, 2, 1, 1e-12};

  CovidTool::SSV::setParameters(p);

  try {
    CovidTool::SSV ss(CovidTool::SSV_{N-1,1,0,0,0,0});
    std::vector<CovidTool::SSV> s0 = {ss};
    CovidTool::StateSpace<CovidTool::SSV> stateSp;

    CovidTool::ReachabilityAnalyser<CovidTool::SSV> ra(s0, stateSp);

    unsigned n1 = ra.computeReachableStates(10000);

    // ra.printReachableStates();
    ret = ra.dumpPrismExplicitModel(std::string("test"), true);
  } catch(Exc & e)
    {
      ret = false;
    }

  return ret;
}

bool testReachabilityAnalyserPrismSymbolic() {
  bool ret = true;
  CovidTool::SSV::Parameters p{3, 1, 0.3, 0.35, 0.5, 0.15, 0.23, 0.21, 0.35, 0.5, 2};

  CovidTool::SSV::setParameters(p);

  try {
    CovidTool::SSV ss(CovidTool::SSV_{2,1,0,0,0,0});
    std::vector<CovidTool::SSV> s0 = {ss};
    CovidTool::StateSpace<CovidTool::SSV> stateSp;

    CovidTool::ReachabilityAnalyser<CovidTool::SSV> ra(s0, stateSp);

    unsigned n1 = ra.computeReachableStates(10000);

    ret = ra.dumpPrismModel(std::string("test"), true);
  } catch(Exc & e)
    {
      ret = false;
    }

  return ret;
}

bool testReachabilityAnalyserPrismSymbolic25() {
  bool ret = true;
  CovidTool::SSV::Parameters p{25, 3, 0.3, 0.35, 0.5, 0.15, 0.23, 0.21, 0.35, 0.5, 2, 1, 1e-12};

  CovidTool::SSV::setParameters(p);

  try {
    CovidTool::SSV ss(CovidTool::SSV_{24,1,0,0,0,0});
    std::vector<CovidTool::SSV> s0 = {ss};
    CovidTool::StateSpace<CovidTool::SSV> stateSp;

    CovidTool::ReachabilityAnalyser<CovidTool::SSV> ra(s0, stateSp);

    unsigned n1 = ra.computeReachableStates(10000);

    ret = ra.dumpPrismModel(std::string("test25"), true);
  } catch(Exc & e)
    {
      ret = false;
    }

  return ret;
}

bool testReachabilityAnalyserStorm()  {
  bool ret = true;
  CovidTool::SSV::Parameters p{3, 1, 0.3, 0.35, 0.5, 0.15, 0.23, 0.21, 0.35, 0.5, 2};

  CovidTool::SSV::setParameters(p);

  try {
    CovidTool::SSV ss(CovidTool::SSV_{2,1,0,0,0,0});
    std::vector<CovidTool::SSV> s0 = {ss};
    CovidTool::StateSpace<CovidTool::SSV> stateSp;

    CovidTool::ReachabilityAnalyser<CovidTool::SSV> ra(s0, stateSp);

    unsigned n1 = ra.computeReachableStates(10000);

    // ra.printReachableStates();
    ret = ra.dumpStormExplicitModel(std::string("test"), true);
  } catch(Exc & e)
    {
      ret = false;
    }

  return ret;
}


bool testGen() {
  bool ret = true;
  const unsigned reac_iterations = 100000;
  const unsigned initial_asyntomatic = 1;
  const unsigned hospital_capacity = 1;

  for(unsigned i = 5; ((i < 50) && ret); i+=5) {
    try {
      assert(initial_asyntomatic < i);
      milliseconds ms;
      CovidTool::SSV::Parameters p{i, hospital_capacity,
	  0.1, 0.4, 0.6, 0.1, 0.23, 0.1, 0.3, 0.5, 4, 1, 1e-12, 1e-15};
      CovidTool::SSV::setParameters(p);
      CovidTool::SSV ss(CovidTool::SSV_{i-initial_asyntomatic,initial_asyntomatic,0,0,0,0});
      std::vector<CovidTool::SSV> s0 = {ss};
      CovidTool::StateSpace<CovidTool::SSV> stateSp;
      CovidTool::ReachabilityAnalyser<CovidTool::SSV> ra(s0, stateSp);

      std::cout<<"============================================"<<std::endl;
      std::cout<<"============COMPUTATION REACHABLE (N="<< setw(3) << i << ")"<<std::endl;
      Clock::time_point t0 = Clock::now();
      unsigned n1 = ra.computeReachableStates(reac_iterations);
      Clock::time_point t1 = Clock::now();
      std::cout<<"============COMPLETED (N=" << setw(3) << i << ")";
      ms = std::chrono::duration_cast<milliseconds>(t1 - t0);
      std::cout<<"Time = " << ms.count()/1000.0 << " (s)" << std::endl;
      std::string prefix = std::string("test_")+std::to_string(i);
      std::cout << "============Dumping PRISM Model '"<< prefix << "'"<< std::endl;
      ret = ra.dumpPrismModel(prefix, true);
      Clock::time_point t2 = Clock::now();
      ms = std::chrono::duration_cast<milliseconds>(t2 - t1);
      std::cout << "============Done: Time = '"<< ms.count()/1000.0 << "' (s)" << std::endl;
    } catch(Exc & e) {
      ret = false;
    }
  }
  return ret;
}

int main() {
  bool res=true;

  bool resPartial;

  resPartial = testReachabilityAnalyserPrism();
  if ( resPartial ) {
      std::cerr<<"Test on dump Prism Explicit model succeeded"<<std::endl;
  }
  else {
    std::cerr<<"Test on dump Prism Explicit model  failed"<<std::endl;
  }
  res = res && resPartial;

  resPartial =  testReachabilityAnalyserStorm();
  if ( resPartial) {
      std::cerr<<"Test on dump Storm Explicit model succeeded"<<std::endl;
  }
  else {
    std::cerr<<"Test on dump Storm Explicit model  failed"<<std::endl;
  }
  res = res && resPartial;

  resPartial = testReachabilityAnalyserPrismSymbolic();
  if ( resPartial ) {
      std::cerr<<"Test on dump Prism Symbolic model succeeded"<<std::endl;
  }
  else {
    std::cerr<<"Test on dump Prism Symbolci model  failed"<<std::endl;
  }
  res = res && resPartial;

#if 0
  resPartial = testReachabilityAnalyserPrismSymbolic25();
  if ( resPartial) {
      std::cerr<<"Test on dump Prism Symbolic model succeeded"<<std::endl;
  }
  else {
    std::cerr<<"Test on dump Prism Symbolci model  failed"<<std::endl;
  }
  res = res && resPartial;
#endif

  resPartial = testGen();
  if ( resPartial ) {
      std::cerr<<"Test on Generation Multiple Prism Symbolic models succeeded"<<std::endl;
  }
  else {
    std::cerr<<"Test on Generation Multiple Prism Symbolic models  failed"<<std::endl;
  }
  res = res && resPartial;

  if (res)
    std::cout << "All the tests pass" << std::endl;
  else
    std::cout << "There are some failing tests" << std::endl;
  return res;

}
