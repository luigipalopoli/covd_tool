#include <iostream>
#include "BasicTypes.hpp"
#include "util/TimeCount.hpp"
#include "Solver.hpp"
#include "Policy.hpp"
#include <functional>
#include "util/json.hpp"
#include"util/Serial.hpp"

bool testTransMatrix() {
   bool ret = true;
   CovidTool::SSV::Parameters p{
     3,      //N
       1,    //C
       0.1,  //omega
       0.4,  //beta
       0.4,  //delta
       0.1,  //mu
       0.23, //psi
       0.1,  //alpha
       0.3,  //sigma
       0.5,  //xi
       2,
       1,
       1e-12,
       1e-15};

   CovidTool::SSV::setParameters(p);
   
   try {
     CovidTool::SSV ss(CovidTool::SSV_{2, 1, 0,0,0,0});
     CovidTool::ConstantPolicy<CovidTool::SSV> cp;
     
     using PolicyType=CovidTool::ConstantPolicy<CovidTool::SSV>;
     using SolverType=CovidTool::Solver<CovidTool::SSV, PolicyType>;
     
     std::vector<CovidTool::SSV> s0 = {ss};
     CovidTool::StateSpace<CovidTool::SSV> stateSp;
     CovidTool::ReachabilityAnalyser<CovidTool::SSV> ra(s0, stateSp);
     //ra.setVerbose();
     SolverType sol( stateSp, ra, cp);
     
     SolverType::Parameters solP {s0, std::vector<double>{1.0}, 100, p, PolicyType::Parameters{CovidTool::SSV::ActionType(1)}};
     sol.setParameters(solP); 
    
    using SolFunType=std::function<Eigen::MatrixXd()>;

    SolFunType cTM = std::bind(&SolverType::computeTransitionMatrix, sol);

    std::pair< Eigen::MatrixXd, double > RES = CovidTool::functTime1(cTM);

    Eigen::MatrixXd P = RES.first;

    std::cout<<"============COMPUTATION COMPLETED =========="<<std::endl;
    std::cout<<"Number of states reached: "<<stateSp.getNumberOfStates()<<std::endl;
    std::cout<<"Time (seconds): "<<RES.second/1e9<<std::endl;
    std::cout<<"============================================"<<std::endl;


    
    bool ch1 = ( Eigen::ArrayXXd(P)  < 0.0 ).any();
    bool ch2 = ( Eigen::ArrayXXd(P)  > 1.0 ).any();
    Eigen::VectorXd c = P.rowwise().sum();
    

    c = (c - Eigen::VectorXd::Constant(c.rows(), 1)).cwiseAbs();
    bool ch3 = (Eigen::ArrayXXd(c) < 1e-12).all();

    ret = !ch1 && !ch2 && ch3;
   } catch(Exc & e)
    {
      ret = false;
    }

  return ret;
}


bool testSolver() {
   bool ret = true;
   const int stateNum=13;

   CovidTool::SSV::Parameters p{stateNum, 3, 0.5, 0.3, 0.25, 0.1, 0.5, 0.4, 0.3, 0.5, 5, 1, 1e-12, 1e-15};

//CovidTool::SSV::setParameters(p);

   try {
     CovidTool::SSV ss(CovidTool::SSV_{stateNum-3,3,0,0,0,0});
     CovidTool::SSV ss1(CovidTool::SSV_{stateNum-4,2,2,0,0,0});
    CovidTool::ConstantPolicy<CovidTool::SSV> cp;

    using PolicyType=CovidTool::ConstantPolicy<CovidTool::SSV>;
    using SolverType=CovidTool::Solver<CovidTool::SSV, PolicyType>;
    
    std::vector<CovidTool::SSV> s0 = {ss, ss1};
    std::vector<double> pi0 = {0.5, 0.5};

    CovidTool::StateSpace<CovidTool::SSV> stateSp;
    CovidTool::ReachabilityAnalyser<CovidTool::SSV> ra(s0, stateSp);
    //ra.setVerbose();

    SolverType sol( stateSp, ra, cp);

    SolverType::Parameters solP {s0, pi0, 100, p, PolicyType::Parameters{CovidTool::SSV::ActionType(1)}};
    sol.setParameters(solP);
    nlohmann::json j;
    CovidTool::to_json(j, solP);
    TRACE(j.dump());
    
    using SolFunType=std::function<std::map<CovidTool::SSV, double>( const int )>;

    SolFunType cTM = std::bind(&SolverType::solve, sol, std::placeholders::_1);

    std::pair< std::map<CovidTool::SSV, double>, double > RES = functTime1(cTM, 20);

    

    std::cout<<"============COMPUTATION COMPLETED =========="<<std::endl;
    std::cout<<"Number of states reached: "<<stateSp.getNumberOfStates()<<std::endl;
    std::cout<<"Time (seconds): "<<RES.second/1e9<<std::endl;
    std::cout<<"============================================"<<std::endl;

    std::map<CovidTool::SSV, double> jp = RES.first;

    double totProb = 0;

    std::pair< std::vector< std::vector<double> >, std::vector<double> > mp = marginalProbabilities(jp, cp, true, "testSolve");

    totProb = std::accumulate( jp.begin(), jp.end(), 0.0, [](double sum, auto & el) {
							    return sum + el.second;
							  });
    TRACE(totProb);
    ret = abs(totProb -1.0)<1e-12;
    
    
    auto vS = mp.first.at(static_cast<int>(CovidTool::SSV::StateNames::Susceptible));
    double sProb = std::accumulate( vS.begin(), vS.end(), 0.0, [](double sum, auto & el) {
							    return sum + el;
							  });
    TRACE(sProb);
    
    ret = ret && (abs(sProb-1)<1e-12);

    auto vA = mp.first.at(static_cast<int>(CovidTool::SSV::StateNames::Asymptomatic));
    double aProb = std::accumulate( vA.begin(), vA.end(), 0.0, [](double sum, auto & el) {
							    return sum + el;
							  });
    TRACE(aProb);
    ret = ret && (abs(aProb-1)<1e-12);

    auto vSy = mp.first.at(static_cast<int>(CovidTool::SSV::StateNames::Symptomatic));
    double syProb = std::accumulate( vSy.begin(), vSy.end(), 0.0, [](double sum, auto & el) {
							    return sum + el;
							  });
    TRACE(syProb);
    ret = ret && (abs(syProb-1)<1e-12);


    
    auto vO = mp.first.at(static_cast<int>(CovidTool::SSV::StateNames::Hospitalised));
    double oProb = std::accumulate( vO.begin(), vO.end(), 0.0, [](double sum, auto & el) {
							    return sum + el;
							  });

    ret = ret && (abs(oProb-1)<1e-12);
    TRACE(oProb);
    auto vR = mp.first.at(static_cast<int>(CovidTool::SSV::StateNames::Recovered));
    double rProb = std::accumulate( vR.begin(), vR.end(), 0.0, [](double sum, auto & el) {
							    return sum + el;
							  });

    ret = ret && (abs(rProb-1)<1e-12);
    TRACE(rProb);
    auto vD = mp.first.at(static_cast<int>(CovidTool::SSV::StateNames::Deceased));
    double dProb = std::accumulate( vD.begin(), vD.end(), 0.0, [](double sum, auto & el) {
							    return sum + el;
							  });
    TRACE(dProb);
    ret = ret && (abs(dProb-1)<1e-12);


   } catch(Exc & e)
    {
      ret = false;
    }

  return ret;
}


bool testSolver1() {
   bool ret = true;
   const int stateNum=15;

   CovidTool::SSV::Parameters p{stateNum, 3, 0.5, 0.3, 0.25, 0.1, 0.5, 0.4, 0.3, 0.5, 5, 1, 1e-12, 1e-15};

   CovidTool::SSV::setParameters(p);

   try {
     CovidTool::SSV ss(CovidTool::SSV_{stateNum-3,3,0,0,0,0});
    CovidTool::AdaptiveSymptPolicy<CovidTool::SSV> ap;

    using PolicyType=CovidTool::AdaptiveSymptPolicy<CovidTool::SSV>;
    using SolverType=CovidTool::Solver<CovidTool::SSV, PolicyType>;
    
    std::vector<CovidTool::SSV> s0 = {ss};
    std::vector<double> pi0 = {1.0};

    CovidTool::StateSpace<CovidTool::SSV> stateSp;
    CovidTool::ReachabilityAnalyser<CovidTool::SSV> ra(s0, stateSp);
    //ra.setVerbose();


    
    SolverType sol( stateSp, ra, ap);

    SolverType::Parameters solP {s0, pi0, 100, p, PolicyType::Parameters{0.1, 0.3}};
    sol.setParameters(solP);
    nlohmann::json j;
    CovidTool::to_json(j, solP);
    TRACE(j.dump());
    
    using SolFunType=std::function<std::map<CovidTool::SSV, double>( const int )>;

    SolFunType cTM = std::bind(&SolverType::solve, sol, std::placeholders::_1);
    
    std::pair< std::map<CovidTool::SSV, double>, double > RES = functTime1(cTM, 5);

    

    std::cout<<"============COMPUTATION COMPLETED =========="<<std::endl;
    std::cout<<"Number of states reached: "<<stateSp.getNumberOfStates()<<std::endl;
    std::cout<<"Time (seconds): "<<RES.second/1e9<<std::endl;
    std::cout<<"============================================"<<std::endl;

    std::map<CovidTool::SSV, double> jp = RES.first;

    
    double totProb = 0;
    std::pair< std::vector< std::vector<double> >, std::vector<double> > mp = marginalProbabilities(jp, ap, true, "testSolve");

    totProb = std::accumulate( jp.begin(), jp.end(), 0.0, [](double sum, auto & el) {
							    return sum + el.second;
      });
    TRACE(totProb);
    ret = abs(totProb -1.0)<1e-12;
    
    auto vS = mp.first.at(static_cast<int>(CovidTool::SSV::StateNames::Susceptible));
    double sProb = std::accumulate( vS.begin(), vS.end(), 0.0, [](double sum, auto & el) {
							    return sum + el;
							  });

    ret = ret && (abs(sProb-1)<1e-12);

    auto vA = mp.first.at(static_cast<int>(CovidTool::SSV::StateNames::Asymptomatic));
    double aProb = std::accumulate( vA.begin(), vA.end(), 0.0, [](double sum, auto & el) {
							    return sum + el;
							  });

    ret = ret && (abs(aProb-1)<1e-12);

    auto vSy = mp.first.at(static_cast<int>(CovidTool::SSV::StateNames::Symptomatic));
    double syProb = std::accumulate( vSy.begin(), vSy.end(), 0.0, [](double sum, auto & el) {
							    return sum + el;
							  });

    ret = ret && (abs(syProb-1)<1e-12);


    
    auto vO = mp.first.at(static_cast<int>(CovidTool::SSV::StateNames::Hospitalised));
    double oProb = std::accumulate( vO.begin(), vO.end(), 0.0, [](double sum, auto & el) {
							    return sum + el;
							  });

    ret = ret && (abs(oProb-1)<1e-12);

    auto vR = mp.first.at(static_cast<int>(CovidTool::SSV::StateNames::Recovered));
    double rProb = std::accumulate( vR.begin(), vR.end(), 0.0, [](double sum, auto & el) {
							    return sum + el;
							  });

    ret = ret && (abs(rProb-1)<1e-12);

    auto vD = mp.first.at(static_cast<int>(CovidTool::SSV::StateNames::Deceased));
    double dProb = std::accumulate( vD.begin(), vD.end(), 0.0, [](double sum, auto & el) {
							    return sum + el;
							  });

    ret = ret && (abs(dProb-1)<1e-12);


   } catch(Exc & e)
    {
      ret = false;
    }

  return ret;
}


int main() {
  bool res=true;

  bool resPartial, resPartial1;
  if ( (resPartial = testTransMatrix()) )
    {
      std::cerr<<"First Test on correct computation of Transition Matrix succeeded"<<std::endl;
    }
  else
    {
      std::cerr<<"First Test on correct computation of Transition Matrix failed"<<std::endl;

    }
  res = res && resPartial;

  if ( (resPartial = testSolver()) )
    {
      std::cerr<<"First Test on Solver succeeded"<<std::endl;
    }
  else
    {
      std::cerr<<"First Test on Solver failed"<<std::endl;

    }
  res = res && resPartial;



  
  if ( (resPartial1 = testSolver1()) )
    {
      std::cerr<<"Second Test on Solver succeeded"<<std::endl;
    }
  else
    {
      std::cerr<<"Second Test on Solver failed"<<std::endl;

    }
  res = res && resPartial1;

  
  return res;

}
