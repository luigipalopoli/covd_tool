/*
 * Source file containing the code to test computation of the probabilities
 *
 * Copyright (C) 2020, University of Trento
 * Authors: Luigi Palopoli <luigi.palopoli@unitn.it>, Marco Roveri <marco.roveri@unitn.it>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
/** \file TransProb.cpp
 * Source file containing the code to test computation of the probabilities
 */
#include <iostream>
#include <fstream>
#include <string>
#include <regex>
#include <cstdlib>
#include <iomanip>
#include "BasicTypes.hpp"
#include "TransProb.hpp"

#define ERROR 0.000000001
// double transProbDelta(const CovidTool::SSV & Vk, const std::vector<unsigned> & Delta)
bool testMulti(const std::vector<unsigned> & S,
	       const std::vector<unsigned> & D,
	       const double ep,
	       const CovidTool::SSV::ActionType & A) {
  CovidTool::SSV_ ssv(S[0], S[1], S[2], S[3], S[4], S[5]);
  double prob = transProbDelta(ssv, D, A);
  bool res = true;
  if (std::fabs(prob - ep) > ERROR) {
    res = false;
    std::cout << setprecision(20) << prob << " " << ep << std::endl;
    std::cout << setprecision(20) << std::fabs(prob - ep) << std::endl;
    EXC_PRINT("Computed probabilities differs of more than the given ERROR");
  }
  return res;
}

void parsesample(const char * line,
		 std::vector<unsigned> & s,
		 std::vector<unsigned> & d,
		 double & p);

void test(const char * fname, const CovidTool::SSV::ActionType & A) {
  std::ifstream infile(fname);
  std::string line;
  std::vector<unsigned> s1 = {10, 10, 10, 10, 10, 10};
  std::vector<unsigned> d1 = {10, 10, 10, 10, 10, 10, 10, 10};
  double p1;
  int test = -1;
  unsigned succ = 0;
  unsigned failed =0;

  if (!infile.is_open()) {
    std::cerr << "Unable to open file " << fname << std::endl;
    EXC_PRINT("Unable to open the given file");
  }
  while(std::getline(infile, line)) {
    parsesample(line.c_str(), s1, d1, p1);

    try {
      test++;
      std::cout << "Executing test " << test << std::endl;
      testMulti(s1, d1, p1, A);
      std::cout << "Executed test  " << test << " Succeded" << std::endl;
      succ++;
    }
    catch(Exc & e)  {
      for(int i = 0; i < 6; i++) std::cout << s1[i] << " "; std::cout << std::endl;
      for(int i = 0; i < 8; i++) std::cout << d1[i] << " "; std::cout << std::endl;
      std::cout << "Test " << test << " failed " << std::endl;
      failed++;
      assert(false);
    }
  }
  std::cout << "------------------------------------------------------" << std::endl;
  std::cout << "Succeeded: " << succ << std::endl;
  std::cout << "Failed: " << failed << std::endl;
  std::cout << "Total: " << test << std::endl;
  infile.close();
}

void parsesample(const char * line,
		 std::vector<unsigned> & s,
		 std::vector<unsigned> & d,
		 double & p) {
  regex expr("transProbDelta: \\[(\\d+),\\s(\\d+),\\s(\\d+),\\s(\\d+),\\s(\\d+),\\s(\\d+)\\] "
	     "\\[(\\d+),\\s(\\d+),\\s(\\d+),\\s(\\d+),\\s(\\d+),\\s(\\d+),\\s(\\d+),\\s(\\d+)\\] "
	     "\\([0-9,\\.\\s]+\\) "
	     "([0-9e,-\\.\\s]+).*", std::regex_constants::icase);

  std::cmatch cm;

  // using explicit flags:
  std::regex_match ( line, cm, expr, std::regex_constants::match_default );

  assert(cm.size() == 16);

  for(int i = 1,j=0; i <= 6; i++) {
    std::string ss = cm[i].str();
    s[j++] = std::atoi(ss.c_str());
  }

  for(int i = 7,j=0; i <= 15; i++) {
    std::string ss = cm[i].str();
    d[j++] = std::atoi(ss.c_str());
  }

  std::string ss = cm[15];
  p = std::atof(ss.c_str());

#if 0
  std::cout << "the matches were: " << std::endl;
  for (unsigned i=0; i<cm.size(); ++i) {
    std::cout << i << " [" << cm[i] << "] " << std::endl;
  }
#endif
}

int main() {
  CovidTool::SSV::Parameters parms;
  // Params(N=7, C=2, omega=0.6, beta=0.4, delta=0.5, mu=0.1,
  //              alpha=0.25, psi=0.6, xi=0.4, sigma=0.01, M=3)
  parms.N = 7;
  parms.C = 2;
  parms.omega = 0.6;
  parms.beta = 0.4;
  parms.delta = 0.5;
  parms.mu = 0.1;
  parms.psi = 0.6;
  parms.alpha = 0.25;
  parms.sigma = 0.01;
  parms.xi = 0.4;
  parms.M = 3;
  CovidTool::SSV::setParameters(parms);
  test("testmultinomial.txt",  CovidTool::SSV::ActionType(parms.M));

  print_memoizing_stats();

#if 0
  try {
    test++;
    std::cout << "Executing test " << test << std::endl;
    std::vector<unsigned> s1 = {};
    std::vector<unsigned> d1 = {};
    double p1 = ;
    testMulti(s1, d1, p1, CovidTool::SSV::ActionType(parms.M));
    std::cout << "Executed test  " << test << " Succeded" << std::endl;
  }
  catch(Exc & e)  {
    std::cout << "Test " << test << " failed " << std::endl;
  }
#endif

  return 0;
}
