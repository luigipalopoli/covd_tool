/*
  Solver application
*/

#include <cstdlib>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <cctype>
#include <string>
#include <lyra/lyra.hpp>
#include <string>
#include "util/PrintUtils.hpp"
#include "BasicTypes.hpp"
#include "util/TimeCount.hpp"
#include "Solver.hpp"
#include "Policy.hpp"
#include <functional>
#include <map>
#include <typeinfo>

using StateType=CovidTool::SSV;
using ReachabilityAnalyserType= CovidTool::ReachabilityAnalyser<StateType>;
using StateSpaceType=CovidTool::StateSpace<StateType>;
using SolverResultType=std::map<CovidTool::SSV, double>;
using SolFunType=std::function<SolverResultType( const int )>;
using ReachFunType=std::function<void( const int )>;
using TVar=std::chrono::high_resolution_clock::time_point;
using SolveProbResultType = std::pair< std::vector< std::vector<double> >, std::vector<double> >;

// MR2LP: The use of STATIC is deprecated, we shall avoid is!
static std::string configFileName;
static  bool showHelp = false;
static  int iterations = 0;
static  int policyNum = 0;
//std::string inputFile;
static  std::string doStorm;
static  std::string doPrism;
static  std::string outPrefix = "TEST";
static  std::string outFile=std::string("");
static  double outPercDec=0.2;
static  double outPercRec=0.8;
static  bool doSolve = false;
static  bool optVerbose = false;
static  bool optMCAllInit = false;
static  bool optStormDRNOut = false;
static  bool optPOMDP = false;

template<typename Base, typename T>
inline bool instanceof(const T*) {
   return is_base_of<Base, T>::value;
}
template<typename Base, typename T>
inline bool instanceof(const T) {
   return is_base_of<Base, T>::value;
}

template<typename Parameters>
static void readParameters(Parameters & sp ) {
  std::ifstream inFile1;
  std::stringstream strStream1;

  inFile1.open(configFileName.c_str());//open the input file
  if (inFile1.fail())
    EXC_PRINT("Cannot open configuration file");
  strStream1 << inFile1.rdbuf();//read the file
  std::string str = strStream1.str();//str holds the content of the file

  nlohmann::json j_in = nlohmann::json::parse( str );

  CovidTool::from_json(j_in, sp);

  return;
}

template<typename PolType>
SolveProbResultType solveProb( PolType & pl, int & N, bool optVerbose) {
  StateSpaceType stateSp;
  ReachabilityAnalyserType ra(stateSp);

  using SolverType=CovidTool::Solver<StateType, PolType>;

  typename SolverType::Parameters sp;
  readParameters( sp );
  N = sp.sp.N;
  SolverType sol( stateSp, ra, pl );
  sol.setParameters(sp);

  if ( optVerbose )
    sol.setVerbose();
  SolFunType cTM = std::bind(&SolverType::solve, sol, std::placeholders::_1);

  std::pair< SolverResultType, double > RES = functTime1(cTM, (int) iterations);

  SolverResultType jp = RES.first;

  std::cout<<"============COMPUTATION COMPLETED =========="<<std::endl;
  std::cout<<"Number of states reached: "<<stateSp.getNumberOfStates()<<std::endl;
  std::cout<<"Time (seconds): "<<RES.second/1e9<<std::endl;
  std::cout<<"============================================"<<std::endl;

  double totProb = std::accumulate(jp.begin(),
				   jp.end(),
				   0.0,
				   [] (double sum, const std::pair<CovidTool::SSV,double> & item) {
				     return sum + item.second;
				   });
  if (std::abs(totProb-1.0) > sp.sp.eps_tot_prob)
    WARNING_PRINT("joint probabilities do not sum to 1");
  std::pair< std::vector< std::vector<double> >, std::vector<double> > mp =  marginalProbabilities(jp, pl, true, outPrefix.c_str());

  return mp;
}

void doSolveProb(SolveProbResultType & mp, int & N,
		 const int policyNum, const bool optVerbose) {
  switch(policyNum) {
  case 0 : {
    using PolicyType=CovidTool::ConstantPolicy<StateType>;
    PolicyType pl;

    mp = solveProb( pl, N, optVerbose );
    break;
  }
  case 1 : {
    using PolicyType=CovidTool::AdaptiveSymptPolicy<StateType>;
    PolicyType pl;

    mp = solveProb( pl, N, optVerbose );
    break;
  }
  case 2: {
    using PolicyType=CovidTool::AdaptiveAsymptPolicy<StateType>;
    PolicyType pl;

    mp = solveProb( pl, N, optVerbose );
    break;
  }
  default:
    EXC_PRINT("Unrecognised solve option!");
  }
}

template<typename PolType>
void dumpMCModels(PolType & pl,
		  const std::string outPrefix,
		  const bool optMCAllInit,
		  const bool optStormDRNOut,
		  const bool optPOMDP,
		  const bool optVerbose) {
  bool ret = true;
  using EP = CovidTool::EmptyPolicy<StateType>;
  bool isMDP = (typeid(pl) == typeid(EP)); // !instanceof<EP>(pl);
  std::string ms = (isMDP) ? std::string("MDP") : std::string("DTMC");
  std::cout << "Dump " << ms << " model with policy " << pl.toString()
	    << ": start" << std::endl;
  try {
    ReachabilityAnalyserType::Parameters sp;
    readParameters(sp);
    // StateType::setParameters(sp.sp);
    TVar t0, t1, t2, t3, t4;
    StateSpaceType stateSp;
    ReachabilityAnalyserType ra(sp.initialStates, stateSp);
    ra.setParameters(sp);
    if (optVerbose)
      ra.setVerbose();

    VERBOSE_PRINT( "Computing reachable and transition: start", optVerbose);

    t0 = std::chrono::high_resolution_clock::now();
    ra.computeTransitionsWithReachable(false, pl, sp.maxReachIter);
    t1 = std::chrono::high_resolution_clock::now();

    VERBOSE_PRINT("Computing reachable and transition: done", optVerbose);
    VERBOSE_PRINT(std::string("Computing reachable and transition: Time(s): ") +
		  std::to_string(std::chrono::duration_cast<std::chrono::nanoseconds>(t1-t0).count()/1e9),
		  optVerbose
		  );

    if ((doPrism.compare("s") == 0) || (doStorm.compare("s") == 0)) {
      VERBOSE_PRINT("Dumping symbolic model: start", optVerbose);
      ret = ra.dumpPrismModel(outPrefix, isMDP, optPOMDP, optMCAllInit);
      t2 = std::chrono::high_resolution_clock::now();
      VERBOSE_PRINT(std::string("Dumping symbolic model: Time(s):") +
		    std::to_string(std::chrono::duration_cast<std::chrono::nanoseconds>(t2-t1).count()/1e9),
		    optVerbose
		    );

    } else {
      t2 = t1;
    }

    if (doPrism.compare("e") == 0) {
      if (optVerbose)
	std::cout << "Dumping Prism Explicit model: start" << std::endl;
      if (optPOMDP) {
	std::cerr << "Dumping Prism Explicit model as table do not support POMDP!" << std::endl;
	return;
      }
      ret = ra.dumpPrismExplicitModel(outPrefix, isMDP, optMCAllInit);
      t3 = std::chrono::high_resolution_clock::now();
      if (optVerbose)
	std::cout << "Dumping Prism Explicit model: done" << std::endl
		  << "Dumping Prism Explicit model: Time(s): "
		  << std::chrono::duration_cast<std::chrono::nanoseconds>(t3-t2).count()/1e9
		  << std::endl;
    } else {
      t3 = t2;
    }

    if (doStorm.compare("e") == 0) {
      if (optVerbose)
	std::cout << "Dumping Storm Explicit model: start" << std::endl;
      if (optStormDRNOut) {
	ret = ra.dumpStormDRNModel(outPrefix, isMDP, optPOMDP, optMCAllInit);
      } else {
	if (optPOMDP) {
	  std::cerr << "Dumping Storm Explicit model as table do not support POMDP!" << std::endl;
	  return;
	}
	ret = ra.dumpStormExplicitModel(outPrefix, isMDP, optMCAllInit);
      }
      t4 = std::chrono::high_resolution_clock::now();
      if (optVerbose)
	std::cout << "Dumping Storm Explicit model: done" << std::endl
		  << "Dumping Storm Explicit model: Time(s): "
		  << std::chrono::duration_cast<std::chrono::nanoseconds>(t3-t2).count()/1e9
		  << std::endl;
    } else {
      t4 = t3;
    }

    std::cout << "Dump " << ms << " model with policy " << pl.toString()
	      << ": done" << std::endl;
  } catch (Exc & e) {
    std::cerr << "Dumping Prism Symbolic Model failed" << std::endl;
    return;
  }
}

void doDumpMCModels(const int policyNum,
		    const std::string outPrefix,
		    const bool optMCAllInit,
		    const bool optStormDRNOut,
		    const bool optPOMDP,
		    const bool optVerbose) {
  switch(policyNum) {
  case -1: {
   using PolicyType=CovidTool::EmptyPolicy<StateType>;
    PolicyType pl;

    dumpMCModels( pl, outPrefix, optMCAllInit,
		  optStormDRNOut, optPOMDP, optVerbose );
    break;
  }
  case 0 : {
    using PolicyType=CovidTool::ConstantPolicy<StateType>;
    PolicyType pl;

    dumpMCModels(pl, outPrefix, optMCAllInit,
		 optStormDRNOut, optPOMDP, optVerbose );
    break;
  }
  case 1 : {
    using PolicyType=CovidTool::AdaptiveSymptPolicy<StateType>;
    PolicyType pl;

    dumpMCModels(pl, outPrefix, optMCAllInit,
		 optStormDRNOut, optPOMDP, optVerbose );
    break;
  }
  case 2: {
    using PolicyType=CovidTool::AdaptiveAsymptPolicy<StateType>;
    PolicyType pl;

    dumpMCModels(pl, outPrefix, optMCAllInit,
		 optStormDRNOut, optPOMDP, optVerbose );
    break;
  }
  default:
    EXC_PRINT("Unrecognised policy option for MC model dumping!");
  }
}

void generateDumps() {
  bool ret = true;
  std::cout << "Dump Models: start" << std::endl;
  try {
    ReachabilityAnalyserType::Parameters sp;
    readParameters(sp);
    // StateType::setParameters(sp.sp);
    TVar t0, t1, t2, t3, t4;
    StateSpaceType stateSp;
    ReachabilityAnalyserType ra(sp.initialStates, stateSp);
    ra.setParameters(sp);
    if (optVerbose)
      ra.setVerbose();

    VERBOSE_PRINT( "Computing reachable and transition: start", optVerbose);

    t0 = std::chrono::high_resolution_clock::now();
    CovidTool::EmptyPolicy<StateType> fakePol;
    ra.computeTransitionsWithReachable(false, fakePol, sp.maxReachIter);
    t1 = std::chrono::high_resolution_clock::now();

    VERBOSE_PRINT("Computing reachable and transition: done", optVerbose);
    VERBOSE_PRINT(std::string("Computing reachable and transition: Time(s): ") +
		  std::to_string(std::chrono::duration_cast<std::chrono::nanoseconds>(t1-t0).count()/1e9),
		  optVerbose
		  );

    if ((doPrism.compare("s") == 0) || (doStorm.compare("s") == 0)) {
      VERBOSE_PRINT("Dumping symbolic model: start", optVerbose);
      ret = ra.dumpPrismModel(outPrefix, false);
      t2 = std::chrono::high_resolution_clock::now();
      VERBOSE_PRINT(std::string("Dumping symbolic model: Time(s):") +
		    std::to_string(std::chrono::duration_cast<std::chrono::nanoseconds>(t2-t1).count()/1e9),
		    optVerbose
		    );

    } else {
      t2 = t1;
    }

    if (doPrism.compare("e") == 0) {
      if (optVerbose)
	std::cout << "Dumping Prism Explicit model: start" << std::endl;
      ret = ra.dumpPrismExplicitModel(outPrefix, false);
      t3 = std::chrono::high_resolution_clock::now();
      if (optVerbose)
	std::cout << "Dumping Prism Explicit model: done" << std::endl
		  << "Dumping Prism Explicit model: Time(s): "
		  << std::chrono::duration_cast<std::chrono::nanoseconds>(t3-t2).count()/1e9
		  << std::endl;
    } else {
      t3 = t2;
    }

    if (doStorm.compare("e") == 0) {
      if (optVerbose)
	std::cout << "Dumping Storm Explicit model: start" << std::endl;
      ret = ra.dumpStormExplicitModel(outPrefix, false);
      t4 = std::chrono::high_resolution_clock::now();
      if (optVerbose)
	std::cout << "Dumping Storm Explicit model: done" << std::endl
		  << "Dumping Storm Explicit model: Time(s): "
		  << std::chrono::duration_cast<std::chrono::nanoseconds>(t3-t2).count()/1e9
		  << std::endl;
    } else {
      t4 = t3;
    }

    std::cout << "Dump Models: done" << std::endl;

  } catch (Exc & e) {
    std::cerr << "Dumping Prism Symbolic Model failed" << std::endl;
    return;
  }
}

void doPrintStats(SolveProbResultType & mp, int N, const std::string outFile) {
  std::ofstream of;
  of.open(outFile, ios::out | ios::app);
  int N1 = std::round(N*outPercDec);
  int N2 = std::round(N*outPercRec);
  std::cout<<"Probability that the deceased will be more than 20% of the population: ";
  std::cout<<1-mp.first.at(static_cast<int>(CovidTool::SSV_::StateNames::Deceased)).at(N1)<<std::endl;
  std::cout<<"Probability that the recovered will be more than 20% of the population: ";
  std::cout<<1-mp.first.at(static_cast<int>(CovidTool::SSV_::StateNames::Recovered)).at(N1)<<std::endl;
  std::cout<<"Probability that the susceptible will be more than 20% of the population: ";
  std::cout<<1-mp.first.at(static_cast<int>(CovidTool::SSV_::StateNames::Susceptible)).at(N1)<<std::endl;
  std::cout<<"Average action used: ";

  double averageAct = mp.second.size()-1;
  for (int i = 0; i < mp.second.size()-1; i++){
    averageAct -= mp.second.at(i);
  }
  std::cout<<averageAct<<std::endl;

  std::cout<<1-mp.first.at(static_cast<int>(CovidTool::SSV_::StateNames::Susceptible)).at(N1)<<std::endl;
  of<<1-mp.first.at(static_cast<int>(CovidTool::SSV_::StateNames::Deceased)).at(N1)<<
    ", "<<
    1-mp.first.at(static_cast<int>(CovidTool::SSV_::StateNames::Recovered)).at(N2)<<
    ", "<<
    averageAct<<
    std::endl;
  of.close();
}


int main(int argc, const char **argv)
{
  auto cli = lyra::help(showHelp)
    |lyra::opt(optVerbose)
    ["-V"]["--verbose"]("Enable/Disable verbosity (default false)")
    | lyra::opt(configFileName, "Name of the input configuration file")
    ["-i"]["--inputFile"]("Configuration Input File Name")
    | lyra::opt(doSolve)
    ["-L"]["--solve"]("Enable/Disable solving (default false)")
    | lyra::opt(iterations, "iterations")
    ["-t"]["--iterations"]("Number of iterations for the solver")
    | lyra::opt(policyNum, "-1|0|1|2")
    ["-Y"]["--policy"]("Type of policy to use for solving and for dumping Prism/Storm models: -1. No policy, 0. constant (default), 1. adaptive symptomatic, 2. adaptive asymptomatic")
    | lyra::opt(outFile, "output file")
    ["-O"]["--outputFile"]("The output file to append the result to")
    | lyra::opt(outPercDec, "percentile of deceased")
    ["-D"]["--outputPercentileDec"]("Percentile to use to compute the probability that there will be more deceased than the specified in this percentile: 0.2 (default)")
    | lyra::opt(outPercRec, "percentile of recovered")
    ["-E"]["--outputPercentileRec"]("Percentile to use to compute the probability that there will be more recovered than specified in this percentile: 0.8 (default)")
    | lyra::opt(doStorm, "e|E|s|S")
    ["-s"]["--storm"]("Enables dump of Storm model: e|E for explicit, s/S for symbolic. Depending on policy it will generate an MDP (policy -1) or a DTMC (policy != -1)")
    | lyra::opt(doPrism, "e|E|s|S")
    ["-p"]["--prism"]("Enables dump of Prism model: e|E for explicit, s/S for symbolic. Depending on policy it will generate an MDP (policy -1) or a DTMC (policy != -1)")
    | lyra::opt(outPrefix, "The prefix to use of dumping files")
    ["-P"]["--prefix"]("Prefix of the generated files")


    |lyra::opt(optStormDRNOut)
    ["-R"]["--drn"]("Enable/Disable DRN storm output for explicit model (default false)")
    |lyra::opt(optPOMDP)
    ["-o"]["--pomdp"]("Enable/Disable dump of POMDP (default false)")
    |lyra::opt(optMCAllInit)
    ["-A"]["--mcallinit"]("Consider all states as initial while dumping models for model checking (default false)")
    ;

  auto res = cli.parse({argc, argv});

  std::transform(doPrism.begin(), doPrism.end(), doPrism.begin(), ::tolower);
  std::transform(doStorm.begin(), doStorm.end(), doStorm.begin(), ::tolower);

  if (!res) {
    ERROR_PRINT(std::string("Error in command line: ") + res.errorMessage());
    std::cerr << cli << std::endl;
    return 1;
  }

  if (showHelp) {
    std::cout << cli << std::endl;
    return 1;
  }

  if ((doStorm.size() == 0) &&
      (doPrism.size() == 0) &&
      !doSolve) {
    std::cout << "At least one option among -p|--prism|-s|--storm|-L|--solve shall be provided" << std::endl;
    std::cout << "If not, the tool does nothing!" << std::endl;
    std::cout << "Use -h|-?|--help for the list of available options." << std::endl;
  }

  if (doStorm.size() > 0 &&
      doStorm.compare("e") != 0 &&
      doStorm.compare("s") != 0) {
    std::cerr << "--storm requires e or E or s or S as argument" << std::endl;
    return 1;
  }
  if (doPrism.size() > 0 &&
      doPrism.compare("e") != 0 &&
      doPrism.compare("s") != 0) {
    std::cerr << "--prism requires e or E or s or S as argument" << std::endl;
    return 1;
  }

  // This has to be changed!
  if (doSolve && (doPrism.size() != 0 || doStorm.size() != 0)) {
    std::cerr << "--prism/--storm cannot be used if --solve is true" << std::endl;
    return 1;
  }

  if ((policyNum != -1) && (policyNum != 0) &&
      (policyNum != 1) && (policyNum != 2)) {
    EXC_PRINT("Unrecognised policy solving option!");
  }

  if (doSolve && (policyNum == -1)) {
    EXC_PRINT("Policy -1 cannot be used in conjunction with --solve!!");
  }

  std::cout << "DRN : " << optStormDRNOut << std::endl;

  // End of check of the inputs

  if (doSolve) {
    SolveProbResultType mp;
    int N;
    doSolveProb(mp, N, policyNum, optVerbose);

    //std::pair< std::vector< std::vector<double> >, std::vector<double> > mp = marginalProbabilities(jp, pl, true, outPrefix.c_str());

    if ( !outFile.empty() ) {
      doPrintStats(mp, N, outFile);
    }
  }

  if ((doPrism.size() > 0) || (doStorm.size() > 0)) {
    doDumpMCModels(policyNum, outPrefix, optMCAllInit,
		   optStormDRNOut, optPOMDP, optVerbose);
    // generateDumps();
  }

  return 1;
}
