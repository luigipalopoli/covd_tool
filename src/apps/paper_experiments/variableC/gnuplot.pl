set terminal png  
set output  "deceased.png"  
set title "CDF of the deceased people"
set xlabel "Deceased #"
set ylabel "CDF"
    plot "test_M1_C1_Constant_Policy_1__deceased.mat"  u 1:2 w lp title "C = 1, M = 1", \
     "test_M1_C2_Constant_Policy_1__deceased.mat"  u 1:2 w lp title "C = 2, M = 1",\
     "test_M1_C3_Constant_Policy_1__deceased.mat"  u 1:2 w lp title "C = 3, M = 1",\
    "test_M1_C4_Constant_Policy_1__deceased.mat"  u 1:2 w lp title "C = 4, M = 1",\
    "test_M3_C1_Constant_Policy_3__deceased.mat"  u 1:2 w lp title "C = 1, M = 3", \
     "test_M3_C2_Constant_Policy_3__deceased.mat"  u 1:2 w lp title "C = 2, M = 3",\
     "test_M3_C3_Constant_Policy_3__deceased.mat"  u 1:2 w lp title "C = 3, M = 3",\
     "test_M3_C4_Constant_Policy_3__deceased.mat"  u 1:2 w lp title "C = 4, M = 3",\
     "test_M5_C1_Constant_Policy_5__deceased.mat"  u 1:2 w lp title "C = 1, M = 5", \
     "test_M5_C2_Constant_Policy_5__deceased.mat"  u 1:2 w lp title "C = 2, M = 5",\
     "test_M5_C3_Constant_Policy_5__deceased.mat"  u 1:2 w lp title "C = 3, M = 5",\
     "test_M5_C4_Constant_Policy_5__deceased.mat"  u 1:2 w lp title "C = 4, M = 5"

set terminal png  
set output  "recovered.png"  
set title "CDF of the recovered people"
set xlabel "Recovered #"
set ylabel "CDF"
    plot "test_M1_C1_Constant_Policy_1__recovered.mat"  u 1:2 w lp title "C = 1, M = 1", \
     "test_M1_C2_Constant_Policy_1__recovered.mat"  u 1:2 w lp title "C = 2, M = 1",\
     "test_M1_C3_Constant_Policy_1__recovered.mat"  u 1:2 w lp title "C = 3, M = 1",\
    "test_M1_C4_Constant_Policy_1__recovered.mat"  u 1:2 w lp title "C = 4, M = 1",\
    "test_M3_C1_Constant_Policy_3__recovered.mat"  u 1:2 w lp title "C = 1, M = 3", \
     "test_M3_C2_Constant_Policy_3__recovered.mat"  u 1:2 w lp title "C = 2, M = 3",\
     "test_M3_C3_Constant_Policy_3__recovered.mat"  u 1:2 w lp title "C = 3, M = 3",\
     "test_M3_C4_Constant_Policy_3__recovered.mat"  u 1:2 w lp title "C = 4, M = 3",\
     "test_M5_C1_Constant_Policy_5__recovered.mat"  u 1:2 w lp title "C = 1, M = 5", \
     "test_M5_C2_Constant_Policy_5__recovered.mat"  u 1:2 w lp title "C = 2, M = 5",\
     "test_M5_C3_Constant_Policy_5__recovered.mat"  u 1:2 w lp title "C = 3, M = 5",\
     "test_M5_C4_Constant_Policy_5__recovered.mat"  u 1:2 w lp title "C = 4, M = 5"


set terminal png  
set output  "susceptible.png"  
set title "CDF of the susceptible people"
set xlabel "Susceptible #"
set ylabel "CDF"
    plot "test_M1_C1_Constant_Policy_1__susceptible.mat"  u 1:2 w lp title "C = 1, M = 1", \
     "test_M1_C2_Constant_Policy_1__susceptible.mat"  u 1:2 w lp title "C = 2, M = 1",\
     "test_M1_C3_Constant_Policy_1__susceptible.mat"  u 1:2 w lp title "C = 3, M = 1",\
    "test_M1_C4_Constant_Policy_1__susceptible.mat"  u 1:2 w lp title "C = 4, M = 1",\
    "test_M3_C1_Constant_Policy_3__susceptible.mat"  u 1:2 w lp title "C = 1, M = 3", \
     "test_M3_C2_Constant_Policy_3__susceptible.mat"  u 1:2 w lp title "C = 2, M = 3",\
     "test_M3_C3_Constant_Policy_3__susceptible.mat"  u 1:2 w lp title "C = 3, M = 3",\
     "test_M3_C4_Constant_Policy_3__susceptible.mat"  u 1:2 w lp title "C = 4, M = 3",\
     "test_M5_C1_Constant_Policy_5__susceptible.mat"  u 1:2 w lp title "C = 1, M = 5", \
     "test_M5_C2_Constant_Policy_5__susceptible.mat"  u 1:2 w lp title "C = 2, M = 5",\
     "test_M5_C3_Constant_Policy_5__susceptible.mat"  u 1:2 w lp title "C = 3, M = 5",\
     "test_M5_C4_Constant_Policy_5__susceptible.mat"  u 1:2 w lp title "C = 4, M = 5"
    
