#set terminal postfile       (These commented lines would be used to )
#set output  "d1_plot.ps"    (generate a postscript file.
#  set terminal png
# set output "susc.png"
# set title "CDF of the susceptible people"
# set xlabel "Susceptible #"
# set ylabel "CDF"
#     plot "test9_1_Constant_Policy_3__deceased.mat"  u 1:2 w lp title "test 1"

set terminal png
set output "converg.png"
set key outside above
set title "Probability of more than 20\% causualties"
set xlabel "Iteration"
set ylabel "Pr(deceased > 0.2 N)"
set key outside
plot "res_1_2.mat" u ($0*5+5):1 w lp title "M = 2, Initial state 1",\
    "res_2_2.mat" u ($0*5+5):1 w lp title "M = 2, Initial state 2",\
    "res_3_2.mat" u ($0*5+5):1 w lp title "M = 2, Initial state 3",\
    "res_1_5.mat" u ($0*5+5):1 w lp title "M = 5, Initial state 1",\
    "res_2_5.mat" u ($0*5+5):1 w lp title "M = 5, Initial state 2",\
    "res_3_5.mat" u ($0*5+5):1 w lp title "M = 5, Initial state 3"


set terminal png
set output  "deceased.png"
set key outside above
set title "Steady State CDF of the deceased people"
set xlabel "Deceased #"
set ylabel "CDF"
plot "test5_2_Constant_Policy_2__deceased.mat"  u 1:2 w lp title "M = 2", \
    "test5_5_Constant_Policy_5__deceased.mat"  u 1:2 w lp title "M = 5"

set terminal png
set output  "recovered.png"
set key outside above
set title "Steady State CDF of the recovered people"
set xlabel "Recovered #"
set ylabel "CDF"
plot "test5_2_Constant_Policy_2__recovered.mat"  u 1:2 w lp title "M = 2", \
    "test5_5_Constant_Policy_5__recovered.mat"  u 1:2 w lp title "M = 5"


set terminal png
set output  "susceptible.png"
set key outside above
set title "Steady State CDF of the recovered people"
set xlabel "Susceptible #"
set ylabel "CDF"
plot "test5_2_Constant_Policy_2__susceptible.mat"  u 1:2 w lp title "M = 2", \
     "test5_5_Constant_Policy_5__susceptible.mat"  u 1:2 w lp title "M = 5"

# latest aggregated
set terminal png
set output "dec-rec-susc.png"
set key outside above
#set title "Steady State CDF of the deceased, recovered and susceptible people"
set xlabel "(Deceased | Recovered | Susceptible) #"
set ylabel "CDF"
plot "test5_2_Constant_Policy_2__deceased.mat"     u 1:2 w lp pt  1 lc 1 dt 1 notitle, \
     "test5_5_Constant_Policy_5__deceased.mat"     u 1:2 w lp pt  1 lc 1 dt 2 notitle, \
     "test5_2_Constant_Policy_2__susceptible.mat"  u 1:2 w lp pt  3 lc 3 dt 1 notitle, \
     "test5_5_Constant_Policy_5__susceptible.mat"  u 1:2 w lp pt  3 lc 3 dt 2 notitle, \
     "test5_2_Constant_Policy_2__recovered.mat"    u 1:2 w lp pt 12 lc 8 dt 1 notitle, \
     "test5_5_Constant_Policy_5__recovered.mat"    u 1:2 w lp pt 12 lc 8 dt 2 notitle, \
     NaN with lines lc 1 title = "Deceased", \
     NaN with lines lc 3 title = "Susceptible", \
     NaN with lines lc 8 title = "Deceased", \
     NaN with lines dt 1 title = "M = 2", \
     NaN with lines dt 2 title = "M = 5"
# We can consider adding at e.g.
#  plot NaN with lines dt 2 title = "M = 5" at 0.5, 0.5