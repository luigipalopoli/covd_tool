set terminal png  
set output  "probDying.png"
set key outside above
set title "Probability of more than 20\% causualties"
set xlabel "Iteration"
set ylabel "Pr(deceased > 0.2 N)"
plot "res_1.mat" u($0*2+2):1 w lp title "Adaptive Symptomatic" ,\
    "res_2.mat" u($0*2+2):1 w lp title "Adaptive Asymptomatic" ,\
    "resLow.mat" u($0*2+2):1 w lp title "Const. M = 1" ,\
    "resHigh.mat" u($0*2+2):1 w lp title "Const. M = 5"

set terminal png  
set output  "averageAct.png"
set key outside above
set title "Average value of M"
set xlabel "Iteration"
set ylabel "Mean(M)"
plot "res_1.mat" u($0*2+2):3 w lp title "Adaptive Symptomatic" ,\
    "res_2.mat" u($0*2+2):3 w lp title "Adaptive Asymptomatic" ,\
    "resLow.mat" u($0*2+2):3 w lp title "Const. M = 1" ,\
    "resHigh.mat" u($0*2+2):3 w lp title "Const. M = 5"

    
    
  

# set terminal png
# set output  "deceased.png"  
# set title "CDF of the deceased people"
# set xlabel "Deceased #"
# set ylabel "CDF"
#     plot "testAdapt__Adaptive_Sympt_Policy_0.100000_0.500000__deceased.mat"  u 1:2 w lp title "Contr(10%, 50%)", \
#     "testAdapt1__Adaptive_Sympt_Policy_0.050000_0.150000__deceased.mat"  u 1:2 w lp title "Contr(5%, 15%)", \
#     "testAdapt2__Adaptive_Sympt_Policy_0.050000_0.100000__deceased.mat"  u 1:2 w lp title "Contr(5%, 10%)"

# set output  "recovered.png"  
# set title "CDF of the recovered people"
# set xlabel "Recovered #"
# set ylabel "CDF"
#     plot "testAdapt__Adaptive_Sympt_Policy_0.100000_0.500000__recovered.mat"  u 1:2 w lp title "Contr(10%, 50%)", \
#     "testAdapt1__Adaptive_Sympt_Policy_0.050000_0.150000__recovered.mat"  u 1:2 w lp title "Contr(5%, 15%)", \
#     "testAdapt2__Adaptive_Sympt_Policy_0.050000_0.100000__recovered.mat"  u 1:2 w lp title "Contr(5%, 10%)"

    
# set output  "susceptible.png"  
# set title "CDF of the susceptible people"
# set xlabel "Recovered #"
# set ylabel "CDF"
#     plot "testAdapt__Adaptive_Sympt_Policy_0.100000_0.500000__susceptible.mat"  u 1:2 w lp title "Contr(10%, 50%)", \
#     "testAdapt1__Adaptive_Sympt_Policy_0.050000_0.150000__susceptible.mat"  u 1:2 w lp title "Contr(5%, 15%)", \
#     "testAdapt2__Adaptive_Sympt_Policy_0.050000_0.100000__susceptible.mat"  u 1:2 w lp title "Contr(5%, 10%)"

    
# set output  "actions.png"  
# set title "PMF of the actions people"
# set xlabel "Actions #"
# set ylabel "PMF"
#     plot "PMF_testAdapt__Adaptive_Sympt_Policy_0.100000_0.500000__actions.mat"  u 1:2 w lp title "Contr(10%, 50%)", \
#     "PMF_testAdapt1__Adaptive_Sympt_Policy_0.050000_0.150000__actions.mat"  u 1:2 w lp title "Contr(5%, 15%)", \
#     "PMF_testAdapt2__Adaptive_Sympt_Policy_0.050000_0.100000__actions.mat"  u 1:2 w lp title "Contr(5%, 10%)"
