# set terminal png  
# set output  "deceased.png"  
# set key outside above
# set title "Steady State CDF of the deceased people"
# set xlabel "Deceased #"
# set ylabel "CDF"
#     plot "test1_Constant_Policy_1__deceased.mat"  u 1:2 w lp title "M = 1", \
#      "test3_Constant_Policy_3__deceased.mat"  u 1:2 w lp title "M = 3",\
#      "test5_Constant_Policy_5__deceased.mat"  u 1:2 w lp title "M = 5"
    
# # set terminal png  
# # set output  "recovered.png"  
# # set title "CDF of the recovered people"
# # set xlabel "Deceased #"
# # set ylabel "CDF"
# #     plot "test1_Constant_Policy_1__deceased.mat"  u 1:2 w lp title "M = 1", \
# #      "test2_Constant_Policy_2__recovered.mat"  u 1:2 w lp title "M = 2", \
# #      "test3_Constant_Policy_3__recovered.mat"  u 1:2 w lp title "M = 3",\
# #      "test4_Constant_Policy_4__recovered.mat"  u 1:2 w lp title "M = 4",\
# #      "test5_Constant_Policy_5__recovered.mat"  u 1:2 w lp title "M = 5"

# set terminal png
# set key outside above
# set output  "susceptible.png"  
# set title "Steady State CDF of the Susceptible people"
# set xlabel "Susceptible #"
# set ylabel "CDF"
#     plot "test1_Constant_Policy_1__susceptible.mat"  u 1:2 w lp title "M = 1", \
#     "test3_Constant_Policy_3__susceptible.mat"  u 1:2 w lp title "M = 3",\
#     "test5_Constant_Policy_5__susceptible.mat"  u 1:2 w lp title "M = 5"



set terminal png
set key outside above
set output  "probDying.png"  
set title "Probability that deceased will be more than 20\% of the population"
set xlabel "M"
set ylabel "Prob(Dk > 0.2 N) "
plot "res_1.mat" u ($0 + 1):1 w lp  title "C = 1" ,\
    "res_2.mat" u ($0 + 1):1 w lp  title "C = 2" ,\
    "res_3.mat" u ($0 + 1):1 w lp  title "C = 3" ,\
    "res_5.mat" u ($0 + 1):1 w lp  title "C = 5" 
