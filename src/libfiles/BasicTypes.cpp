#include "BasicTypes.hpp"
#include "TransProb.hpp"
#include "util/algo.hpp"
#include <numeric>
#include <fstream>

bool CovidTool::SSV_::Parameters::checkConsistency() const {
  CHECK_PROB( omega );
  CHECK_PROB( beta  );
  CHECK_PROB( delta );

  CHECK_PROB( beta + delta);

  CHECK_PROB( mu );
  CHECK_PROB( psi );
  CHECK_PROB( alpha );

  CHECK_PROB( mu+ alpha+ psi );

  CHECK_PROB( sigma );
  CHECK_PROB( xi );

  CHECK_PROB( sigma + xi);

  return true;
};



CovidTool::SSV_::Parameters CovidTool::SSV_::parms;
bool CovidTool::SSV_::parmsSet;

template<>
CovidTool::StateWrapper<CovidTool::SSV_>::Parameters & CovidTool::StateWrapper<CovidTool::SSV_>::parms = CovidTool::SSV_::parms;

CovidTool::ESV_::Parameters CovidTool::ESV_::parms;
bool CovidTool::ESV_::parmsSet;

template<>
CovidTool::StateWrapper<CovidTool::ESV_>::Parameters & CovidTool::StateWrapper<CovidTool::ESV_>::parms = CovidTool::ESV_::parms;

// std::map< CovidTool::SSV_ , std::map< CovidTool::NextState<CovidTool::SSV_>, double >   > CovidTool::SSV::_exploredStates;


std::map< CovidTool::NextState<CovidTool::SSV_>, double >  CovidTool::SSV_::nextStateDirect(const SSV_::ActionType & action) const {
  std::map< CovidTool::NextState<SSV_>, double > work;
  for (unsigned d1 = 0; d1 <= s; d1++)
    for (unsigned d2 = 0; d2 <= a; d2++)
      for (unsigned d3 = 0; d3 <= a - d2; d3++)
	for (unsigned d4 = 0; d4 <= i; d4++)
	  for (unsigned d5 = 0; d5 <= i-d4; d5++)
	    for (unsigned d6 = 0; d6 <= i-d4-d5; d6++)
	      for (unsigned d7 = 0; d7 <= o; d7++)
		for (unsigned d8 = 0; d8 <= o - d7; d8++) {
		  CovidTool::NextState<CovidTool::SSV_> ns(
				      action,
				      SSV_(s-d1,
					  a+d1-d2-d3,
					  i + d2 - d4 - d5 -d6,
					  r + d3 + d4 + d8,
					  o + d5 - d7 - d8,
					  d + d6+d7));

		  auto stateIt = work.find(ns);
		  if (stateIt == work.end())
		    {
		      auto res =work.insert(std::pair<NextState<SSV_>, double>(ns, 0.0));

		      if (!res.second)
			EXC_PRINT("Insertion into set failed");
		      stateIt = res.first;
		    }
		  stateIt->second +=  transProbDelta(*this, std::vector<unsigned>({d1, d2, d3, d4, d5, d6, d7, d8}), action);

		}
  return work;
}


bool CovidTool::SSV_::checkConsistency() const {
  if (!parmsSet)
    EXC_PRINT("Cannot perform consistency check without a proper initialisation");
  return ((s + a + i + r + o + d) == parms.N);

}


// std::map< CovidTool::NextState<CovidTool::SSV>, double>  CovidTool::SSV::nextStates() {
//   if (!parmsSet)
//     EXC_PRINT("Cannot compute nextState if you did not initialise the parameters");
// #ifndef NO_CACHE_EXPLORED_STATES
//   auto it = _exploredStates.find(*this);
//   if ( it != _exploredStates.end() )
//     return it->second;
// #endif
//   std::map < CovidTool::NextState<CovidTool::SSV>, double >  res;
//   for ( unsigned code = parms.getMinActionNumber(); code <= parms.getActionNumber(); code++ ) {
//     auto m = nextStates(code);
//     for (auto it1 = m.begin(); it1 != m.end(); it1++)
//       res.insert(*it1);

//     //res.insert(m.begin(), m.end());
//     //res.merge(m);
//   }
// #ifndef NO_CACHE_EXPLORED_STATES
//   auto insOk = _exploredStates.insert(std::pair<CovidTool::SSV , std::map< CovidTool::NextState<SSV>, double > >   (*this, res));
//   if ( insOk.second == false )
//     EXC_PRINT("Could not create map entry.");
// #endif
//   return res;
// }

// std::map< CovidTool::NextState<CovidTool::SSV>, double > CovidTool::SSV::nextStates( const ActionType & action )  {
//   if (!parmsSet)
//     EXC_PRINT("Cannot compute nextState if you did not initialise the parameters");

//   if ( !checkConsistency() ) {
//     std::string msg = dump();
//     TRACE(msg);
//     EXC_PRINT("Cannot compute next function for inconsistent state");
//   }

//   std::map< CovidTool::NextState<SSV>, double > work;
//   for (unsigned d1 = 0; d1 <= s; d1++)
//     for (unsigned d2 = 0; d2 <= a; d2++)
//       for (unsigned d3 = 0; d3 <= a - d2; d3++)
// 	for (unsigned d4 = 0; d4 <= i; d4++)
// 	  for (unsigned d5 = 0; d5 <= i-d4; d5++)
// 	    for (unsigned d6 = 0; d6 <= i-d4-d5; d6++)
// 	      for (unsigned d7 = 0; d7 <= o; d7++)
// 		for (unsigned d8 = 0; d8 <= o - d7; d8++) {
// 		  CovidTool::NextState<CovidTool::SSV> ns(
// 				      action,
// 				      SSV(s-d1,
// 					  a+d1-d2-d3,
// 					  i + d2 - d4 - d5 -d6,
// 					  r + d3 + d4 + d8,
// 					  o + d5 - d7 - d8,
// 					  d + d6+d7));

// 		  auto stateIt = work.find(ns);
// 		  if (stateIt == work.end())
// 		    {
// 		      auto res =work.insert(std::pair<NextState<SSV>, double>(ns, 0.0));

// 		      if (!res.second)
// 			EXC_PRINT("Insertion into set failed");
// 		      stateIt = res.first;
// 		    }
// 		  stateIt->second +=  transProbDelta(*this, std::vector<unsigned>({d1, d2, d3, d4, d5, d6, d7, d8}), action);

// 		}

//   double totProb = std::accumulate(work.begin(),
// 				   work.end(),
// 				   0.0,
// 				   [] (double sum, const std::pair<NextState<SSV>,double> & item) {
// 				     return sum + item.second;
// 				   });
//   assert(abs(totProb-1)<parms.eps_tot_prob);

//   erase_if(work,
// 	   [] (const std::pair<NextState<SSV>,double> & item) {
// 	     return item.second < parms.eps_prob;
// 	   });
//   totProb = std::accumulate(work.begin(),
// 			    work.end(),
// 			    0.0,
// 			    [] (double sum, const std::pair<NextState<SSV>,double> & item) {
// 			      return sum + item.second;
// 			    });

//   for (auto el : work)
//     el.second /= totProb;
// #ifndef NDEBUG
//   totProb = std::accumulate(work.begin(),
// 			    work.end(),
// 			    0.0,
// 			    [] (double sum, const std::pair<NextState<SSV>,double> & item) {
// 			      return sum + item.second;
// 			    });
//   assert(abs(totProb-1)<parms.eps_tot_prob);
// #endif
//   return work;
// }


// std::vector< std::vector<double> > CovidTool::marginalProbabilities(const std::map<CovidTool::SSV,double> & jointProb,  bool save, const char * filePrefix) {
//   std::vector< std::vector<double> >  work{
//     std::vector<double>(CovidTool::SSV::parms.N + 1, 0),
//     std::vector<double>(CovidTool::SSV::parms.N + 1, 0),
//     std::vector<double>(CovidTool::SSV::parms.N + 1, 0),
//     std::vector<double>(CovidTool::SSV::parms.N + 1, 0),
//     std::vector<double>(CovidTool::SSV::parms.N + 1, 0),
//     std::vector<double>(CovidTool::SSV::parms.N + 1, 0)
//   };

//     ;
//   for ( auto el : jointProb ) {
//     (work.at(static_cast<int>(CovidTool::SSV::StateNames::Susceptible))).at(el.first.s) += el.second;
//     (work.at(static_cast<int>(CovidTool::SSV::StateNames::Asymptomatic))).at(el.first.a) += el.second;
//     (work.at(static_cast<int>(CovidTool::SSV::StateNames::Symptomatic))).at(el.first.i) += el.second;
//     (work.at(static_cast<int>(CovidTool::SSV::StateNames::Recovered))).at(el.first.r) += el.second;
//     (work.at(static_cast<int>(CovidTool::SSV::StateNames::Hospitalised))).at(el.first.o) += el.second;
//     (work.at(static_cast<int>(CovidTool::SSV::StateNames::Deceased))).at(el.first.d) += el.second;
//   }
//   if (save) {
//     ofstream suscStream, asympStream, sympStream, recStream, hospStream, decStream;
//     suscStream.open(std::string(filePrefix)+std::string("_susceptible.mat"), ios::out | ios::trunc );
//     asympStream.open(std::string(filePrefix)+std::string("_asymptomatic.mat"), ios::out | ios::trunc);
//     sympStream.open(std::string(filePrefix)+std::string("_symptomatic.mat"), ios::out | ios::trunc);
//     recStream.open(std::string(filePrefix)+std::string("_recovered.mat"), ios::out | ios::trunc);
//     hospStream.open(std::string(filePrefix)+std::string("_hospitalised.mat"), ios::out | ios::trunc);
//     decStream.open(std::string(filePrefix)+std::string("_deceased.mat"), ios::out | ios::trunc);

//     for (int i = 0; i <= CovidTool::SSV::parms.N ; i++) {
//       suscStream<<i<<'\t'<< (work.at(static_cast<int>(CovidTool::SSV::StateNames::Susceptible))).at(i)<<std::endl;
//       asympStream<<i<<'\t'<< (work.at(static_cast<int>(CovidTool::SSV::StateNames::Asymptomatic))).at(i)<<std::endl;
//       sympStream<<i<<'\t'<<(work.at(static_cast<int>(CovidTool::SSV::StateNames::Symptomatic))).at(i)<<std::endl;
//       recStream<<i<<'\t'<<(work.at(static_cast<int>(CovidTool::SSV::StateNames::Recovered))).at(i)<<std::endl;
//       hospStream<<i<<'\t'<<(work.at(static_cast<int>(CovidTool::SSV::StateNames::Hospitalised))).at(i)<<std::endl;
//       decStream<<i<<'\t'<<(work.at(static_cast<int>(CovidTool::SSV::StateNames::Deceased))).at(i)<<std::endl;
//     }
//   }



//   return work;
// };



void CovidTool::saveProbs(const std::vector< std::vector<double> >  & work, std::vector<double> & workA, const std::string & filePrefix, const std::string & policyId) {
    ofstream suscStream, asympStream, sympStream, recStream, hospStream, decStream, actStream;
    suscStream.open(std::string(filePrefix)+std::string("_")+policyId+std::string("_")+std::string("_susceptible.mat"), ios::out | ios::trunc );
    asympStream.open(std::string(filePrefix)+std::string("_")+policyId+std::string("_")+std::string("_asymptomatic.mat"), ios::out | ios::trunc);
    sympStream.open(std::string(filePrefix)+std::string("_")+policyId+std::string("_")+std::string("_symptomatic.mat"), ios::out | ios::trunc);
    recStream.open(std::string(filePrefix)+std::string("_")+policyId+std::string("_")+std::string("_recovered.mat"), ios::out | ios::trunc);
    hospStream.open(std::string(filePrefix)+std::string("_")+policyId+std::string("_")+std::string("_hospitalised.mat"), ios::out | ios::trunc);
    decStream.open(std::string(filePrefix)+std::string("_")+policyId+std::string("_")+std::string("_deceased.mat"), ios::out | ios::trunc);
    actStream.open(std::string(filePrefix)+std::string("_")+policyId+std::string("_")+std::string("_actions.mat"), ios::out | ios::trunc);

    for (int i = 0; i <= CovidTool::SSV_::parms.N ; i++) {
      suscStream<<i<<'\t'<< (work.at(static_cast<int>(CovidTool::SSV_::StateNames::Susceptible))).at(i)<<std::endl;
      asympStream<<i<<'\t'<< (work.at(static_cast<int>(CovidTool::SSV_::StateNames::Asymptomatic))).at(i)<<std::endl;
      sympStream<<i<<'\t'<<(work.at(static_cast<int>(CovidTool::SSV_::StateNames::Symptomatic))).at(i)<<std::endl;
      recStream<<i<<'\t'<<(work.at(static_cast<int>(CovidTool::SSV_::StateNames::Recovered))).at(i)<<std::endl;
      hospStream<<i<<'\t'<<(work.at(static_cast<int>(CovidTool::SSV_::StateNames::Hospitalised))).at(i)<<std::endl;
      decStream<<i<<'\t'<<(work.at(static_cast<int>(CovidTool::SSV_::StateNames::Deceased))).at(i)<<std::endl;
    }
    for (int i = CovidTool::SSV_::parms.Mmin; i <= CovidTool::SSV_::parms.M ; i ++)
      actStream<<i<<'\t'<<(workA.at(i))<<std::endl;


}



