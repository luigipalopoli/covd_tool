/*
 * Source file containing the code to compute the probabilities
 *
 * Copyright (C) 2020, University of Trento
 * Authors: Luigi Palopoli <luigi.palopoli@unitn.it>, Marco Roveri <marco.roveri@unitn.it>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
/** \file TransProb.cpp
 * Source file containing the code to compute the probabilities
 */
#include <cmath>
#include <cassert>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_sf_gamma.h>
#include "TransProb.hpp"

#include <tuple>
#include <ext/pb_ds/assoc_container.hpp>

using namespace __gnu_pbds;

// gp_hash_table<int, int> table;
/*
struct chash {
    int operator()(pii x) const { return x.first* 31 + x.second; }
};
gp_hash_table<pii, int, chash> table;
*/

/*
tuplehash(PyTupleObject *v)
{
    Py_uhash_t x;  // Unsigned for defined overflow behavior.
    Py_hash_t y;
    Py_ssize_t len = Py_SIZE(v);
    PyObject **p;
    Py_uhash_t mult = _PyHASH_MULTIPLIER;
    x = 0x345678UL;
    p = v->ob_item;
    while (--len >= 0) {
        y = PyObject_Hash(*p++);
        if (y == -1)
            return -1;
        x = (x ^ y) * mult;
        // the cast might truncate len; that doesn't change hash stability
        mult += (Py_hash_t)(82520UL + len + len);
    }
    x += 97531UL;
    if (x == (Py_uhash_t)-1)
        x = -2;
    return x;
}
std::hash<double>{}(myFloat);
https://github.com/mochow13/competitive-programming-library/blob/master/Data%20Structures/GP%20Hash%20Table.cpp
https://usaco.guide/gold/faster-hashmap

    size_t res = std::hash<unsigned>{}(std::get<0>(x));
    res += std::hash<unsigned>{}(std::get<1>(x));
    res += std::hash<unsigned>{}(std::get<2>(x));
    res += std::hash<unsigned>{}(std::get<3>(x));
    res += std::hash<double>{}(std::get<4>(x));
    res += std::hash<double>{}(std::get<5>(x));
    res += std::hash<double>{}(std::get<6>(x));
    res += std::hash<double>{}(std::get<7>(x));
    return res;
 */
#define _HASH_MULTIPLIER 1000003UL

typedef std::tuple<int,int,int,int,double,double,double,double> _typetuple;

inline size_t hfunct(const _typetuple & x) {
  unsigned long res = 0x345678UL;
  unsigned long y =  std::hash<unsigned>{}(std::get<0>(x));
  unsigned long mult = _HASH_MULTIPLIER;
  const unsigned long umin = (unsigned long)-1;
  if (y == umin)
    return umin;
  res = (res ^ y) * mult;
  mult += (unsigned long)(82520UL + 8UL + 8UL);
  y =  std::hash<unsigned>{}(std::get<1>(x));
  if (y == umin)
    return umin;
  res = (res ^ y) * mult;
  mult += (unsigned long)(82520UL + 8UL + 8UL);
  y =  std::hash<unsigned>{}(std::get<2>(x));
  if (y == umin)
    return umin;
  res = (res ^ y) * mult;
  mult += (unsigned long)(82520UL + 8UL + 8UL);
  y =  std::hash<unsigned>{}(std::get<3>(x));
  if (y == umin)
    return umin;
  res = (res ^ y) * mult;
  mult += (unsigned long)(82520UL + 8UL + 8UL);
  y =  std::hash<double>{}(std::get<4>(x));
  if (y == umin)
    return umin;
  res = (res ^ y) * mult;
  mult += (unsigned long)(82520UL + 8UL + 8UL);
  y =  std::hash<double>{}(std::get<5>(x));
  if (y == umin)
    return umin;
  res = (res ^ y) * mult;
  mult += (unsigned long)(82520UL + 8UL + 8UL);
  y =  std::hash<double>{}(std::get<6>(x));
  if (y == umin)
    return umin;
  res = (res ^ y) * mult;
  mult += (unsigned long)(82520UL + 8UL + 8UL);
  y =  std::hash<double>{}(std::get<7>(x));
  if (y == umin)
    return umin;
  res = (res ^ y) * mult;
  mult += (unsigned long)(82520UL + 8UL + 8UL);
  res += 97531UL;
  if (res == umin)
    res = (unsigned long)-2;
  return res;
}

struct _typetuple_hash {
  size_t operator()(const _typetuple & x) const {
    return hfunct(x);
  }
};
static gp_hash_table<_typetuple, double, _typetuple_hash> _mnhash({},{},{},{},{1<<13});
static unsigned long long _hit = 0;
static unsigned long long _miss = 0;
#define USE_MEMOIZING 1
// using namespace std;
double _gsl_ran_multinomial_pdf(size_t N, const double p[],
				const unsigned int n[]) {
#ifndef NO_CACHE_MULTINOMIAL
  double res;
  assert(N <= 4);
  assert(N >1);
  _typetuple key(n[0],
		 (N>1)? n[1] : 0,
		 (N>2)? n[2] : 0,
		 (N>3)? n[3] : 0,
		 p[0],
		 (N>1)? p[1] : 0.0,
		 (N>2)? p[2] : 0.0,
		 (N>3)? p[3] : 0.0);
  auto it = _mnhash.find(key);
  if (it != _mnhash.end()) {
    //    assert(gsl_ran_multinomial_pdf(N,p,n) == it->second);
    res = it->second;
    _hit++;
  }
  else {
    res = gsl_ran_multinomial_pdf(N,p,n);
    _mnhash[key] = res;
    _miss++;
  }
  return res;
#else
  return gsl_ran_multinomial_pdf(N,p,n);
#endif
}

void print_memoizing_stats() {
  std::cout << "Cache    hit: " << _hit << std::endl;
  std::cout << "Cache miss: " << _miss << std::endl;
}

double binomial_coefficient(unsigned long n, unsigned long k);

// \tau(D1, Vk)
double  _tau(unsigned D0, const CovidTool::SSV_ & Vk, unsigned M);

// \rho(D2, D3, Vk)
double _rho(unsigned D1, unsigned D2, const CovidTool::SSV_ & Vk);

// \phi(D4, D5, D6)
double _phi(unsigned D3, unsigned D4, unsigned D5, const CovidTool::SSV_ & Vk);

// \zeta(D7, D8, Vk)
double _zeta(unsigned D6, unsigned D7, const CovidTool::SSV_ & Vk);

double transProbDelta(const CovidTool::SSV_ & Vk,
		      const std::vector<unsigned> & Delta,
		      const CovidTool::SSV_::ActionType &  A) {
  assert(Delta.size() == 8);
  unsigned M =  unsigned(A);
  double tau = _tau(Delta[0], Vk, M);
  double rho = _rho(Delta[1], Delta[2], Vk);
  double phi = _phi(Delta[3], Delta[4], Delta[5], Vk);
  double zeta = _zeta(Delta[6], Delta[7], Vk);
  double res = tau * rho * phi *  zeta;
  // std::cout << res << " " << tau << " " << rho << " " << phi << " " << zeta << std::endl;
  return res;
}

/*
    Computes the probability that a number of susceptible subjects
    will contract the infection

    D0 : number of subjects that will become infected
    Vk : current state
 */
double  _tau(unsigned D0, const CovidTool::SSV_ & Vk, unsigned M) {
  double tau;
  const unsigned &  N = Vk.parms.N;
  const double & omega = Vk.parms.omega;
  assert(M >= Vk.parms.Mmin && M <= Vk.parms.M);

  if (Vk.d + Vk.i + Vk.o == N) {
    if (D0 == 0)
      tau = 1.0;
    else
      tau = 0;
  } else {
    tau = 1.0-pow((1.0-omega*Vk.a/(N-Vk.d-Vk.i-Vk.o)), M);
    tau = gsl_ran_binomial_pdf(D0, tau, Vk.s);
    assert(!std::isnan(tau));
  }
  return tau;
}

/*
    Probability that D1 infected asymptomatic subjects recover and D2
    infected subjects die

    D1 : Subjects that become symptomatic
    D2 : number of subjects that recover
    Vk : current state
 */
double _rho(unsigned D1, unsigned D2, const CovidTool::SSV_ & Vk) {
  double rho;

  if (D1 + D2 > Vk.a) {
    rho = 0.0;
  }
  else {
    const double & beta = Vk.parms.beta;
    const double & delta = Vk.parms.delta;
    const unsigned n[] = {D1, D2, Vk.a - D1 - D2};
    const double p[] = {delta, beta, 1.0-beta-delta};
    rho = _gsl_ran_multinomial_pdf(3, p, n);
    assert(!std::isnan(rho));
  }
  return rho;
}

/*
    Probability that D3 symptomatic subjects recover and D4 become
    hosppitalised and D5 die:

    D3 : Symptomatic Subjects that Recover
    D4 : Symptomatic Subjects that are Hospitalised
    D5:  Symtomatic Subjects that die
    Vk : current state
 */
double _phi(unsigned D3, unsigned D4, unsigned D5, const CovidTool::SSV_ & Vk) {
  double phi;

  const int Cb = Vk.parms.C - Vk.o;

  if ((D3 + D4 + D5 > Vk.i) || (D4 > Cb)) {
    phi = 0.0;
  }
  else {
    const double & mu = Vk.parms.mu; // probability of symtomatic to recover
    const double & psi = Vk.parms.psi; // probability of a symtomatic  to be hospitalised
    const double & alpha = Vk.parms.alpha; // probability of a symtomatic to die
    const double p[] = {mu, psi, alpha, 1.0-psi-mu-alpha};
    unsigned n[] = {D3, D4, D5, Vk.i-D3-D4-D5};

    if (D4 < Cb) {
      phi = _gsl_ran_multinomial_pdf(4, p, n);
      assert(!std::isnan(phi));
    }
    else {
      // Case D4 == Cb
      assert(D4 == Cb);
      phi = 0.0;
      for(unsigned h = 0; h <= Vk.i - Cb - D3 - D5; h++) {
	// n[] = {D3, Cb+h, D5, Vk.i-D3-Cb-h-D5};
	n[1] = Cb+h;
	n[3] = Vk.i-D3-Cb-h-D5;
	phi += _gsl_ran_multinomial_pdf(4, p, n);
	assert(!std::isnan(phi));
      }
    }
  }
  return phi;
}

/*
    Probability that D6 hospitalised subjects recover and D7 die.

    D6 : Hospitalised Subjects that die
    D7 : Hospitalised Subjects that recover
    Vk : current state
 */
double _zeta(unsigned D6, unsigned D7, const CovidTool::SSV_ & Vk) {
  double zeta;

  if ((D6 + D7 > Vk.o)) {
    zeta = 0.0;
  }
  else {
    const double & sigma = Vk.parms.sigma; // probability to die when hospitalised
    const double & xi = Vk.parms.xi; // probability to recover when hospitalised
    const unsigned n[] = {D6, D7, Vk.o-D6-D7};
    const double p[] = {sigma, xi, 1.0-xi-sigma};
    zeta = _gsl_ran_multinomial_pdf(3, p, n);
    assert(!std::isnan(zeta));
  }
  return zeta;
}


double _lambdaT(unsigned H, const CovidTool::ESV_ & Vk) {
  double res = 0.0;
  unsigned Nk = Vk.a + Vk.s + Vk.ra;
  double gamma = CovidTool::ESV_::parms.gamma;
  unsigned T = Vk.parms.T;
  for (unsigned p = 0; p <= Nk; p++) {
    double tmp = gsl_ran_binomial_pdf(H, gamma, p); // compute C(p,H)*gamma^H*(1-gamma)^{p-H}
    tmp *= binomial_coefficient(Vk.a, p); // compute C(p,Vk.a)
    tmp *= binomial_coefficient(Vk.s+Vk.ra, T - p); // compute C(Sk+Rak, T-p)
    res += tmp;
  }
  res *= 1.0/binomial_coefficient(Nk, T); // compute C(Nk, T)^{-1}
  return res;
}

double _tauT(unsigned D1, const CovidTool::ESV_ & Vk, unsigned M) {
  return _tau(D1, CovidTool::SSV_(Vk), M);
}

double _rhoET(unsigned D2, unsigned D3, unsigned D9, const CovidTool::ESV_ & Vk) {
  unsigned T = Vk.parms.T;

  if ((D2 + D3 + D9 > Vk.a) ||
      (D9 > T)) {
    return 0.0;
  }

  unsigned Ak = Vk.a;
  double res = 1.0;
  for (unsigned H = 0; H <= T; H++) {
    double t1 = _lambdaT(H, Vk);

    double t2 = 0.0;
    for (unsigned F = 0; F <= D9; F++) {
      double t3 = _rho(D2, D3+F, CovidTool::SSV_(Vk));

       t3 *= gsl_sf_fact(H);
       for (unsigned i = 1; i<= F; i++)
	 t3 *= double(D3+i);
       for (unsigned h = F; h<D9; h++)
	 t3 *= double(Ak-D2-D3-h);
       for (unsigned j = 0; j<=D9-H; j++)
	 t3 *= double(D2+j);
       for (unsigned i = 0; i<=H-1; i++)
	 t3 *= 1.0/double(Ak-i);

       t2 += t3;
    }
    res *= t1 * t2;
  }
  return res;
}

double _phiT(unsigned D4, unsigned D5, unsigned D6, const CovidTool::ESV_ & Vk) {
  return _phi(D4, D5, D6, CovidTool::SSV_(Vk));
}

double _zetaT(unsigned D6, unsigned D7, const CovidTool::ESV_ & Vk) {
  return _zeta(D6, D7, CovidTool::SSV_(Vk));
}

double _xi(unsigned D10, unsigned D11, const CovidTool::ESV_ & Vk) {
  double xi;

  if ((D10 + D11 > Vk.q)) {
    xi = 0.0;
  }
  else {
    const double & iota = Vk.parms.iota; // probability a quarantined develops symptoms
    const double & upsilon = Vk.parms.upsilon; // probability to quarantine recovers
    const unsigned n[] = {D10, D11, Vk.q-D10-D11};
    const double p[] = {iota, upsilon, 1.0-upsilon-iota};
    xi = _gsl_ran_multinomial_pdf(3, p, n);
    assert(!std::isnan(xi));
  }
  return xi;
}

double binomial_coefficient(unsigned long n, unsigned long k) {
  unsigned long i;
  double b;
  if (0 == k || n == k) {
    return 1.0;
  }
  if (k > n) {
    return 0.0;
  }
  if (k > (n - k)) {
    k = n - k;
  }
  if (1 == k) {
    return double(n);
  }
  b = 1;
  for (i = 1; i <= k; ++i) {
    b *= double((n - (k - i)));
    if (b < 0) return -1.0; /* Overflow */
    b /= double(i);
  }
  return b;
}

double transProbDeltaT(const CovidTool::ESV_ & Vk,
		       const std::vector<unsigned> & Delta,
		       const CovidTool::SSV_::ActionType &  A) {
  assert(Delta.size() == 11);
  unsigned M =  unsigned(A);
  double tau = _tauT(Delta[0], Vk, M);
  double rho = _rhoET(Delta[1], Delta[2], Delta[8], Vk);
  double phi = _phiT(Delta[3], Delta[4], Delta[5], Vk);
  double zeta = _zetaT(Delta[6], Delta[7], Vk);
  double xi = _xi(Delta[9], Delta[10], Vk);
  double res = tau * rho * phi * zeta * xi;
  // std::cout << res << " " << tau << " " << rho << " " << phi << " " << zeta << std::endl;
  return res;
}
