#include <iostream>
#include <iomanip>
#include <cstdlib>

// Inclusion of utilities

// Inclusion of other libraries
// #include "BasicTypes.hpp"
// #include "Reachability.hpp"
// #include "Reachability_src.hpp"
using namespace std;

double binomial_coefficient(unsigned long n, unsigned long k) {
  unsigned long i;
  double b;
  if (0 == k || n == k) {
    return 1.0;
  }
  if (k > n) {
    return 0.0;
  }
  if (k > (n - k)) {
    k = n - k;
  }
  if (1 == k) {
    return double(n);
  }
  b = 1;
  for (i = 1; i <= k; ++i) {
    b *= double((n - (k - i)));
    if (b < 0) return -1.0; /* Overflow */
    b /= double(i);
  }
  return b;
}



// Test inclusion of the GSL libraries
#include <gsl/gsl_sf_bessel.h>
#include <gsl/gsl_randist.h>

void test_gsl(int argc, char * argv[]) {
  unsigned n,k;
  double r1, r2;

  n = atoi(argv[1]);
  k = atoi(argv[2]);

  r1 = binomial_coefficient(n, k);
  r2 = gsl_ran_binomial_pdf(k, 1.0, n);

  cout << "r1 = " << setprecision(18) << r1 << endl;
  cout << "r2 = " << setprecision(18) << r2 << endl;
}

using namespace std;

int main(int argc, char * argv[]) {
  test_gsl(argc, argv);
  return 0;
}
